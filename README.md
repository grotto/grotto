# Grotto

## Dev setup

You're welcome to run your dev environment if you like, but here is the suggested pattern.

Be sure you have `docker` and `make` installed. Then `cd` into this directory. Use

* `make build` to build your dev docker image
* `make init` will get everything to a base running state (use this once)
* `make run` to run your dev instance (navigate browser to `localhost:8000` to interact)
* `make migrations` when you've changed the models (requires running instance)
* `make stop` to bring the instance down
* `make stop && make run` will apply any migrations
* `make logs` to follow the app logs
* `make superuser` to create a superuser for yourself

See `makefile` for complete list of command targets.

### Setting up

You need to upload a `gedcom` file to the "Instance gedcoms" section of admin, then select that row, choose the action called "Make selected config active", and hit "Go". Allow this to run for several minutes (you may observe the progress in the worker logs).

You will also need to upload 4 CSVs which specify many game elements. Create a new "Instance configs" and provide the 4 CSVs. Once saved use the "Make selected config active" action and allow it to run.

Finally, run the "Seed items, lock doors, place traps" action (just choose any list item) and allow it to finish.


## Deployment

### Environment Variables

| Variable | Description |
|----------|-------------|
| APP_ENV  | Expresses which environment the app is running "dev" or "prod" |
| SECRET_KEY | Django secret key |
| DEBUG | Django debug setting (set to "False" in prod) |

### Droplet setup

Install docker and docker compose

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Install nginx

```sh
apt install -y nginx
```

Set up nginx with ???


Set up docker compose by creating `/root/prod/docker-compose.yml` with contents like:
```
version: '3'

services:
  app:
    image: thismatters/grotto:latest
    ports:
      - "8000:8000"
    environment:
      - APP_ENV=prod
      - DEBUG=False
    volumes:
      - ./db.sqlite:/app/src/db.sqlite3
    restart: always
```

Start grotto with (from `/root/prod/`)

```
docker-compose up -d
```
