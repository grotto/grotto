
if [[ $CONTAINER_PURPOSE == "app" ]]; then
    python manage.py migrate
    python manage.py loaddata grotto/fixtures/guild.json

    if [[ $APP_ENV == "dev" ]]; then
        python manage.py runserver 0.0.0.0:8000
    elif [[ $APP_ENV == "staging" ]]; then
        gunicorn grotto.wsgi:application -b 0.0.0.0:8000
    elif [[ $APP_ENV == "prod" ]]; then
        gunicorn grotto.wsgi:application -b 0.0.0.0:8000
    fi
elif [[ $CONTAINER_PURPOSE == "beat" ]]; then
    sleep 30
    celery --app=grotto $CONTAINER_PURPOSE --loglevel=INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile="/tmp/celerybeat.pid"
else
    celery --app=grotto $CONTAINER_PURPOSE --loglevel=INFO --queues grotto
fi
