# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
### Changed
### Removed
### Fixed

## [1.0.4] -- migration
### Changed
- Some environment stuff for new hosting


## [1.0.3] -- reawakening
### Added
- Sound
- Endpoint for tracking character visits to rooms
- character map via iframe
### Changed
- Item sheet to allow item actions
- Non takable items to have implicit view action when character has bodyparts
- Relics to be seeded in places other than its cenotaph
- Drop icon for cursed objects
- default to using characters sense organ for objects which don't have a default verb
### Fixed
- relic item page to not show `undefined` as action.


## [1.0.2] -- new deployment
### Changed
- deployment model to permit blue-green deployments


## [1.0.1] -- bugs and stuff
### Changed
- making csv validation more tolerant
### Fixed
- message escape to allow quotes and ampersands
- reliquary handles empty cenotaphs better


## [1.0.0] -- showtime!
### Added
- Candle place animation
- Writable letters!
- customizable character greetings
- a simple banning system for people using racial slurs and other egregious language
- effects for using potions
### Changed
- added `touch` handlers for better support on mobile devices
- crone character creation sequence
- death now shows wumpus bite
### Fixed
- wrong animation shown on dropping item
- Some bashing fixes


## [0.10.3]
### Added
- inspector warnings for non-actionable things.
- bashing doors (chance to break, chance to open door)
- Nose with "sniff" action.
- Organ actions can be activated via graphs
- Tracking when an item is held
- View room graphics without characters or items
- URL addressable page for viewing relics
- Sensing things in rooms with organs
- npc viewing page
### Changed
- Item sheet shows who has held the item if character has a nose equipped
- Room description now shows past visitors to the room if character has a nose equipped
- Character generation auto equips per-kind organs.
- Room warnings appear based on what organs a character has equippped.
- Room warnings cannot be sensed through a locked door.
- Sense organs sense more consistently
- Moved resolving entropy changes for NPCs from request-response loop to worker
- Character history shows rooms where things happened
- Character Inventory limited to 20 items/stacks
- Characters unequip their organs upon death
- Characters can be made immortal (via admin)
### Removed
- no more gui view toggle (\`)
### Fixed
- bashing via graphics

## [0.10.2]
### Added
- body parts may be equipped.
- equipped body parts show up in their own inventory.
- cursed items cannot be unequipped/dropped
- banner animation on front page
### Changed
- keys more durable (chance to break)

## [0.10.1]
### Added
### Changed
- CI pipeline to use fewer minutes
- Animations to fire more cleanly, and roll into "loading" animation if response isn't ready.
- custom animation display when certain items are used (e.g. candle)
### Removed
### Fixed
- Player sees "you have died" screen after creating a new character
- npc greetings were displaying _abstract_npc_ greeting templates

## [0.10.0] -- Grotto Paint
### Added
- Grotto paint, a pixi.js powered graphical view of rooms and their contents
### Changed
- Room warnings bundled
### Removed
- gui room; text view now has gui-ish view.

## [0.9.7]
### Added
- `foundry.models.Item.related_cenotaph` represent relics for a cenotaph
- Cenotaph Activation Criteria based upon room attributes which, when satisfied, release relics (see `item.related_cenotaph`) from the cenotaph. When those relics are dropped in other rooms they create a temporary portal back to the cenotaph. When the activation criteria lapse the item is recalled to the cenotaph (and the portal is removed).
- `guild.models.NonPlayerCharacter.factotem` to look after cenotaph activation criteria; when an npc with the `factotem` flag moves into a room, it will log activation criteria and may either activate or deactivate a cenotaph.
- Better imports of collectables
- AbstractNonPlayerCharacters to provide a template for creating several NPCs in the maze. Requires deletion of instances of following classes: `Item`, `AbstractItem`, `Swap`, `NonPlayerCharacterDeath`, `NonPlayerCharacter`
- import cenotaph activation criteria
- `hostile` as npc flag
- Shield bash (new verb for shields)
- pagination to anonymous user room index
### Changed
- Player actions only cause NPCs on same level of the maze to tick their actions.
### Removed
- `foundry.models.Item.activates_cenotaph`
### Fixed
- Anonymous user room view when there is a room name collision

## [0.9.6]
### Added
- `incorporeal` and `trap_resistant` flags for NPCs
### Changed
- NPCs to not be able to pass through locked doors
- Arrows bounce off of locked doors (beware of ricochets!)
### Removed
### Fixed
- Door locking, which was too sensitve to inconsistent lock status
- Trap springing (in a couple places)
- Guild hall text referencing crone goes away when character list is full

## [0.9.5]
### Added
- machinery to lock stairwells and trap exits randomly
- CSV export of cenotaphs
### Changed
- Only humans and robots start with scrubbrushes
- Players can only have one of each class of character
### Fixed
- Homepage version is now dynamic

## [0.9.4]
### Added
- NPCs can carry candles and they will always act lit.
- Locking doors and keys and stuff
- Message queue and asynchronous tasks (via Celery)

## [0.9.3]
### Added
- `ref` fields to several models for tracking against external database
- Several import management command for parsing CSVs to place items, npcs in maze
- Admin model for uploading then parsing CSVs for items, npcs.

## [0.9.2]
### Added
- Tuffet item type for kneeling in rooms.
- Rooms now have a `level`, use management command `sort_rooms_to_level` to populate
- NPCs have attributes for `level`, `mobile_up`, and `mobile_down` to constrain NPC location and movements.
### Changed
- Items more visible in dark rooms
- Cenotaph text generation
- Cenotaphs less visible in dark rooms.

## [0.9.1]
### Fixed
- Cenotaphs now hidden for living people.

## [0.9.0]
### Added
- Traps can be set on doors
- Lanters are permanent lights which will appear in rooms randomly
- Arrows and Shields may randomly spawn in rooms (upon room creation)
- Colors now have built in json codes
- body_parts.txt to word lists
### Changed
- A single starting room can be set for all characters.
- Rooms now created with gedcom data
- Room colors are de-duplicated

## [0.8.0]
### Added
- Eyes for characters to view items
- Arena for fighting ancestors
- Inline files for Items
- Explicit GLB model for Item
### Changed
- Fields/filters for admin models
### Fixed
- Bug that prevented candles and incense from burning out

## [0.7.1]
### Added
- Static deadly NPCs redistribute collectables into a random room.
- Characters show their history on their character sheet.
- `/api/v1/guild/scoreboard/` endpoint to display NPC kills.
- Items can be placed in a cenotaph (via admin); these items will be relocated to the room when the cenotaphs activation item is dropped in the room.
### Changed
- If a shield has the word "durable" in its name then it will withstand many arrows.
- `maze.models.RoomEvent` -> `guild.models.Event`, `Event` instances are tied to the character that generated the event.
- `characters` API endpoint is now available at `/api/v1/guild/characters/`
- `character_tests` API endpoint is now available at `/api/v1/guild/character_tests/`
- Repeated events in room history are consolidated and marked with "x3", for example.

## [0.7.0]
### Added
- Keyboard support on guild, crone, cenotaph, and character pages
- Item types for "collectable" and "decoration".
- Reference for favicon.
- Cenotaph may be "activated" by placing a specific item in the room.
- Arrow key nav in cenotaph
### Changed
- Made NPCs look for swaps when feces is used (support for latrine npc)
- Swaps may now discriminate between abstract items
- Improved generation base game resources (abstract items, npcs)
- `warnings` and `messages` have wrappers in API
### Fixed
- Race condition which occured under some circumstances following a character death

## [0.6.0] -- 2022-02-27
### Added
- This changelog!
- Conept of floors. Rooms can now be adjacent to each other in 4 directions: `UP`, `DOWN`, `SAME`, `PORTAL`. The `RoomAdjacencyService` is the only way that rooms should be added.
- Kneeling as an action which can be performed on a room. Creates a `RoomEvent` and persists a record of the `Kneel`
- User preferences around which view they are using. Preference is respectced by all pages. Preferences can be set by way of the `/api/v1/account/preferences/` endpoint.
- A kind of basic map (press "m")
- [GUI] Basic door animation
- [GUI] Display room warnings
- No-robots meta tags
### Changed
- **BREAKING** `/api/v1/map/...` -> `/api/v1/maze/...`
- Moved login/registration urls/views to the `account` app.
- Arrow interaction; `aim` arrow, then select a room to `fire` at.
### Removed
- "Reset Password" link from login; we don't collect email addresses so we cannot offer password changes.
