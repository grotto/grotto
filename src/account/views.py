from urllib.parse import urlparse

from django import forms
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.mixins import LoginRequiredMixin as BaseLoginRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import redirect_to_login
from django.shortcuts import resolve_url
from django.views.generic import FormView

from account.models import UserPreferences


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("username",)
        field_classes = {"username": UsernameField}


class RegisterView(FormView):
    form_class = CustomUserCreationForm
    template_name = "registration/register.html"
    success_url = "/game/play/"

    def form_valid(self, form):
        form.save()
        # log in the user here!
        user = authenticate(
            self.request,
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password1"],
        )
        UserPreferences.objects.create(user=user)
        login(self.request, user)
        return super().form_valid(form)


class TermsAcceptForm(forms.Form):
    accept_terms = forms.BooleanField()


class TermsAcceptView(BaseLoginRequiredMixin, FormView):
    form_class = TermsAcceptForm
    success_url = "/game/play/"
    template_name = "registration/accept_terms.html"

    def form_valid(self, form):
        if form.cleaned_data["accept_terms"] is True:
            self.request.user.accepts_terms = True
            self.request.user.save()
        return super().form_valid(form)


class LoginRequiredMixin(UserPassesTestMixin):
    login_url = "/accounts/terms/"

    def test_func(self):
        return self.request.user.is_anonymous or self.request.user.accepts_terms

    def handle_no_permission(self):
        path = self.request.build_absolute_uri()
        resolved_login_url = resolve_url(self.get_login_url())
        # If the login url is the same scheme and net location then use the
        # path as the "next" url.
        login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
        current_scheme, current_netloc = urlparse(path)[:2]
        if (not login_scheme or login_scheme == current_scheme) and (
            not login_netloc or login_netloc == current_netloc
        ):
            path = self.request.get_full_path()
        return redirect_to_login(
            path,
            resolved_login_url,
            self.get_redirect_field_name(),
        )

    def dispatch(self, request, *args, **kwargs):
        # check for login specifically
        if request.user.is_anonymous:
            self.login_url = None
            return self.handle_no_permission()
        if not request.user.accepts_terms:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
