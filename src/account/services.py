
def ban(user, reason):
    user.is_active = False
    user.ban_reason = reason
    user.set_unusable_password()
    user.save()
