from django.contrib.auth.models import AbstractUser
from django.db import models
from django_enumfield import enum

from guild.models import Character


class UserViewEnum(enum.Enum):
    TEXT = 0
    GUI = 1


class User(AbstractUser):
    """Placeholder User model in case we need to add fields in the future
    https://docs.djangoproject.com/en/3.1/topics/auth/customizing/...
      #using-a-custom-user-model-when-starting-a-project
    """

    accepts_terms = models.BooleanField(default=False)
    character = models.OneToOneField(
        Character,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="chosen_one",
    )
    ban_reason = models.CharField(max_length=256, null=True, blank=True)


class UserPreferences(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="preferences"
    )
    locked = models.BooleanField(default=False)
    # this is now unused!
    view = enum.EnumField(UserViewEnum, default=UserViewEnum.TEXT)
