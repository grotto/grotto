from django_enumfield.contrib.drf import NamedEnumField
from rest_framework import serializers

from account import models


class UserPreferencesSerializer(serializers.ModelSerializer):
    view = NamedEnumField(models.UserViewEnum)

    class Meta:
        model = models.UserPreferences
        fields = ("view",)
