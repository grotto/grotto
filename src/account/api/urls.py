from django.urls import path
from rest_framework.routers import DefaultRouter, Route

from account.api import views

app_name = "account-api"


class UserPreferencesRouter(DefaultRouter):
    routes = [
        Route(
            url=r"^{prefix}{trailing_slash}$",
            mapping={
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
            },
            name="{basename}-list",
            detail=False,
            initkwargs={"suffix": "List"},
        ),
    ]


router = UserPreferencesRouter()
router.register("preferences", views.UserPreferencesViewSet, basename="preferences")

urlpatterns = router.urls

urlpatterns += [path("api_token/", views.auth_token, name="api-token")]
