from rest_framework import mixins, status, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from account import models
from account.api import serializers


@api_view(["GET", "DELETE"])
@permission_classes((IsAuthenticated,))
def auth_token(request):
    # User must be logged in with a session from the frontend to get this token
    if request.method == "GET":
        token, _ = Token.objects.get_or_create(user=request.user)
        return Response({"token": token.key})
    elif request.method == "DELETE":
        # allow a user to remove their existing api token so that a new one
        #  can be generated.
        Token.objects.filter(user=request.user).delete()
        return Response({}, status=status.HTTP_202_ACCEPTED)


class UserPreferencesViewSet(
    viewsets.GenericViewSet,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
):
    serializer_class = serializers.UserPreferencesSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return models.UserPreferences.objects.filter(user=self.request.user)

    def get_object(self):
        return self.request.user.preferences

    def perform_update(self, serializer):
        if self.request.user.preferences.locked:
            return
        return super().perform_update(serializer)
