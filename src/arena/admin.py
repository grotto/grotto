from django.contrib import admin

from arena import models


class SkillInline(admin.TabularInline):
    model = models.Skill
    extra = 0
    fields = ["name", "level", "description"]


class WeaponInline(admin.TabularInline):
    model = models.Weapon
    extra = 0
    fields = ["name", "description"]


@admin.register(models.Fighter)
class FighterAdmin(admin.ModelAdmin):
    list_display = ("name", "birth", "death")
    inlines = [SkillInline, WeaponInline]


class AttackInline(admin.TabularInline):
    model = models.Attack
    extra = 0
    fields = ["attacker", "roll", "modifier", "hit", "coup_de_grace"]
    read_only_fields = ["attacker", "roll", "modifier", "hit", "coup_de_grace"]


@admin.register(models.Skirmish)
class SkirmishAdmin(admin.ModelAdmin):
    list_display = ("id", "aggressor", "retaliator", "victor")
    inlines = [AttackInline]
