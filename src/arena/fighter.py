import random

from gedcom.element.individual import IndividualElement
from gedcom.parser import Parser

# Path to your `.ged` file
file_path = "../word_lists/family_data/wiley.ged"
# Initialize the parser
gedcom_parser = Parser()

# Parse your file
gedcom_parser.parse_file(file_path, False)  # Disable strict parsing

root_child_elements = (
    gedcom_parser.get_root_child_elements()
)  # what is this? is it a list?


# how do we check to make sure someone is dead? - is_deceased


# Iterate through all root child elements example
"""
for element in root_child_elements:

    # Is the `element` an actual `IndividualElement`?
    # (Allows usage of extra functions such as `surname_match` and `get_name`.)
    if isinstance(element, IndividualElement):

        # Get all individuals whose surname matches "Doe"
        if element.surname_match('Doe'):

            # Unpack the name tuple
            (first, last) = element.get_name()

            # Print the first and last name of the found individual
            print(first + " " + last) 
"""


class Fighter:
    def __init__(self):
        self.name = "Wiley Wiggins"
        # kind is implicit in dispute, but if we bring combat into
        #  grotto it won't be
        self.kind = "Human"
        self.description = "A middle-aged, pasty grad student"
        self.occupation = "student"
        self.skills = ["procrastination: 12", "irritability: 18"]
        self.weapon = "rolling pin"
        self.birthdate = "Nov 6 1976"
        self.birthplace = "Texas, United States of America"
        self.deathdate = "Jan 1 2048"
        self.deathplace = "Eshmun Crater, Ganymede"
        self.points = 0
        self.gender = "male"
        self.id = 1  # I think gedcom entries have a hash we can check for dupes with?

    def getRandomAncestor(self):
        foundAncestor = False
        while not foundAncestor:
            ancestor = random.choice(root_child_elements)
            if isinstance(ancestor, IndividualElement):
                if ancestor.is_deceased():
                    foundAncestor = True
                    (first, last) = ancestor.get_name()
                    if first is None or first == "":
                        first = "(unknown)"
                    self.name = first + " " + last
                    self.birthdate = ancestor.get_birth_year()
                    if self.birthdate == -1 or self.birthdate is None:
                        self.birthdate = "an unknown year"
                    birthdata = ancestor.get_birth_data()
                    self.birthplace = birthdata[1]
                    if self.birthplace == "" or self.deathdate is None:
                        self.birthplace = "an unknown place"
                    self.deathdate = ancestor.get_death_year()
                    if self.deathdate == -1 or self.deathdate is None:
                        self.deathdate = "an unknown year"
                    deathdata = ancestor.get_death_data()
                    self.deathplace = deathdata[1]
                    if self.deathplace == "" or self.deathdate is None:
                        self.deathplace = "an unknown place"
                    self.occupation = ancestor.get_occupation()
                    self.gender = ancestor.get_gender()
                    if self.gender == "M":
                        posPronoun = "his"
                        pronoun = "he"
                    elif self.gender == "F":
                        posPronoun = "her"
                        pronoun = "she"
                    else:
                        posPronoun = "their"
                        pronoun = "they"

                    if self.occupation is not None and self.occupation != "":
                        occupationString = (
                            "During "
                            + posPronoun
                            + " life, "
                            + pronoun
                            + " worked as a "
                            + self.occupation
                            + ".\n"
                        )
                    else:
                        occupationString = ""
                    descriptionString = (
                        self.name
                        + " was born in "
                        + str(self.birthdate)
                        + " in "
                        + self.birthplace
                        + ".\n"
                        + occupationString
                        + pronoun.capitalize()
                        + " died in "
                        + str(self.deathdate)
                        + " in "
                        + self.deathplace
                        + ".\n"
                        + "\nDEBUG:"
                        + "Gender: "
                        + self.gender
                        + "\n"
                        + "Occupation: "
                        + self.occupation
                    )
                    self.description = descriptionString
                    # return ancestor

    # fighters each get a random weapon name for flavor text

    # generate skill stats, display each fighter's idle animation,
    #  historical data, weapon and skill stats

    # hmmmm do the json for animations live inside the fighter object?
    #  if there end up being different versions, maybe!
