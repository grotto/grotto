from random import randint, sample

from arena import models


class SkirmishException(Exception):
    pass


class SkirmishService:
    def create(self):
        try:
            aggressor, retaliator = sample(
                list(models.Fighter.objects.all().values_list("pk", flat=True)),
                2,
            )
        except ValueError:
            raise

        skirmish = models.Skirmish.objects.create(
            aggressor_id=aggressor, retaliator_id=retaliator
        )
        return skirmish

    def advance(self, skirmish):
        if skirmish.victor is not None:
            return skirmish.attacks.get(coup_de_grace=True)
        # choose attacker
        aggressor_attack_count = skirmish.attacks.filter(
            attacker=skirmish.aggressor
        ).count()
        aggressor_hit_count = skirmish.attacks.filter(
            attacker=skirmish.aggressor, hit=True
        ).count()
        retaliator_attack_count = skirmish.attacks.filter(
            attacker=skirmish.retaliator
        ).count()
        retaliator_hit_count = skirmish.attacks.filter(
            attacker=skirmish.retaliator, hit=True
        ).count()

        attacker = skirmish.aggressor
        attacker_hit_count = aggressor_hit_count
        defender = skirmish.retaliator
        if aggressor_attack_count > retaliator_attack_count:
            attacker = skirmish.retaliator
            attacker_hit_count = retaliator_hit_count
            defender = skirmish.aggressor

        # count modifiers
        modifier = sum(attacker.skills.all().values_list("level", flat=True))
        modifier -= sum(defender.skills.all().values_list("level", flat=True))

        # attack roll
        roll = randint(0, 100)

        # resolve
        hit = roll + modifier > 50
        coup_de_grace = False
        if hit and attacker_hit_count >= 3:
            # fourth hit resolves skirmish
            skirmish.victor = attacker
            skirmish.save()
            coup_de_grace = True

        return models.Attack.objects.create(
            skirmish=skirmish,
            attacker=attacker,
            roll=roll,
            modifier=modifier,
            hit=hit,
            coup_de_grace=coup_de_grace,
        )
