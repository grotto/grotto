from random import randint

from django.core.management.base import BaseCommand
from python_gedcom_2.element.individual import IndividualElement
from python_gedcom_2.parser import Parser

from arena.models import Fighter, Skill, Weapon
from grotto.services import BaseGenerationService


class Command(BaseCommand):
    help = "Intakes GEDCOM files to create fighters."

    def add_arguments(self, parser):
        # arg for hostname
        parser.add_argument(
            "--gedcom_filename",
            "-g",
        )

    def _normalize_unknowns(self, value):
        if value in ("", -1, None):
            value = None
        return value

    def handle_child_element(self, element):
        fighter = self._create_fighter(element)
        self._generate_skills(fighter)
        self._generate_weapons(fighter)

    def _generate_skills(self, fighter):
        skills = list(
            Skill.objects.filter(fighter=fighter).values_list("name", flat=True)
        )
        skills_count = max(0, 3 - len(skills))
        for _ in range(skills_count):
            skill = None
            while skill is None or skill in skills:
                skill = self.generator.skill()
            skills.append(skill)
            Skill.objects.create(fighter=fighter, name=skill, level=randint(-10, 18))
        return skills

    def _generate_weapons(self, fighter):
        weapons = list(
            Weapon.objects.filter(fighter=fighter).values_list("name", flat=True)
        )
        weapons_count = max(0, 1 - len(weapons))
        for _ in range(weapons_count):
            weapon = None
            while weapon is None or weapon in weapons:
                weapon = self.generator.weapon()
            weapons.append(weapon)
            Weapon.objects.create(
                fighter=fighter,
                name=weapon,
            )
        return weapons

    def _create_fighter(self, element):
        verbose = {
            "birth": "an unknown year",
            "death": "an unknown year",
            "birthplace": "an unknown place",
            "deathplace": "an unknown place",
            "occupation": "",
        }
        fields = {
            "birth": ("get_birth_year",),
            "death": ("get_death_year",),
            "birthplace": ("get_birth_data", 1),
            "deathplace": ("get_death_data", 1),
        }
        defaults = {}
        for field, address in fields.items():
            prelim = self._normalize_unknowns(getattr(element, address[0])())
            if len(address) > 1:
                prelim = prelim[address[1]]
            defaults[field] = prelim
            if defaults[field] is not None:
                verbose[field] = defaults[field]

        name = " ".join(element.get_name())
        occupation = self._normalize_unknowns(element.get_occupation())
        gender = element.get_gender()

        if gender == "M":
            posPronoun = "his"
            pronoun = "he"
        elif gender == "F":
            posPronoun = "her"
            pronoun = "she"
        else:
            posPronoun = "their"
            pronoun = "they"

        f_occupation = ""
        if occupation is not None:
            f_occupation = (
                f"During {posPronoun} life, {pronoun} worked as a {occupation}.\n"
            )

        defaults["description"] = (
            "{name} was born in {verbose[birth]} in {verbose[birthplace]}.\n"
            "{occupation}"
            "{pronoun} died in {verbose[death]} in {verbose[deathplace]}.\n"
        ).format(
            name=name,
            verbose=verbose,
            pronoun=pronoun.capitalize(),
            occupation=f_occupation,
        )

        return Fighter.objects.update_or_create(
            name=name,
            birth=defaults.pop("birth"),
            death=defaults.pop("death"),
            defaults=defaults,
        )[0]

    def handle(self, *args, gedcom_filename, **options):
        self.generator = BaseGenerationService()
        parser = Parser()

        # Parse your file
        parser.parse_file(gedcom_filename, False)  # Disable strict parsing

        root_child_elements = (
            parser.get_root_child_elements()
        )  # what is this? is it a list?

        for element in root_child_elements:
            if isinstance(element, IndividualElement):
                if element.is_deceased():
                    self.handle_child_element(element)
                else:
                    print(">> Living person")
                    print(element)
            else:
                print(">> Non IndividualElement")
                print(type(element))
                print(element)
