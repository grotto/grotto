from django.db import models


class Fighter(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField()
    birth = models.CharField(max_length=64, null=True, blank=True)
    death = models.CharField(max_length=64, null=True, blank=True)
    birthplace = models.CharField(max_length=256, null=True, blank=True)
    burialplace = models.CharField(max_length=256, null=True, blank=True)
    deathplace = models.CharField(max_length=256, null=True, blank=True)
    # portrait = models.FileField(
    #     upload_to=centaph_portrait_uploads,
    #     validators=[validate_pdf_image_or_audio],
    #     null=True,
    #     blank=True,
    # )

    def __str__(self):
        return self.name


class Weapon(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    fighter = models.ForeignKey(
        Fighter, on_delete=models.CASCADE, related_name="weapons"
    )

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    level = models.IntegerField()
    fighter = models.ForeignKey(
        Fighter, on_delete=models.CASCADE, related_name="skills"
    )

    def __str__(self):
        return f"lvl {self.level} {self.name}"


class Skirmish(models.Model):
    aggressor = models.ForeignKey(
        Fighter,
        on_delete=models.CASCADE,
        related_name="skirmishes_started",
    )
    retaliator = models.ForeignKey(
        Fighter, on_delete=models.CASCADE, related_name="skirmishes"
    )
    victor = models.ForeignKey(
        Fighter,
        on_delete=models.CASCADE,
        related_name="victories",
        null=True,
        blank=True,
    )
    started = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "skirmishes"

    def __str__(self):
        return f"{self.aggressor} vs {self.retaliator}"


class Attack(models.Model):
    skirmish = models.ForeignKey(
        Skirmish, on_delete=models.CASCADE, related_name="attacks"
    )
    attacker = models.ForeignKey(
        Fighter,
        on_delete=models.CASCADE,
        related_name="attacks",
    )
    roll = models.IntegerField()
    modifier = models.IntegerField()
    hit = models.BooleanField(default=False)
    coup_de_grace = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (
            f"{self.attacker} {'hits' if self.hit else 'misses'} "
            f"({self.roll - self.modifier}){' FTW' if self.coup_de_grace else ''}"
        )
