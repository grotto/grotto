from rest_framework.routers import DefaultRouter

from arena.api import views

app_name = "arena-api"

router = DefaultRouter()
router.register("skirmishes", views.SkirmishViewSet, basename="skirmish")

urlpatterns = router.urls
