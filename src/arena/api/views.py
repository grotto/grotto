from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from arena.api import serializers
from arena.models import Skirmish
from arena.services import SkirmishService
from grotto.api.serializers import NullSerializer


class SkirmishViewSet(ModelViewSet):
    queryset = Skirmish.objects.all()
    serializer_class = NullSerializer

    @swagger_auto_schema(responses={201: serializers.SkirmishSerializer})
    def create(self, request, *args, **kwargs):
        skirmish = SkirmishService().create()
        serializer = serializers.SkirmishSerializer(
            skirmish, context=self.get_serializer_context()
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(responses={200: serializers.AttackSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=NullSerializer,
    )
    def advance(self, request, pk):
        skirmish = self.get_object()
        attack = SkirmishService().advance(skirmish)
        # respond
        serializer = serializers.AttackSerializer(attack)
        return Response(serializer.data)
