from rest_framework import serializers

from arena import models


class WeaponSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Weapon
        fields = (
            "pk",
            "name",
        )


class FighterSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Skill
        fields = (
            "pk",
            "name",
            "level",
        )


class FighterSerializer(serializers.ModelSerializer):
    weapons = WeaponSerializer(many=True, read_only=True)
    skills = FighterSkillSerializer(many=True, read_only=True)

    class Meta:
        model = models.Fighter
        fields = (
            "pk",
            "name",
            "description",
            "birth",
            "death",
            "birthplace",
            "burialplace",
            "weapons",
            "skills",
        )


class AttackSerializer(serializers.ModelSerializer):
    skirmish_pk = serializers.PrimaryKeyRelatedField(read_only=True, source="skirmish")
    attacker_pk = serializers.PrimaryKeyRelatedField(read_only=True, source="attacker")

    class Meta:
        model = models.Attack
        fields = (
            "pk",
            "skirmish_pk",
            "attacker_pk",
            "roll",
            "modifier",
            "hit",
            "coup_de_grace",
            "created",
        )


class SkirmishSerializer(serializers.ModelSerializer):
    aggressor = FighterSerializer(read_only=True)
    retaliator = FighterSerializer(read_only=True)
    victor = FighterSerializer(read_only=True)
    attacks = AttackSerializer(many=True, read_only=True)

    class Meta:
        model = models.Skirmish
        fields = (
            "pk",
            "aggressor",
            "retaliator",
            "victor",
            "started",
            "attacks",
        )
