import io
import random

import fighter

attacksWords = io.open(
    "../word_lists/attacks.txt", encoding="utf-8"
)  # sorry I am not doing this the correct way yet
attacksList = list(attacksWords)

defenseWords = io.open("../word_lists/defenses.txt", encoding="utf-8")
defenseList = list(defenseWords)

weaponWords = io.open("../word_lists/weapons.txt", encoding="utf-8")
weaponList = list(weaponWords)

# gedcom code moved to fighter.py

# always five turns doesn't feel right, a match shouldn't end on a
#  missed swing. first to get five hits now.
matchPoints = 4
# tournament would be ten ancestors each one fighting the last winner
brackets = 10  # five fights per tournament

# generate a fighter from fighter class- get two ancestor's names and
#   data, show their starting animations with stats and description

fighter1 = fighter.Fighter()

fighter2 = fighter.Fighter()


def attack(attacker, defender):
    attackString = (
        attacker.name
        + " "
        + random.choice(attacksList)
        + " "
        + defender.name
        + " with their "
        + attacker.weapon
        + "!"
    )
    attackString = attackString.replace("\n", "")
    return attackString


for x in range(10):
    fighter1.getRandomAncestor()
    fighter2.getRandomAncestor()
    fighter1.weapon = random.choice(weaponList)
    fighter2.weapon = random.choice(weaponList)
    print(
        fighter1.name
        + " ("
        + str(fighter1.birthdate)
        + " - "
        + str(fighter1.deathdate)
        + ") vs. "
        + fighter2.name
        + " ("
        + str(fighter2.birthdate)
        + " - "
        + str(fighter2.deathdate)
        + ")"
    )

    print(fighter1.description + "\n")
    print(fighter2.description + "\n")

print("\nMatch 1:")
print("Fight!\n")
turn = 1
while fighter1.points < matchPoints and fighter2.points < matchPoints:
    hitRoll = random.randint(0, 100)
    if turn % 2 == 0:
        print(attack(fighter1, fighter2))
        if hitRoll > 50:
            print("It's a hit!")
            fighter1.points += 1
            if fighter1.points > 2:
                print(fighter2.name + " is looking hurt!")
        else:
            defenseString = fighter2.name + " " + random.choice(defenseList)
            defenseString = defenseString.replace("\n", "")
            print(defenseString)
    else:
        print(attack(fighter2, fighter1))
        if hitRoll > 50:
            print("It's a hit!")
            fighter2.points += 1
            if fighter2.points > 2:
                print(fighter1.name + " is looking hurt!")
        else:
            defenseString = fighter1.name + " " + random.choice(defenseList)
            defenseString = defenseString.replace("\n", "")
            print(defenseString)
    turn += 1
if fighter1.points > fighter2.points:
    print(fighter1.name + " wins! " + fighter2.name + " is removed from history!")
else:
    print(fighter2.name + " wins! " + fighter1.name + " is removed from history!")


# random fighter goes first

# all skills are added together to get a total skill number,
#   this modifies chance to hit- 50% + modifier chance to hit

# play starting fighter's attack animation

# play defending fighter's hit or dodge animation

# could be five different idles for different levels of damage

# after turn five display winner and loser animations

# fighter goes on to next bracket, pick new random opponent
