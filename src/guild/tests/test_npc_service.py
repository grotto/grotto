from django.test import TestCase
from django.utils.timezone import now

from foundry.tests.base import ItemBuilderModelMixin
from guild.services import NonPlayerCharacterService
from guild.tests.base import CharacterBuilderModelMixin
from maze.models import Room
from maze.tests.base import MapBuilderModelMixin


class NonPlayerCharacterServiceTestCase(
    TestCase,
    MapBuilderModelMixin,
    ItemBuilderModelMixin,
    CharacterBuilderModelMixin,
):
    def setUp(self):
        super().setUp()
        # 4 rooms, greycoded lit and sanctified
        self.full = self._room(name="full")
        self.lit = self._room(name="lit")
        self.sanctified = self._room(name="sanctified")
        self.empty = self._room(name="empty")

        # make all rooms adjacent
        self.full.exits.add(self.lit, self.sanctified, self.empty)
        self.lit.exits.add(self.sanctified, self.empty)
        self.sanctified.exits.add(self.empty)

        # put items in room
        self._candle(active=now(), room=self.full)
        self._candle(active=now(), room=self.lit)
        self._incense(active=now(), room=self.full)
        self._incense(active=now(), room=self.sanctified)

        # 1 npc
        self.npc = self._npc()

        # 1 character
        self.character = self._character()

    def test_npc_random_placement(self):
        chosen_rooms = set()
        chosen_counts = {}
        for i in range(50):
            NonPlayerCharacterService().move(npc=self.npc, room="random")
            chosen_rooms.add(self.npc.room.name)
            chosen_counts.setdefault(self.npc.room.name, 0)
            chosen_counts[self.npc.room.name] += 1
        print(chosen_rooms)
        self.assertEqual(len(chosen_rooms), 4)
        print(chosen_counts)

    def test_npc_movement(self):
        chosen_rooms = set()
        for i in range(50):
            NonPlayerCharacterService().move(npc=self.npc)
            chosen_rooms.add(self.npc.room.name)
        print(chosen_rooms)
        self.assertEqual(len(chosen_rooms), 4)

    def test_demonic_npc_random_placement(self):
        # chooses only unlit unsanctified room
        self.npc.demonic = True
        self.npc.save()
        chosen_rooms = set()
        for i in range(50):
            NonPlayerCharacterService().move(npc=self.npc, room="random")
            chosen_rooms.add(self.npc.room.name)
        print(chosen_rooms)
        self.assertEqual(len(chosen_rooms), 1)
        self.assertEqual(chosen_rooms, {self.empty.name})

    def test_demonic_npc_random_placement_no_rooms(self):
        # cannot choose room
        self.npc.demonic = True
        self.npc.room = None
        self.npc.save()
        self._candle(active=now(), room=self.empty)
        chosen_rooms = set()
        for i in range(50):
            NonPlayerCharacterService().move(npc=self.npc, room="random")
            chosen_rooms.add(self.npc.room)
        print(chosen_rooms)
        self.assertEqual(len(chosen_rooms), 1)
        self.assertEqual(chosen_rooms, {None})

    def test_demonic_npc_movement(self):
        self.npc.demonic = True
        self.npc.room = None
        self.npc.save()
        chosen_rooms = set()
        for i in range(10):
            self.npc.refresh_from_db()
            NonPlayerCharacterService().move(npc=self.npc)
            self.npc.refresh_from_db()
            print(self.npc.room.name)
            chosen_rooms.add(self.npc.room.name)
        print(chosen_rooms)
        for room in Room.objects.all():
            print(room, room.lit_candle_count, room.lit_incense_count)
        print(Room.objects.all().sanctified())
        print(Room.objects.all().sanctified(exclude=True))
        print(Room.objects.all().dark().sanctified(exclude=True))
        print(self.lit.exits.all().sanctified(exclude=True))
        print(self.empty.exits.all().sanctified(exclude=True))
        print(self.full.exits.all().sanctified(exclude=True))
        self.assertEqual(len(chosen_rooms), 2)
        self.assertEqual(chosen_rooms, {self.empty.name, self.lit.name})

    def test_killer_npc_kills(self):
        # moves between two adjacent rooms
        self.npc.demonic = True
        self.npc.deadly = True
        self.npc.room = self.empty
        self.npc.save()
        chosen_rooms = set()

        self.character.room = self.lit
        self.character.save()

        self.npc.refresh_from_db()
        self.assertFalse(self.character.dead)
        NonPlayerCharacterService().move(npc=self.npc)
        self.npc.refresh_from_db()
        self.assertEqual(self.npc.room, self.lit)
        # kills character in room
        self.character.refresh_from_db()
        self.assertTrue(self.character.dead)
