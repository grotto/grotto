from guild import models


class CharacterBuilderModelMixin:
    def _character(self, **kwargs):
        _base = {
            "kind": "human",
            "description": "a pasty programmer",
        }
        _base.update(kwargs)
        return models.Character.objects.get_or_create(**_base)[0]

    def _npc(self, **kwargs):
        _base = {
            "name": "npc",
            "warning_text": "npc nearby",
        }
        _base.update(kwargs)
        return models.NonPlayerCharacter.objects.get_or_create(**_base)[0]
