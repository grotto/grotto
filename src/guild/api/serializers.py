from rest_framework import serializers
from django_enumfield.contrib.drf import NamedEnumField

from foundry.api.serializers import ItemStackSerializer, AbstractItemSerializer
from foundry.enum import ItemType
from foundry.models import AbstractItem, Swap
from guild import models


class OccupantSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Character
        fields = ("pk", "kind", "name", "greeting")


class SwapSerializer(serializers.ModelSerializer):
    puts = AbstractItemSerializer()
    picks_type = NamedEnumField(ItemType)
    picks_abstract_item = AbstractItemSerializer()

    class Meta:
        model = Swap
        fields = ("picks_type", "picks_abstract_item", "puts")


class AbstractNonPlayerCharacterSerializer(serializers.ModelSerializer):
    abstract_loot = AbstractItemSerializer(many=True)
    swaps = SwapSerializer(many=True)

    class Meta:
        model = models.AbstractNonPlayerCharacter
        fields = (
            "hostile",
            "incorporeal",
            "demonic",
            "mortal",
            "abstract_loot",
            "swaps",
        )


class NonPlayerCharacterSerializer(serializers.ModelSerializer):
    abstract_npc = AbstractNonPlayerCharacterSerializer()
    icon_data = serializers.CharField(source="abstract_npc.icon_data")

    class Meta:
        model = models.NonPlayerCharacter
        fields = (
            "pk",
            "name",
            "abstract_npc",
            "greeting",
            "icon_data",
            "description",
        )


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Skill
        fields = (
            "name",
            "level",
        )


class EventSerializer(serializers.ModelSerializer):
    room = serializers.StringRelatedField()

    class Meta:
        model = models.Event
        fields = ("created", "text", "room")


class CharacterSerializer(serializers.ModelSerializer):
    inventory = ItemStackSerializer(many=True, source="stacked_inventory")
    bodyparts = ItemStackSerializer(many=True, source="stacked_bodyparts")
    skills = SkillSerializer(many=True)
    events = EventSerializer(many=True)

    class Meta:
        model = models.Character
        fields = (
            "pk",
            "kind",
            "name",
            "description",
            "greeting",
            "events",
            "skills",
            "inventory",
            "bodyparts",
            "arrow_count",
            "key_count",
            "trap_count",
            "dead",
            "pub_date",
            "death_date",
            "deathnote",
        )


class RoomVisitSerializer(serializers.ModelSerializer):
    character = OccupantSerializer()

    class Meta:
        model = models.Visit
        fields = ("character", "stamp_date", "died_here")


class CharacterTestChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CharacterTestChoice
        fields = (
            "choice",
            "pk",
        )


class CharacterTestSerializer(serializers.ModelSerializer):
    choices = CharacterTestChoiceSerializer(many=True)

    class Meta:
        model = models.CharacterTest
        fields = ("question", "choices")

    def create(self, validated_data):
        choices = validated_data.pop("choices", [])
        test = models.CharacterTest.objects.create(**validated_data)
        for choice in choices:
            models.CharacterTestChoice.objects.create(
                choice=choice["choice"], character_test=test
            )
        return test


class NonPlayerCharacterDeathSerializer(serializers.ModelSerializer):
    npc_name = serializers.StringRelatedField(source="npc")
    killer_name = serializers.StringRelatedField(source="killer")
    room_name = serializers.StringRelatedField(source="room")

    class Meta:
        model = models.NonPlayerCharacterDeath
        fields = ("npc_name", "killer_name", "room_name", "stamp_date")
