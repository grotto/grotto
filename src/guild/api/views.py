from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, mixins, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from grotto.api.serializers import NullSerializer, TableauSerializer
from grotto.api.views import TableauSerializerMixin
from grotto.services import GrottoGameWarning
from guild.api.serializers import (
    CharacterSerializer,
    CharacterTestSerializer,
    NonPlayerCharacterSerializer,
    NonPlayerCharacterDeathSerializer,
)
from guild.models import (
    Character,
    CharacterTest,
    NonPlayerCharacterDeath,
    NonPlayerCharacter,
    Visit,
)
from guild.services import PlayerCharacterService
from maze.api.serializers import CharacterRoomVisitSerializer


class CharacterViewSet(
    TableauSerializerMixin,
    viewsets.GenericViewSet,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
):
    serializer_class = CharacterSerializer
    queryset = Character.objects.all()
    filterset_fields = ("dead", "user")

    @swagger_auto_schema(responses={200: TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=NullSerializer,
    )
    def become(self, request, pk):
        character = None
        try:
            character = Character.objects.get(user=request.user, pk=pk)
        except Character.DoesNotExist:
            raise GrottoGameWarning("Character doesn't exist")
        if character.dead:
            raise GrottoGameWarning("Cannot become dead character")
        PlayerCharacterService().enter(character=character)
        request.user.character = character
        request.user.save()
        request.character = character
        return Response(self.get_tableau_serializer().data)

    @action(
        detail=False,
        methods=["post"],
        serializer_class=NullSerializer,
    )
    def new(self, request):
        _service = PlayerCharacterService()
        try:
            character = _service.create(user=request.user)
        except _service.PartyFullException:
            return Response({"error": "Too many characters!"})
        serializer = CharacterSerializer(character)
        return Response(serializer.data)


class CharacterTestViewSet(
    viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin
):
    serializer_class = CharacterTestSerializer
    queryset = CharacterTest.objects.all().order_by("?")

    def perform_create(self, serializer):
        # print(self.request.user)
        serializer.save(creator=self.request.user)


class ScoreboardView(generics.ListAPIView):
    serializer_class = NonPlayerCharacterDeathSerializer
    queryset = NonPlayerCharacterDeath.objects.all().order_by("-stamp_date")


class NonPlayerCharacterViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    serializer_class = NonPlayerCharacterSerializer
    queryset = NonPlayerCharacter.objects.filter(room__isnull=False)


class CharacterRoomVisitViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = CharacterRoomVisitSerializer

    def get_queryset(self):
        qs = Visit.objects.filter(character=self.request.character).order_by(
            "-stamp_date"
        )
        return qs
