from django.urls import path
from rest_framework.routers import DefaultRouter

from guild.api import views

app_name = "guild-api"

router = DefaultRouter()
router.register(
    "character_tests", views.CharacterTestViewSet, basename="character_test"
)
router.register("characters", views.CharacterViewSet, basename="character")
router.register(
    "character_visits", views.CharacterRoomVisitViewSet, basename="character_visits"
)
router.register("npcs", views.NonPlayerCharacterViewSet, basename="npcs")

urlpatterns = router.urls

urlpatterns += [
    path("scoreboard/", views.ScoreboardView.as_view()),
]
