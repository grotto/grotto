from math import ceil

from django.core.management.base import BaseCommand

from guild.models import AbstractNonPlayerCharacter, NonPlayerCharacter
from guild.services import NonPlayerCharacterService
from maze.models import Room


class Command(BaseCommand):
    def handle(self, *args, **options):
        service = NonPlayerCharacterService()
        for abstract in AbstractNonPlayerCharacter.objects.all():
            target_room_count = Room.objects.filter(
                **abstract.spawning_room_level_kwargs()
            ).count()
            npc_count = ceil(abstract.spawn_rate * target_room_count / 100)
            npc_count = max(npc_count, abstract.min_cardinality)
            # ensure correct number of npcs exist
            for _ in range(abstract.instances.count(), npc_count):
                service.create(abstract_npc=abstract)
