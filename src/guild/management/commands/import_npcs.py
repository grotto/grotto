from grotto.management.base.csv import CsvImportCommand
from guild.models import AbstractNonPlayerCharacter


class Command(CsvImportCommand):
    model_class = AbstractNonPlayerCharacter
    boolean_fields = (
        "demonic",
        "mortal",
        "hostile",
        "deadly",
        "hunter",
        "thief",
        "factotem",
        "incorporeal",
        "trap_resistant",
        "mobile_up",
        "mobile_down",
        "appears_below",
    )
