from django.contrib import admin

from guild.models import (
    AbstractNonPlayerCharacter,
    Character,
    CharacterTest,
    CharacterTestChoice,
    NonPlayerCharacter,
    Visit,
)


class CharacterTestChoiceInline(admin.StackedInline):
    model = CharacterTestChoice


@admin.register(CharacterTest)
class CharacterTestAdmin(admin.ModelAdmin):
    list_display = ("question",)
    inlines = (CharacterTestChoiceInline,)


@admin.register(Visit)
class VisitAdmin(admin.ModelAdmin):
    list_display = ("character", "room", "stamp_date")


class NonPlayerCharacterInline(admin.StackedInline):
    model = NonPlayerCharacter


@admin.register(AbstractNonPlayerCharacter)
class AbstractNonPlayerCharacterAdmin(admin.ModelAdmin):
    list_display = ("name", "ref", "level", "appears_below", "mobile", "deadly")
    # inlines = (NonPlayerCharacterInline,)


@admin.register(NonPlayerCharacter)
class NonPlayerCharacterAdmin(admin.ModelAdmin):
    list_display = ("name", "abstract_npc", "room", "room_level")

    @admin.display(ordering="room__level")
    def room_level(self, obj):
        if obj.room is None:
            return None
        return obj.room.level


@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "dead", "room")
    list_filter = ("user", "dead")
