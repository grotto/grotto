from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from grotto.views import ActionMixin
from guild.models import Character


class CharacterDetailView(ActionMixin, DetailView):
    model = Character
    template_name = "character.html"
    query_pk_and_slug = True
    actions = [
        {
            "url": "/rooms/",
            "text": "Return to map",
        },
    ]

    def get(self, request, *args, **kwargs):
        self.character_pk = kwargs["pk"]
        self.is_players_character = False
        if not request.user.is_anonymous:
            self.is_players_character = Character.objects.filter(
                user=request.user, id=kwargs["pk"]
            ).exists()
        return super().get(request, *args, **kwargs)


class CryptView(ActionMixin, ListView):
    template_name = "crypt.html"
    model = Character
    actions = [
        {
            "url": "/rooms/",
            "text": "Return to map",
        }
    ]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(dead=True)
        return queryset
