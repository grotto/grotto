# Generated by Django 3.2.15 on 2022-10-09 02:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("guild", "0011_nonplayercharacter_hunter"),
    ]

    operations = [
        migrations.AddField(
            model_name="nonplayercharacter",
            name="incorporeal",
            field=models.BooleanField(default=False),
        ),
    ]
