# Generated by Django 3.2.15 on 2022-11-21 03:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('guild', '0015_auto_20221119_0916'),
    ]

    operations = [
        migrations.AddField(
            model_name='nonplayercharacter',
            name='greeting',
            field=models.CharField(blank=True, max_length=280, null=True),
        ),
        migrations.AlterField(
            model_name='nonplayercharacter',
            name='description',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
