import datetime

from django.conf import settings
from django.db import models
from django.utils import timezone

from foundry.enum import ItemType
from foundry.helpers import ItemStackingHelper
from maze.enum import RoomRelationEnum
from maze.models import Cenotaph, Room


class NamedModel(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Character(NamedModel):
    kind = models.CharField(max_length=200)
    description = models.TextField()
    pub_date = models.DateTimeField("date created", default=timezone.now)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="characters",
    )
    room = models.ForeignKey(
        Room,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="occupants",
    )
    immortal = models.BooleanField(default=False)
    death_date = models.DateTimeField(null=True, blank=True)
    dead = models.BooleanField(default=False)
    deathnote = models.CharField(max_length=200, null=True, blank=True)
    poop_counter = models.IntegerField(default=0)
    poop_threshold = models.IntegerField(default=500)
    greeting = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    @property
    def arrow_count(self):
        return self.inventory.filter(abstract_item__item_type=ItemType.ARROW).count()

    @property
    def shield_count(self):
        return self.inventory.filter(abstract_item__item_type=ItemType.SHIELD).count()

    @property
    def trap_count(self):
        return self.inventory.filter(abstract_item__item_type=ItemType.TRAP).count()

    @property
    def key_count(self):
        return self.inventory.filter(abstract_item__item_type=ItemType.AMULET).count()

    @property
    def stacked_inventory(self):
        return ItemStackingHelper().stack_items(self.inventory.filter(equipped=False))

    @property
    def stacked_bodyparts(self):
        return ItemStackingHelper().stack_items(self.inventory.filter(equipped=True))

    def skill_modifier(self):
        return sum([s.level for s in self.skills.all()])


class CharacterMessage(models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="messages"
    )
    text = models.CharField(max_length=256)
    seen = models.BooleanField(default=False)


class AbstractNonPlayerCharacter(NamedModel):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    mobile = models.BooleanField(
        default=True,
        help_text="May move around maze (possibly subject to room level constraints",
    )
    deadly = models.BooleanField(
        default=True,
        help_text="Will kill player characters on contact",
    )
    hostile = models.BooleanField(
        default=False,
        help_text="May kill player before npc moves",
    )
    mortal = models.BooleanField(
        default=True,
        help_text="Can die (may respawn)",
    )
    demonic = models.BooleanField(
        default=False,
        help_text="Makes rooms cursed, cannot move into sanctified rooms",
    )
    hunter = models.BooleanField(
        default=False,
        help_text="Will chase player characters (not yet implemented)",
    )
    thief = models.BooleanField(
        default=False,
        help_text=(
            "Will steal an item from a player in the same room before "
            "moving (not implemented)"
        ),
    )
    incorporeal = models.BooleanField(
        default=False,
        help_text="May pass through locked doors",
    )
    trap_resistant = models.BooleanField(
        default=False, help_text="Passes through traps unscathed"
    )
    factotem = models.BooleanField(
        default=False, help_text="Checks cenotaph activation criteria"
    )

    level = models.IntegerField(null=True, blank=True)
    appears_below = models.BooleanField(default=True)
    mobile_up = models.BooleanField(default=False)
    mobile_down = models.BooleanField(default=False)

    spawn_rate = models.PositiveSmallIntegerField(
        default=1,
        help_text="Represents the number of such NPCs that will spawn per 100 rooms",
    )
    min_cardinality = models.PositiveSmallIntegerField(
        default=0,
        help_text=(
            "Minimum number of such NPCs that will exist in the maze "
            "(0 indicates each killed NPC respawns)"
        ),
    )
    name_template = models.CharField(
        max_length=250,
        default="{abstract_npc_name}",
        help_text=(
            "You can use patterns like '{adjective}', '{animal}', "
            "'{substance}', and '{elaborate_color}' to pull random words from "
            "the words lists in the 'src/words_lists' directory; and "
            "'{abstract_item_name}'"
        ),
    )

    movement_threshold = models.IntegerField(
        default=100,
        help_text="When Movement Entropy exceeds this threshold the NPC will move",
    )
    warning_text = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    greeting = models.CharField(max_length=140, null=True, blank=True)
    icon_data = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        default="121",
    )

    def spawning_room_level_kwargs(self):
        if self.level is None:
            return {}
        if self.appears_below:
            return {"level__gte": self.level}
        return {"level": self.level}


class NonPlayerCharacter(NamedModel):
    room = models.ForeignKey(
        Room, on_delete=models.SET_NULL, related_name="npcs", null=True, blank=True
    )
    abstract_npc = models.ForeignKey(
        AbstractNonPlayerCharacter,
        on_delete=models.CASCADE,
        related_name="instances",
        null=True,
    )
    description = models.CharField(max_length=500, null=True, blank=True)
    movement_entropy = models.IntegerField(
        default=0, help_text="See Movement Threshold"
    )
    greeting = models.CharField(max_length=280, null=True, blank=True)

    def __getattr__(self, attr):
        if hasattr(self.abstract_npc, attr):
            return getattr(self.abstract_npc, attr)
        raise AttributeError(f"Attribute {attr} not found")

    @property
    def accessible_room_relations(self):
        relations = [RoomRelationEnum.SAME, RoomRelationEnum.PORTAL]
        if self.mobile_up:
            relations.append(RoomRelationEnum.UP)
        if self.mobile_down:
            relations.append(RoomRelationEnum.DOWN)
        return relations


class Skill(NamedModel):
    level = models.IntegerField()
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="skills"
    )


class CharacterTest(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    question = models.CharField(max_length=2000)


class CharacterTestChoice(models.Model):
    character_test = models.ForeignKey(
        CharacterTest, on_delete=models.CASCADE, related_name="choices"
    )
    choice = models.CharField(max_length=400)


class RoomWarning(models.Model):
    text = models.CharField(max_length=128)


class Visit(models.Model):
    room = models.ForeignKey(
        Room, on_delete=models.SET_NULL, null=True, related_name="_visits"
    )
    character = models.ForeignKey(
        Character, on_delete=models.SET_NULL, null=True, related_name="visits"
    )
    stamp_date = models.DateTimeField("date created", default=timezone.now)
    died_here = models.BooleanField(default=False)
    warnings = models.ManyToManyField(RoomWarning)

    class Meta:
        get_latest_by = ("stamp_date",)


class Kneel(models.Model):
    character = models.ForeignKey(Character, models.CASCADE, related_name="kneels")
    cenotaph = models.ForeignKey(
        Cenotaph, on_delete=models.CASCADE, related_name="kneels"
    )
    created = models.DateTimeField(auto_now_add=True)


class NonPlayerCharacterDeath(models.Model):
    npc = models.ForeignKey(
        NonPlayerCharacter, on_delete=models.PROTECT, related_name="deaths"
    )
    room = models.ForeignKey(Room, on_delete=models.PROTECT, related_name="npc_deaths")
    killer = models.ForeignKey(
        Character,
        on_delete=models.PROTECT,
        related_name="npc_kills",
        null=True,
        blank=True,
    )
    stamp_date = models.DateTimeField(default=timezone.now)


class Event(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name="events")
    character = models.ForeignKey(
        Character,
        on_delete=models.CASCADE,
        related_name="events",
        null=True,
        blank=True,
    )
    created = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=256)

    class Meta:
        ordering = ["-created"]
