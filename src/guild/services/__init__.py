import re
from random import choice, randint

from django.db.models import Max
from django.utils.timezone import now

from account.services import ban
from foundry.enum import ItemType
from foundry.models import AbstractItem, PlacedTrap
from foundry.services import ItemService
from grotto.models import StartingRoom, BannedWord
from grotto.services import (
    escape,
    BaseGenerationService,
    GrottoGameWarning,
    ServiceReturn,
)
from guild.models import (
    Character,
    CharacterMessage,
    Event,
    Kneel,
    NonPlayerCharacter,
    NonPlayerCharacterDeath,
    RoomWarning,
    Skill,
    Visit,
)
from guild.services.generation import CharacterGeneratorService
from maze.models import Room, RoomExit
from maze.services import CenotaphActivationService


class NonPlayerCharacterService:
    pass


class TrapMixin:
    def trap_placements(self, *, near_room, far_room):
        # look for traps
        visible_traps = list(
            PlacedTrap.objects.filter(
                exit__from_room=near_room, exit__to_room=far_room, removed=None
            )
        )
        hidden_traps = list(
            PlacedTrap.objects.filter(
                exit__to_room=near_room, exit__from_room=far_room, removed=None
            )
        )
        return visible_traps + hidden_traps


class PlayerCharacterService(TrapMixin):
    default_item_types = (
        (ItemType.CANDLE, {"name": "Candle"}),
        (ItemType.INCENSE,),
        (ItemType.JUNK,),
        (ItemType.ARROW,),
    )

    class PartyFullException(Exception):
        pass

    def _roll(self, *, d=20):
        return randint(1, 20)

    def create(self, *, user):
        # use generator service to
        _excludes = user.characters.filter(dead=False).values_list("kind", flat=True)
        _service = CharacterGeneratorService()
        try:
            char = _service.generate(exclude=_excludes)
        except _service.NoAvailableClasses:
            raise self.PartyFullException("no available classes")

        skills = char.pop("skills")
        body_parts = char.pop("body_parts")
        item_types = char.pop("item_types")
        character = Character.objects.create(
            user=user,
            **char,
        )
        for skill, level in skills.items():
            Skill.objects.create(
                name=skill,
                level=level,
                character=character,
            )

        _item_types = list(self.default_item_types)
        _item_types.extend(item_types)
        for item_type, *opts in _item_types:
            if not opts:
                opts = [{}]
            try:
                _abstract = choice(
                    list(AbstractItem.objects.filter(item_type=item_type, **opts[0]))
                )
            except IndexError:
                pass
            else:
                ItemService().create(abstract_item=_abstract, character=character)
        for body_part in body_parts:
            _abstract = AbstractItem.objects.get(
                name=body_part,
                item_type=ItemType.BODYPART,
            )
            item = ItemService().create(abstract_item=_abstract, character=character)
            ItemService().equip(item=item, character=character)
        return character

    def kill(self, *, character, deathnote, **kwargs):
        if character.immortal:
            return ServiceReturn()
        character.dead = True
        character.death_date = now()
        character.deathnote = deathnote
        # leave a "tombstone"
        Visit.objects.create(room=character.room, character=character, died_here=True)
        # character drops all their stuff
        character.inventory.all().update(
            room=character.room,
            character=None,
            equipped=False,
        )
        # remove character from the room
        character.room = None
        character.save()
        return ServiceReturn(messages=(deathnote,))

    def enter(self, *, character):
        if character is None:
            # no character
            return
        if character.room is not None:
            # character already in game
            return
        try:
            _starting = StartingRoom.objects.all().latest()
        except StartingRoom.DoesNotExist:
            character.room_id = choice(
                list(
                    Room.objects.exclude(npcs__deadly=True).values_list("pk", flat=True)
                )
            )
        else:
            character.room_id = _starting.room_id
        character.save()

    def move(self, *, character, raises=GrottoGameWarning, **room_kwargs):
        if character.room is None:
            raise raises("Character is not in grotto")
        old_room = character.room
        try:
            room = old_room.exits.get(**room_kwargs)
        except Room.DoesNotExist:
            raise raises("Room is not accessable")
        try:
            RoomExit.objects.get(
                from_room=old_room,
                to_room=room,
                locked=False,
            )
        except RoomExit.DoesNotExist:
            return ServiceReturn(messages=("That door is locked!",))
        except RoomExit.MultipleObjectsReturned:
            # A portal may create a duplicate door.
            pass
        character.room = room
        character.save()
        visit = Visit.objects.create(room=old_room, character=character)
        for warning in old_room.warnings_for(character):
            room_warning, _ = RoomWarning.objects.get_or_create(text=warning)
            visit.warnings.add(room_warning)

        killers = list(room.npcs.filter(abstract_npc__deadly=True))
        if killers:
            killer = choice(killers)
            Event.objects.create(
                room=room,
                text=(f"{character.name} walked in on the {killer.name} and has died"),
                character=character,
            )
            return self.kill(
                character=character,
                deathnote=f"{character.name} was killed by {killer.name}",
            )

        # look for traps
        skill_modifier = character.skill_modifier()
        for trap_placement in self.trap_placements(near_room=old_room, far_room=room):
            # roll to save
            save = (self._roll(d=100) + skill_modifier) > 50
            if save:
                # disarm the trap
                trap_placement.item.active = None
                trap_placement.item.save()
                trap_placement.removed = now()
                trap_placement.save()
            else:
                # spring the trap
                ItemService().spring_trap(trap_placement)
                # kill the character
                return self.kill(
                    character=character,
                    deathnote=f"{character.name} stepped on a trap and died",
                )
                Event.objects.create(
                    room=trap_placement.item.room,
                    character=character,
                    text=f"{character.name} stepped on a trap and died",
                )
        return ServiceReturn()

    def kneel(self, *, character):
        """This is a deprecated code path"""
        service_return = ServiceReturn()
        # find room
        room = character.room
        if room is None:
            return service_return
        cenotaph = None
        message = f"{character} knelt"
        if room.cleanliness != 2:
            message += " and got their knees dirty"
        # add Kneel record
        if hasattr(room, "cenotaph"):
            cenotaph = room.cenotaph
            Kneel.objects.create(character=character, cenotaph=cenotaph)
            message += f" at {cenotaph.name}'s cenotaph"

        message += f" in the {room.name}"
        # add room message
        Event.objects.create(
            room=room,
            text=message,
            character=character,
        )
        service_return.messages.append(message)
        return service_return

    def place_trap(self, *, character, raises=GrottoGameWarning, **room_kwargs):
        if character.room is None:
            raise raises("Character is not in grotto")
        # check that character has an arrow
        if character.trap_count <= 0:
            raise raises("You don't have any traps")
        trap = character.inventory.filter(abstract_item__item_type=ItemType.TRAP)[0]
        try:
            target_room = character.room.exits.get(**room_kwargs)
        except Room.DoesNotExist:
            raise raises("Room is not accessable")
        try:
            exit = RoomExit.objects.get(
                from_room_id=character.room.id,
                to_room_id=target_room.id,
            )
        except RoomExit.DoesNotExist:
            raise raises("Room is not accessable")
        service_return = ServiceReturn()
        ItemService().place_trap(item=trap, exit=exit, character=character)
        service_return.messages.append(
            f"You have trapped the exit to the {target_room}"
        )
        return service_return

    def toggle_lock(self, *, character, raises=GrottoGameWarning, **room_kwargs):
        if character.room is None:
            raise raises("Character is not in grotto")
        # check that character has an arrow
        if character.key_count <= 0:
            raise raises("You don't have any keys")
        key = character.inventory.filter(
            abstract_item__item_type=ItemType.AMULET
        ).first()
        try:
            target_room = character.room.exits.get(**room_kwargs)
        except Room.DoesNotExist:
            raise raises("Room is not accessable")
        try:
            exit = RoomExit.objects.get(
                from_room_id=character.room.id,
                to_room_id=target_room.id,
            )
            _exit = RoomExit.objects.get(
                to_room_id=character.room.id,
                from_room_id=target_room.id,
            )
        except RoomExit.DoesNotExist:
            raise raises("Room is not accessable")
        service_return = ServiceReturn()
        locked = ItemService().toggle_lock(
            item=key, exits=(exit, _exit), character=character
        )
        verb = "unlocked"
        if locked:
            verb = "locked"
        if "skeleton" not in key.name:
            if randint(0, 3) == 0:
                service_return.messages.append("*clink* your key broke")
                self.destroy(item)
        service_return.messages.append(f"You have {verb} the exit to the {target_room}")
        return service_return

    def bash(self, *, character, other, raises=GrottoGameWarning):
        shield = character.inventory.filter(
            abstract_item__item_type=ItemType.SHIELD
        ).first()
        if shield is None:
            raise raises("You don't have a shield")
        service_return = ServiceReturn()
        if "durable" not in shield.name:
            if randint(0, 3) == 0:
                service_return.messages.append("*crack* your shield broke")
                ItemService().break_shield(shield)
        if isinstance(other, (Character, NonPlayerCharacter)):
            return self._bash_character(
                character=character,
                other=other,
                raises=raises,
                return_obj=service_return,
            )
        if isinstance(other, (Room)):
            return self._bash_door(
                character=character,
                room=other,
                raises=raises,
                return_obj=service_return,
            )

    def _bash_door(self, *, character, room, raises, return_obj):
        try:
            exit = RoomExit.objects.get(
                from_room=character.room,
                to_room=room,
                locked=True,
            )
            _exit = RoomExit.objects.get(
                from_room=room,
                to_room=character.room,
                locked=True,
            )
        except RoomExit.DoesNotExist:
            raise raises("Door is not bashable")
        if randint(0, 1) == 0:
            ItemService().toggle_lock(
                item=None, exits=(exit, _exit), character=character, locked=False
            )
            return_obj.messages.append(f"Door to {room} has been opened")
        else:
            return_obj.messages.append(f"The door to {room} was unfazed by your blow")
        return return_obj

    def _bash_character(self, *, character, other, raises, return_obj):
        _other = str(other)
        _character = str(character)
        if character.room is None:
            raise raises("Character is not in grotto")
        # check that character has an arrow
        if other.room != character.room:
            raise raises(f"You are not in the same room as {_other}")
        if getattr(other, "incorporeal", False):
            raise raises(f"{_other} cannot be bashed")
        if character == other:
            _other = "yourself"
            _character = "yourself"
        try:
            exit = choice(
                list(
                    RoomExit.objects.filter(
                        from_room=character.room,
                        locked=False,
                    )
                )
            )
        except IndexError:
            # kill that mf
            return_obj.messages.append(
                f"Your bashed {_other} to smithereens against the wall"
            )
            if isinstance(other, NonPlayerCharacter):
                NonPlayerCharacterService().kill(npc=other, killer=character)
            else:
                self.kill(
                    character=other, deathnote=f"Bashed against a wall by {_character}"
                )
        else:
            return_obj.messages.append(f"You bashed {_other} into the {exit.to_room}")
            other.room = exit.to_room
            other.save()
        # chance that your shield breaks
        return return_obj

    def fire_arrow(self, *, character, raises=GrottoGameWarning, **room_kwargs):
        if character.room is None:
            raise raises("Character is not in grotto")
        # check that character has an arrow
        if character.arrow_count <= 0:
            raise raises("You don't have any arrows")
        # check that the room being fired into is adjancent to character room
        try:
            target_room = character.room.exits.get(**room_kwargs)
        except Room.DoesNotExist:
            raise raises("Room is not accessable")
        try:
            exit = character.room.exits_from.get(to_room_id=target_room.id)
        except RoomExit.DoesNotExist:
            raise raises("Room is not accessable")
        source_room = character.room
        arrow = character.inventory.filter(abstract_item__item_type=ItemType.ARROW)[0]
        arrow.character = None
        service_return = ServiceReturn()
        #
        if exit.locked:
            service_return.messages.append(
                f"Your arrow bounced off the {target_room} door."
            )
            target_room = source_room
        else:
            for trap_placement in self.trap_placements(
                near_room=source_room, far_room=target_room
            ):
                # arrow dies
                ItemService().destroy(arrow)
                # trap dies
                ItemService().spring_trap(trap_placement)
                return service_return

        arrow.room = target_room
        # see what is in room (wumpus or player character or nothing)
        occupants = list(target_room.occupants.all())
        npcs = list(target_room.npcs.filter(abstract_npc__mortal=True))
        unlucky = None
        protected = None
        if npcs:
            unlucky = choice(npcs)
            service_return.combine(
                NonPlayerCharacterService().kill(npc=unlucky, killer=character)
            )
            service_return.messages.append(f"Your arrow killed the {unlucky}")
        elif occupants:
            unlucky = choice(occupants)
            # check for shield item
            shield = unlucky.inventory.filter(
                abstract_item__item_type=ItemType.SHIELD
            ).first()
            if shield is None:
                # kill unlucky
                service_return.combine(
                    self.kill(
                        character=unlucky,
                        deathnote=(
                            f"{unlucky.name} was killed by an arrow from "
                            f"the {source_room}"
                        ),
                    )
                )
                character.refresh_from_db()  # in case you shot yourself.
                service_return.messages.append(f"Your arrow killed {unlucky}")
            else:
                # hit character doesn't die
                protected = unlucky
                unlucky = None
                # hit character gets the arrow
                arrow.character = protected
                arrow.room = None
                # hit character loses their shield
                service_return.messages.append(f"Your arrow hit {protected}'s shield")
                if ItemService().break_shield(shield):
                    CharacterMessage.objects.create(
                        character=protected,
                        text=(
                            f"An arrow from the {source_room.name} destroyed "
                            "your shield"
                        ),
                    )
                else:
                    CharacterMessage.objects.create(
                        character=protected,
                        text=(
                            f"An arrow from the {source_room.name} shatters "
                            "against your magic shield"
                        ),
                    )

        if unlucky is not None:
            Event.objects.create(
                room=target_room,
                text=(
                    f"{character.name} fired an arrow from the {source_room.name} "
                    f"and killed {unlucky.name}"
                ),
                character=character,
            )
        if protected is not None:
            Event.objects.create(
                room=target_room,
                text=(
                    f"{character.name} fired an arrow from the {source_room.name} "
                    f"and hit {protected.name}'s shield."
                ),
                character=character,
            )

        arrow.save()
        return service_return

    def set_greeting(self, *, character, message):
        service_return = ServiceReturn()
        _tokens = re.split(r"\W+", message.lower())
        if BannedWord.objects.filter(word__in=_tokens).exists():
            # log out the user; ban the account.
            ban(character.user, reason=f"wrote note: '{message}'")
            service_return.messages.append("goodbye")
            return service_return
        character.greeting = escape(message)
        character.save()
        service_return.messages.append("You have altered your character greeting")
        return service_return


class NonPlayerCharacterGenerationService(BaseGenerationService):
    def generate(self, abstract_npc):
        return {
            "name": self._enrich_template(
                template=abstract_npc.name_template,
                abstract_npc_name=abstract_npc.name,
            ),
            "description": self._enrich_template(
                template=abstract_npc.description,
            ),
            "greeting": self._enrich_template(
                template=abstract_npc.greeting,
            ),
        }


class NonPlayerCharacterService(TrapMixin):  # noqa: F811
    def create(self, *, abstract_npc):
        npc = NonPlayerCharacter.objects.create(
            abstract_npc=abstract_npc,
            **NonPlayerCharacterGenerationService().generate(abstract_npc),
        )
        self.move(npc=npc, room="random")
        return npc

    def _choose_future_room(self, *, npc, room):
        if isinstance(room, (Room,)):
            return room

        current = npc.room
        # allow NPC to stay in their current room
        future = current

        if current is None or room == "random":
            qs = Room.objects.filter(**npc.spawning_room_level_kwargs())
            if npc.demonic:
                qs = qs.dark().sanctified(exclude=True)
        else:
            qs = current.exits.filter(
                exits_from__direction__in=npc.accessible_room_relations,
            )
            if not npc.incorporeal:
                qs = qs.filter(exits_from__locked=False)
            if npc.demonic:
                qs = qs.sanctified(exclude=True)
        try:
            future = choice(qs)
        except IndexError:
            pass
        return future

    def _resolve_movement(self, *, npc):
        ret = ServiceReturn()
        # sully the room
        if npc.demonic and npc.room:
            to_be_cursed = npc.room.items.filter(cursed=False).first()
            if randint(0, 1) == 0 and to_be_cursed is not None:
                to_be_cursed.cursed = True
                to_be_cursed.save()
            npc.room.is_cursed = True
            npc.room.save()
        # does it swap?
        ItemService()._perform_swaps(npc=npc)
        # does it kill?
        self._kill_nearby_characters(npc=npc)
        if npc.factotem and npc.room is not None:
            # check room activation criteria
            CenotaphActivationService().check_activation(npc.room)
        return ret

    def _kill_character(self, *, npc, character, return_obj=None):
        Event.objects.create(
            room=npc.room,
            text=(f"{npc.name} walked in on {character.name} and ate them"),
            character=character,
        )
        kill_ret = PlayerCharacterService().kill(
            character=character,
            deathnote=f"{character.name} was killed by {npc.name}",
        )
        if return_obj is not None:
            return_obj.combine(kill_ret)

    def _kill_nearby_characters(self, *, npc, return_obj=None):
        if not npc.deadly or npc.room is None:
            return
        # kill any player characters in the room
        for unlucky in npc.room.occupants.all():
            self._kill_character(npc=npc, character=unlucky, return_obj=return_obj)

    def move(self, *, npc, room="adjacent"):
        # pre-movement checks
        if npc.hostile and npc.room is not None:
            # target player character who has been in the room the longest
            unlucky = (
                npc.room.occupants.annotate(last_move=Max("visits__stamp_date"))
                .order_by("-last_move")
                .first()
            )
            if unlucky:
                self._kill_character(npc=npc, character=unlucky)
        # npc moves
        # choose a destination
        future_room = self._choose_future_room(npc=npc, room=room)

        # TODO: is NPC a thief?

        # look for traps
        npc.movement_entropy = 0
        if not npc.trap_resistant:
            for trap_placement in self.trap_placements(
                near_room=npc.room, far_room=future_room
            ):
                # spring the trap
                npc.room = trap_placement.item.room
                ItemService().spring_trap(trap_placement)
                # kill the character
                return self.kill(
                    npc=npc,
                    killer=None,
                )
        npc.room = future_room
        npc.save()
        return self._resolve_movement(npc=npc)

    def kill(self, *, npc, killer, **kwargs):
        # transfer loot to room
        item_service = ItemService()
        for abstract_item in npc.abstract_loot.all():
            item_service.create(abstract_item=abstract_item, room=npc.room)
        NonPlayerCharacterDeath.objects.create(
            npc=npc,
            room=npc.room,
            killer=killer,
        )
        # npc is dead!
        npc.room = None
        npc.save()
        # npc may respawn
        _abstract = npc.abstract_npc
        living_count = _abstract.instances.filter(room__isnull=False).count()
        if living_count < _abstract.min_cardinality:
            # npc will be placed in random room
            return self.move(npc=npc, room="random")
        return ServiceReturn()
