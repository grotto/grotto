from random import choice, choices, randint

from foundry.enum import ItemType
from grotto.services import BaseGenerationService


class BaseCharacterGenerationService(BaseGenerationService):
    kind = "unknown"
    selection_weight = 2
    skills_count = 4
    poop_threshold = 1000
    item_types = ()
    body_parts = ()


class GhostCharacterGeneration(BaseCharacterGenerationService):
    kind = "Ghost"
    name_list = "word_lists/ghost_types.txt"
    description_corpus = "word_lists/corpuses/ghost_corpus.txt"
    description_sentences = (
        ("", ()),
        ("", ()),
        ("", ()),
    )
    body_parts = ("Spectral Organ", "Eyes")
    poop_threshold = 2000

    def description(self, *, name=None):
        return f"{self.add_a_or_an(self.adjective())} spirit. {self._description()}"


class RobotCharacterGeneration(BaseCharacterGenerationService):
    kind = "Robot"
    name_list = "word_lists/robot_names.txt"
    description_corpus = "word_lists/corpuses/robot_corpus.txt"
    description_sentences = (
        ("", ()),
        ("", ()),
        ("", ()),
        ("", ()),
    )
    body_parts = ("Sensor", "Eyes")
    poop_threshold = 1600
    item_types = (
        (ItemType.SCRUBBRUSH,),
        (ItemType.SHIELD,),
    )

    def name(self):
        return f"{super().name()}-{randint(100,1000)}"

    def description(self, *, name=None):
        return f"A hard working robot. {self._description()}"


class BirdCharacterGeneration(BaseCharacterGenerationService):
    kind = "Bird"
    name_list = "word_lists/bird_names.txt"
    description_corpus = "word_lists/corpuses/bird_corpus.txt"
    description_sentences = (
        ("", ()),
        ("", ()),
        ("", ()),
    )
    markovify_newline = True
    poop_threshold = 100
    body_parts = ("Ear", "Eyes")

    def description(self, *, name=None):
        enemy = self.firstname()
        _type = self.animal()
        return (
            f"{self._description()} {name} the bird has been hunted through life "
            f"by its enemy, {enemy}, {self.add_a_or_an(self.adjective())} "
            f"{_type}."
        )


class AnimalCharacterGeneration(BaseCharacterGenerationService):
    kind = "Animal"
    name_list = "word_lists/firstnames.txt"
    description_corpus = "word_lists/corpuses/animal_corpus.txt"
    description_sentences = (
        ("_short", (140,)),
        ("", ()),
        ("", ()),
    )
    body_parts = ("Eyes", "Nose")  # gut?
    description_default = "It hunts daily for food. "
    poop_threshold = 400

    def description(self, *, name=None):
        _type = f"{self.add_a_or_an(self.adjective())} {self.animal()}"
        return f"{_type.capitalize()} {self._description()}"


class HumanCharacterGeneration(BaseCharacterGenerationService):
    kind = "Human"
    selection_weight = 4
    item_types = ((ItemType.SCRUBBRUSH,),)
    body_parts = ("Eyes",)

    def name(self):
        return (
            f"{self.firstname()} " + self.lastname_beginning() + self.lastname_ending()
        )

    def description(self, *, name=None):
        adjectives = []
        adjectives.append(self.adjective(exclude=adjectives))
        adjectives.append(self.adjective(exclude=adjectives))
        return (
            "A bipedal mammal with smooth skin and "
            + self.hair()
            + f" hair on its head. It is wearing {self.add_a_or_an(self.color())} "
            f"{self.wearable()}. "
            f"{name} worked as "
            + self.add_a_or_an(self.career())
            + f" before embarking on their {adjectives[0]} quest to find the "
            f"legendary {self.substance()} obelisk. They are followed by their "
            f"faithful compainion {self.firstname()}"
            f" the {adjectives[1]} "
            f"{self.animal()}. "
        )


# INANIMATE!


class InanimateCharacterGeneration(BaseCharacterGenerationService):
    skills_count = 0
    selection_weight = 1
    poop_threshold = 3000
    body_parts = ()


class ObeliskCharacterGeneration(InanimateCharacterGeneration):
    """The simplest character kind"""

    kind = "Obelisk"
    name_list = "word_lists/obelisk_names.txt"
    # body_parts = ("Evil Eye",)

    """ def name(self):
        return "Nameless obelisk" """

    def description(self, *, name=None):
        return f"A {randint(0, 100)} foot high obelisk made of {self.substance()}."


class FungusCharacterGeneration(InanimateCharacterGeneration):
    kind = "Fungus"
    name_list = "word_lists/fungi_names.txt"
    body_parts = ("Mycelium",)

    def description(self, *, name=None):
        _type = self.fungi_type()
        return f"A colony of {self.adjective()}, {self.color()} {_type}."


class VegetableCharacterGeneration(InanimateCharacterGeneration):
    kind = "Vegetable"
    description_corpus = "word_lists/corpuses/veggie_corpus.txt"
    description_sentences = (
        ("", ()),
        ("", ()),
    )
    body_parts = ("Root",)
    markovify_newline = True

    def name(self):
        return "nameless vegetable"

    def description(self, *, name=None):
        _bin = choice(["pumpkin", "vegetable"])
        _type = getattr(self, _bin)()
        return f"{self.add_a_or_an(self.color())} {_type}. {self._description()}"


class CharacterGeneratorService:
    generation_classes = (
        HumanCharacterGeneration,
        FungusCharacterGeneration,
        VegetableCharacterGeneration,
        AnimalCharacterGeneration,
        BirdCharacterGeneration,
        ObeliskCharacterGeneration,
        GhostCharacterGeneration,
        RobotCharacterGeneration,
    )

    class NoAvailableClasses(Exception):
        pass

    def generate(self, *, generation_class=None, exclude):
        if generation_class is None:
            _classes = [c for c in self.generation_classes if c.kind not in exclude]
            if not _classes:
                raise self.NoAvailableClasses("all classes used up!")
            generation_class = choices(
                _classes,
                weights=(c.selection_weight for c in _classes),
            )[0]
        generator = generation_class()
        name = generator.name()
        return {
            "name": name,
            "kind": generation_class.kind,
            "description": generator.description(name=name),
            "greeting": f"{generation_class.kind} noises",
            "skills": generator.skills(),
            "poop_threshold": generator.poop_threshold,
            "body_parts": generation_class.body_parts,
            "item_types": generation_class.item_types,
        }
