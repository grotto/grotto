from django.urls import path

from . import views

app_name = "guild"
urlpatterns = [
    path("character/<int:pk>/", views.CharacterDetailView.as_view(), name="character"),
    path("crypt/", views.CryptView.as_view(), name="crypt"),
]
