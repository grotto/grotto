Don't change the name of any file here.

If you add a new word list be sure it is named like f"{singular_noun}s.txt" always use snake case if there is need for a space (e.g. `lastname_endings.txt`).

List of corpuses

* 100 dungeon room descriptions - <https://web.archive.org/web/20161101181215/http://archive.wizards.com/default.asp?x=dnd/ru/20051026a&page=2>
* house search results from Zillow.com
* Archeological Investigations by Gerard Fowke - <https://www.gutenberg.org/ebooks/18931>
* The Great Galveston disaster by Paul Lester - <https://www.gutenberg.org/ebooks/60105>
* The Cathedrals of Great Britain, by P. H. Ditchfield - <https://www.gutenberg.org/files/43402/43402-h/43402-h.htm>
* Legends of Old Bohemia by Alois Jirásek (short excerpts)
* Czech Reading Book 1892 by Jan Lepar et al. - <https://www.gutenberg.org/ebooks/59765>
* Czech Fairy Tales - <https://www.gutenberg.org/ebooks/32217>
