import json
import re
from functools import partial
from pathlib import Path
from random import choice, randint

import markovify


class GrottoGameWarning(Exception):
    pass


class ServiceReturn:
    def __init__(self, *, messages=None):
        self.messages = messages or []

    def combine(self, other):
        self.messages += other.messages


class RandomColorService:
    palette_names = ("grotto_colors",)

    def __init__(self):
        self.__palettes = {}
        self._usable_palettes = list(self.palette_names)
        self._used_colors = set()

    def get_color(self):
        count = 0
        while True:
            if count > 25:
                raise Exception("Not enough colors")
            count += 1
            color, _hex = self._get_color()
            if color not in self._used_colors:
                self._used_colors.add(color)
                return color, _hex

    def _get_color(self):
        palette_name = choice(self._usable_palettes)
        palette = self._get_palette(palette_name)
        color, _hex = choice(list(palette.items()))
        del self.__palettes[palette_name][color]
        if not self.__palettes[palette_name]:
            self._usable_palettes.remove(palette_name)
        return color, _hex

    def _repack_palette(self, palette):
        return {p["color"]: p["hex"] for p in palette}

    def _get_palette(self, palette_name):
        if palette_name in self.__palettes:
            return self.__palettes[palette_name]
        with open(f"word_lists/json/colors/{palette_name}.json") as palette_file:
            _palette = self._repack_palette(json.load(palette_file)["colors"])
        self.__palettes[palette_name] = _palette
        return _palette


class BaseGenerationService:
    name_list = "word_list.txt"
    description_corpus = "word_list.txt"
    markovify_newline = False
    description_sentences = (("_short", (80,)),)
    description_default = "It is very mysterious. "

    def _text_model(self, *, word_list=None, newline=False):
        with open(word_list) as f:
            text = f.read()
        if newline:
            base = markovify.NewlineText(text)
        else:
            base = markovify.Text(text)
        return base.compile()

    def _choose_from_file(self, name_list, *, exclude=None):
        if exclude is None:
            exclude = []
        with open(name_list) as f:
            choices = f.readlines()
        while (chosen := choice(choices).strip(" \n")) in exclude:
            pass
        return chosen

    def add_a_or_an(self, word):
        if word[-1] != "s":
            if word[0] in "aeiou":
                return f"an {word}"
            else:
                return f"a {word}"
        return word

    def name(self):
        return self._choose_from_file(self.name_list)

    def _description(self, *, name=None, corpus=None):
        _corpus = corpus or self.description_corpus
        text_model = self._text_model(
            word_list=_corpus,
            newline=self.markovify_newline,
        )
        description = ""
        for length_mod, args in self.description_sentences:
            method = getattr(text_model, f"make{length_mod}_sentence")
            try:
                description += method(*args, tries=100) + " "
            except TypeError:
                description += self.description_default
                break
        return description

    def description(self, *, name=None):
        return self._description(name=name)

    def _enrich_template(self, *, template, **kwargs):
        # get desired token types from template
        token_types = set(re.findall(r"\{([^\}]*)\}", template))
        # remove certain tokens
        for key in kwargs:
            token_types.discard(key)
        # get tokens for each desired token type
        tokens = {}
        for token_type in token_types:
            try:
                value = getattr(self, token_type)()
            except AttributeError:
                # handle a missing token type
                value = f"[{token_type}]"
            tokens.update({token_type: value})
        return template.format(
            **kwargs,
            **tokens,
        )

    def _name(self, *, template, **kwargs):
        return self._enrich_template(template=template, **kwargs)

    def __getattr__(self, name):
        word_list = f"word_lists/{name}s.txt"
        if not Path(word_list).exists():
            raise AttributeError
        return partial(self._choose_from_file, word_list)

    def skills(self, skills_count=4):
        if hasattr(self, "skills_count"):
            skills_count = self.skills_count
        skills = {}
        for _ in range(skills_count):
            skills[self.skill()] = randint(-10, 18)
        return skills


def escape(message):
    """Convert angle brackets to html safe"""
    re.sub(">", "&gt;", message)
    re.sub("<", "&lt;", message)
    return message
