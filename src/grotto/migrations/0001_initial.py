# Generated by Django 3.2.15 on 2022-09-03 15:54

from django.db import migrations, models

import grotto.models
import grotto.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="InstanceConfig",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "abstract_items_csv",
                    models.FileField(
                        upload_to=grotto.models.config_file_uploads,
                        validators=[grotto.validators.validate_csv_upload],
                    ),
                ),
                (
                    "collectables_csv",
                    models.FileField(
                        upload_to=grotto.models.config_file_uploads,
                        validators=[grotto.validators.validate_csv_upload],
                    ),
                ),
                (
                    "npcs_csv",
                    models.FileField(
                        upload_to=grotto.models.config_file_uploads,
                        validators=[grotto.validators.validate_csv_upload],
                    ),
                ),
                (
                    "swaps_csv",
                    models.FileField(
                        upload_to=grotto.models.config_file_uploads,
                        validators=[grotto.validators.validate_csv_upload],
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("name", models.CharField(blank=True, max_length=128, null=True)),
                ("imported", models.DateTimeField(blank=True, null=True)),
            ],
        ),
    ]
