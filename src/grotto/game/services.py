from random import choice, randint

from foundry.enum import ItemType
from foundry.services import ItemService
from grotto.services import ServiceReturn
from guild.models import Event, NonPlayerCharacter
from guild.services import NonPlayerCharacterService
from maze.models import Room


class GameService:
    def _roll(self, *, d=20):
        return randint(1, d)

    def roll_to_dirty_room(self, *, room, dice_count=1):
        if room is None:
            return
        make_dirtier = False
        for _ in range(dice_count):
            if self._roll() == 1:
                make_dirtier = True
                break
        if make_dirtier:
            new_cleanliness = max(1, room.cleanliness - 1)
            if new_cleanliness != room.cleanliness:
                room.cleanliness = new_cleanliness
                room.save()
                Event.objects.create(
                    room=room, text=("Room was left dirtier than it was found")
                )
        return ServiceReturn()

    def roll_to_move_npc(self, *, npc, dice_count=1):
        entropy = 0
        # roll appropriate number of dice
        for x in range(dice_count):
            # TODO: handle crits
            entropy += self._roll()
        # add the quantity to the entropy score for every mobile NPC
        npc.movement_entropy += entropy
        npc.save()
        # resolve any NPC movements
        ret = ServiceReturn()
        if npc.movement_threshold <= npc.movement_entropy:
            ret = NonPlayerCharacterService().move(npc=npc)
        return ret

    def roll_to_resolve_npc_action(self, *, level, dice_count=1):
        ret = ServiceReturn()
        for npc in NonPlayerCharacter.objects.filter(room__level=level):
            if npc.mobile:
                ret.combine(self.roll_to_move_npc(npc=npc, dice_count=dice_count))
            elif npc.deadly:
                # deadly and not mobile (e.g. chasm)
                # remove any non-collectable items in the room
                npc.room.items.exclude(
                    abstract_item__item_type=ItemType.COLLECTABLE
                ).update(room=None)
                # relocate any "collectable" items in the room
                for item in npc.room.items.all():
                    # choose a room
                    item.room_id = choice(
                        list(
                            Room.objects.exclude(id=item.room.id).values_list(
                                "pk", flat=True
                            )
                        )
                    )
                    item.save()
        return ret

    def roll_to_character_poop(self, *, character, dice_count=1):
        for x in range(dice_count):
            roll = self._roll()
            if roll == 20:
                # crit gets poop automatically
                character.poop_counter = character.poop_threshold
            character.poop_counter += roll
            if character.poop_counter >= character.poop_threshold:
                character.poop_counter = 0
                ItemService().create_feces(character=character)
        character.save()
