from django.urls import path

from grotto.game import views

app_name = "game"

urlpatterns = [
    path("play/", views.PlayView.as_view(), name="play"),
]
