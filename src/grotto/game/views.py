from django.conf import settings
from django.views.generic import TemplateView

from account.views import LoginRequiredMixin


class PlayView(LoginRequiredMixin, TemplateView):
    template_name = "play.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context.update(
            {
                "room_context": {
                    "user": {
                        "pk": user.pk,
                        "is_anonymous": user.is_anonymous,
                        "is_staff": user.is_staff,
                        "view_mode": str(user.preferences.view),
                    },
                    "base_url": settings.BASE_URL,
                }
            }
        )
        return context
