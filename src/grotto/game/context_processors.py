import grotto


def character(request):
    return {"character": request.character}


def version(request):
    return {"version": grotto.__version__}
