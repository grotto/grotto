from celery import Celery
from django.conf import settings

app = Celery("grotto")
app.config_from_object(settings, namespace="CELERY")
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    """Entrypoint for debugging"""
    print("Request: {0!r}".format(self.request))
