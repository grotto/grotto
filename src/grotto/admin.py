from django.contrib import admin

from grotto import celery_app, models


@admin.action(description="Make selected config active")
def make_active(modeladmin, request, queryset):
    # ensure only one item in the queryset
    assert queryset.count() == 1, "too many!"
    record = queryset[0]
    # run management commands for each uploaded file

    celery_app.send_task(
        "import_config_csvs",
        kwargs={"instance_config_pk": record.pk},
    )


@admin.action(description="Seed items, lock doors, place traps")
def lock_trap_seed(modeladmin, request, queryset):
    celery_app.send_task(
        "lock_trap_seed",
    )


@admin.register(models.InstanceConfig)
class InstanceConfigAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", "created", "imported")
    fields = (
        "name",
        "abstract_items_csv",
        "collectables_csv",
        "npcs_csv",
        "swaps_csv",
        "activation_criteria_csv",
    )
    actions = [make_active, lock_trap_seed]


@admin.action(description="Make selected gedcom file active")
def make_gedcom_active(modeladmin, request, queryset):
    # ensure only one item in the queryset
    assert queryset.count() == 1, "too many!"
    record = queryset[0]

    celery_app.send_task(
        "import_gedcom",
        kwargs={"instance_gedcom_pk": record.pk},
    )


@admin.register(models.InstanceGedcom)
class InstanceGedcomAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", "created", "imported")
    fields = ("name", "gedcom_file")
    actions = [make_gedcom_active, lock_trap_seed]


@admin.register(models.BannedWord)
class BannedWordAdmin(admin.ModelAdmin):
    list_display = ("pk", "word")
