from django.http import HttpResponse


def health_check_middleware(get_response):
    def middleware(request):
        if request.META["PATH_INFO"] == "/ping/":
            return HttpResponse("pong")
        return get_response(request)
    return middleware
