import os

from django.db import models
from django.utils.timezone import now

from grotto.validators import (
    validate_csv_upload,
    validate_ged_upload,
    validate_inline_upload,
)


def inline_file_uploads(instance, filename):
    # ext = os.path.splitext(filename)[1]
    name = f"inline/{filename}"
    return name


class InlineFileModel(models.Model):
    templates = {
        "img": "<img src='{instance.file.url}' alt_text='{instance.alt_text}' />",
        "audio": (
            "<audio controls autoplay> <source src='{instance.file.url}' type='audio/mpeg'> "
            "Your browser does not support the audio tag. </audio>"
        ),
        "model": (
            "<model-viewer src='{instance.file.url}' alt='{instance.alt_text}' "
            "auto-rotate camera-controls> <div class='progress-bar hide' "
            "slot='progress-bar'> <div class='update-bar'></div> </div> </model-viewer>"
        ),
        "pdf": "<a href='{instance.file.url}'>{instance.alt_text}</a>",
        "video": (
            "<div class='player'> <video controls autoplay loop> <source src='{instance.file.url}' "
            "type='video/mp4'></source> </video> </div>"
        ),
    }

    extensions = {
        ".jpeg": "img",
        ".jpg": "img",
        ".png": "img",
        ".gif": "img",
        ".mp3": "audio",
        ".glb": "model",
        ".pdf": "pdf",
        ".mp4": "video",
    }
    file = models.FileField(
        upload_to=inline_file_uploads,
        validators=[validate_inline_upload],
    )
    original_filename = models.CharField(max_length=128, blank=True)
    extension = models.CharField(max_length=8, blank=True)
    slug = models.SlugField(max_length=120)
    alt_text = models.TextField()

    def __str__(self):
        return f"{self.original_filename}{self.extension}"

    def placeholder(self):
        return rf"\[!{self.slug}\]"

    def markup(self):
        """Generates the HTML for displaying a CenotaphFile"""
        # identify element
        element_type = self.extensions.get(self.extension)
        if element_type is None:
            return "[file extension not supported]"
        # retrieve template
        template = self.templates.get(element_type)
        if template is None:
            return "[file extension not supported]"
        # generate markup
        return template.format(instance=self)

    def save(self, **kwargs):
        """If this is the first time saving, then capture the original
        filename"""
        if not self.id:
            self.original_filename, self.extension = os.path.splitext(str(self.file))
        return super().save(**kwargs)

    class Meta:
        abstract = True


def config_file_uploads(instance, filename):
    stub, ext = os.path.splitext(filename)
    name = f"config/{stub}-{now()}{ext}"
    return name


class InstanceGedcom(models.Model):
    gedcom_file = models.FileField(
        upload_to=config_file_uploads,
        validators=[validate_ged_upload],
    )
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=128, null=True, blank=True)
    imported = models.DateTimeField(null=True, blank=True)


class InstanceConfig(models.Model):
    abstract_items_csv = models.FileField(
        upload_to=config_file_uploads,
        validators=[validate_csv_upload],
    )
    collectables_csv = models.FileField(
        upload_to=config_file_uploads,
        validators=[validate_csv_upload],
    )
    npcs_csv = models.FileField(
        "Abstract NPCs",
        upload_to=config_file_uploads,
        validators=[validate_csv_upload],
    )
    swaps_csv = models.FileField(
        upload_to=config_file_uploads,
        validators=[validate_csv_upload],
    )
    activation_criteria_csv = models.FileField(
        upload_to=config_file_uploads,
        validators=[validate_csv_upload],
        null=True,
    )
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=128, null=True, blank=True)
    imported = models.DateTimeField(null=True, blank=True)


class StartingRoom(models.Model):
    room_id = models.BigIntegerField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        get_latest_by = ("created",)


class BannedWord(models.Model):
    word = models.CharField(max_length=25)
