from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

urlpatterns = [
    path("api/v1/", include("grotto.api.urls")),
    path("accounts/", include("account.urls")),
    path("admin/", admin.site.urls),
    path("game/", include("grotto.game.urls")),
    path("guild/", include("guild.urls")),
    path("rooms/", include("maze.urls")),
    path(
        "",
        TemplateView.as_view(template_name="static_pages/index.html"),
        name="index",
    ),
    path("tmp", TemplateView.as_view(template_name="static_pages/tmp.html")),
    path(
        "privacy/",
        TemplateView.as_view(template_name="static_pages/privacy.html"),
        name="privacy",
    ),
    path(
        "agreement/",
        TemplateView.as_view(template_name="static_pages/agreement.html"),
        name="agreement",
    ),
    path(
        "reporting/",
        TemplateView.as_view(template_name="static_pages/reporting.html"),
        name="reporting",
    ),
]
