from csv import DictReader
from datetime import timedelta
from random import choice

from django.core.management.base import BaseCommand

from maze.models import Room


class CsvImportCommand(BaseCommand):
    field_model_map = {}
    file_fields = []
    m2m_fields = {}
    enum_fields = {}
    duration_fields = ()
    boolean_fields = ()
    model_class = None

    class PreSaveException(Exception):
        pass

    def add_arguments(self, parser):
        parser.add_argument("--input_csv")

    def _pick_room_on_level(self, level):
        qs = Room.objects.all()
        if level is not None:
            qs = qs.filter(level=level)
        return choice(list(qs.values_list("id", flat=True)))

    def _coerce_nones(self, row):
        for field, value in row.items():
            if value.lower() in ("none",):
                row[field] = None

    def _coerce_enums(self, row):
        for field, enum_class in self.enum_fields.items():
            row[field] = enum_class.get(row[field])

    def _coerce_booleans(self, row):
        for field in self.boolean_fields:
            row[field] = row[field].lower() == "true"

    def _coerce_durations(self, row):
        for field in self.duration_fields:
            if row[field] is not None:
                row[field] = timedelta(seconds=int(row[field]))

    def _coerce_filepaths(self, row):
        for field in self.file_fields:
            if row.get(field):
                if not row[field].startswith("inline/"):
                    row[field] = f"inline/{row[field]}"

    def _prehandle_m2ms(self, row):
        ctx = {}
        for field in self.m2m_fields:
            _value = row.pop(field)
            if _value:
                _values = {v.strip() for v in _value.split(";")}
                ctx.setdefault(field, []).extend([v for v in _values if v])
        return ctx

    def _handle_m2ms(self, instance, *, m2m_context):
        for field, model in self.m2m_fields.items():
            manager = getattr(instance, field)
            for related in m2m_context.get(field, []):
                manager.add(model.objects.get(ref=related))

    def _preprocess_row(self, row):
        return {}

    def _pre_save_row(self, row, *, preprocess_output):
        return {}

    def _post_save_row(self, *args, **kwargs):
        return {}

    def _handle_row(self, row):
        self._coerce_nones(row)
        self._coerce_enums(row)
        self._coerce_durations(row)
        self._coerce_booleans(row)
        self._coerce_filepaths(row)
        _m2m_context = self._prehandle_m2ms(row)
        _pre = self._preprocess_row(row)
        for field in tuple(row.keys()):
            # remove redacted fields
            if field.startswith("_"):
                row.pop(field)
        ref = row.pop("ref")
        for field, model in self.field_model_map.items():
            _value = row.pop(field)
            if _value:
                row[field] = model.objects.get(ref=_value)
        try:
            self._pre_save_row(row, preprocess_output=_pre)
        except self.PreSaveException as e:
            print(str(e))
            return
        instance, _ = self.model_class.objects.update_or_create(
            ref=ref,
            defaults=row,
        )
        self._handle_m2ms(instance, m2m_context=_m2m_context)
        self._post_save_row(row, preprocess_output=_pre, instance=instance)

    def handle_file_handle(self, *, fh):
        rows = DictReader(fh)
        for row in rows:
            self._handle_row(row)

    def initialize(self):
        pass

    def handle(self, *, input_csv, **kwargs):
        self.initialize()
        if isinstance(input_csv, (str,)):
            with open(input_csv) as _fh:
                self.handle_file_handle(fh=_fh)
        else:
            self.handle_file_handle(fh=input_csv)
