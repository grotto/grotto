from pathlib import Path

from .celery import app as celery_app  # noqa: F401

version_file = Path(__file__).resolve().parent.parent / "VERSION"
with open(version_file) as f:
    __version__ = f.read().strip()
