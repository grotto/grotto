class ActionMixin:
    """Puts action details on bottom of template"""

    actions = [
        {
            "url": "#",
            "text": "demo action 1",
        },
        {
            "url": "#",
            "text": "demo action 2",
        },
        {
            "url": "#",
            "text": "demo action 3",
        },
    ]

    def formatted_actions(self):
        """Hook for doing whatever mutation on actions that might be necessary"""
        return self.actions

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"actions": self.formatted_actions()})
        return context
