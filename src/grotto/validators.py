import os

import magic
from django.core.exceptions import ValidationError


def _validate_upload(file, valid_mime_types):
    file_mime_type = magic.from_buffer(file.read(1024), mime=True)
    print(file_mime_type)
    if file_mime_type not in valid_mime_types.keys():
        # _valid_types = f''
        raise ValidationError(
            f"Unsupported file type. Must be one of the following: "
            f'{", ".join([k for k in valid_mime_types.keys()])}'
        )
    valid_file_extensions = valid_mime_types[file_mime_type]
    ext = os.path.splitext(file.name)[-1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError(
            f"File must have "
            f'{" or ".join([f"{e!r}" for e in valid_file_extensions]) } '
            "as its extension"
        )


def validate_inline_upload(file):
    """Make sure the uploaded file's extension matches the content, and is
    within allowable extensions.

    Parameters
    -----------
    file : file
        The file to validate

    Raises
    -------
    ValdiationError
        When file type is unsupported, or has the wrong extension

    """
    # print('validate_is_pdf')
    valid_mime_types = {
        "image/jpeg": [".jpg", ".jpeg"],
        "image/png": [".png"],
        "image/gif": [".gif"],
        "audio/mpeg3": [".mp3"],
        "audio/mpeg": [".mp3"],
        "audio/x-mpeg-3": [".mp3"],
        "model/gltf-binary": [".glb"],
        "application/pdf": [".pdf"],
        "video/mp4": [".mp4"],
    }
    _validate_upload(file, valid_mime_types)


def validate_glb_upload(file):
    """Make sure the uploaded file's extension matches the content, and is
    within allowable extensions.

    Parameters
    -----------
    file : file
        The file to validate

    Raises
    -------
    ValdiationError
        When file type is unsupported, or has the wrong extension

    """
    # print('validate_is_pdf')
    valid_mime_types = {
        "model/gltf-binary": [".glb"],
    }
    _validate_upload(file, valid_mime_types)


def validate_csv_upload(file):
    """Make sure the uploaded file's extension matches the content, and is
    within allowable extensions.

    Parameters
    -----------
    file : file
        The file to validate

    Raises
    -------
    ValdiationError
        When file type is unsupported, or has the wrong extension

    """
    valid_mime_types = {
        "text/csv": [".csv"],
        "text/plain": [".csv"],
    }
    _validate_upload(file, valid_mime_types)


def validate_pdf_upload(file):
    """Make sure the uploaded file's extension matches the content, and is
    within allowable extensions.

    Parameters
    -----------
    file : file
        The file to validate

    Raises
    -------
    ValdiationError
        When file type is unsupported, or has the wrong extension

    """
    valid_mime_types = {
        "application/pdf": [".pdf"],
    }
    _validate_upload(file, valid_mime_types)


def validate_mp3_upload(file):
    """Make sure the uploaded file's extension matches the content, and is
    within allowable extensions.

    Parameters
    -----------
    file : file
        The file to validate

    Raises
    -------
    ValdiationError
        When file type is unsupported, or has the wrong extension

    """
    valid_mime_types = {
        "audio/mpeg3": [".mp3"],
        "audio/x-mpeg-3": [".mp3"],
    }
    _validate_upload(file, valid_mime_types)


def validate_rom_upload(file):
    valid_file_extensions = [".a26"]
    ext = os.path.splitext(file.name)[-1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError(
            f"File must have "
            f'{" or ".join([f"{e!r}" for e in valid_file_extensions]) } '
            "as its extension"
        )


def validate_ged_upload(file):
    valid_file_extensions = [".ged"]
    ext = os.path.splitext(file.name)[-1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError(
            f"File must have "
            f'{" or ".join([f"{e!r}" for e in valid_file_extensions]) } '
            "as its extension"
        )
