from django.conf import settings
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from grotto.api import views

schema_view = get_schema_view(
    openapi.Info(
        title="Grotto API",
        default_version="v1",
        description="Idk",
    ),
    url=settings.DEFAULT_API_URL,
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register("game/rooms", views.RoomViewSet, basename="room")
router.register("game/items", views.ItemViewSet, basename="item")
router.register("game/characters", views.CharacterViewSet, basename="character")
router.register("game/npcs", views.NonPlayerCharacterViewSet, basename="npc")

urlpatterns = router.urls

urlpatterns += [
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    re_path(
        r"swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "docs/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("foundry/", include("foundry.api.urls", namespace="foundry-api")),
    path("guild/", include("guild.api.urls", namespace="guild-api")),
    path("maze/", include("maze.api.urls", namespace="maze-api")),
    path("account/", include("account.api.urls", namespace="account-api")),
    path("arena/", include("arena.api.urls", namespace="arena-api")),
    path("game/tableau/", views.TableauAPIView.as_view(), name="tableau"),
    path("game/enter/", views.EnterAPIView.as_view()),
]
