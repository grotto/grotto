from django.contrib import messages
from django.db.models import Prefetch, Count, Q
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from grotto import celery_app
from foundry.api.serializers import ItemSerializer
from foundry.models import Item
from foundry.enum import ItemType
from foundry.services import ItemService
from grotto.api import serializers
from grotto.game.services import GameService
from grotto.services import GrottoGameWarning
from guild.api.serializers import CharacterSerializer, NonPlayerCharacterSerializer
from guild.models import Character, NonPlayerCharacter
from guild.services import PlayerCharacterService
from maze.api.serializers import RoomSerializer
from maze.models import Room, RoomExit


class TableauSerializerMixin:
    def get_tableau_serializer(self, *, character="from-request", room_pk="from-character"):
        if character == "from-request":
            character = self.request.character
        if character is not None:
            character.refresh_from_db()
        if room_pk == "from-character":
            room_pk = None
            if character is not None and character.room is not None:
                room_pk = character.room.pk
        stored_messages = []
        room = None
        if character is not None:
            try:
                # Use the manager to get the object so that it has full annotations
                room = (
                    Room.objects.filter(pk=room_pk)
                    .select_related("cenotaph")
                    .prefetch_related(
                        Prefetch(
                            "items",
                            queryset=Item.objects.exclude(
                                abstract_item__item_type=ItemType.TRAP,
                                active__isnull=False,
                            ),
                        )
                    )
                    .prefetch_related("occupants")
                    .prefetch_related("npcs")
                    .prefetch_related(
                        Prefetch(
                            "exits_from",
                            queryset=RoomExit.objects.all().annotate(
                                visit_count=Count(
                                    "to_room___visits",
                                    filter=Q(to_room___visits__character=character),
                                ),
                            )
                        )
                    )[0]
                )
            except (IndexError, AttributeError):
                pass
            messages_qs = character.messages.filter(seen=False)
            stored_messages = [
                {"message": m} for m in messages_qs.values_list("text", flat=True)
            ]
            messages_qs.update(seen=True)
        elif room_pk is not None:
            try:
                # Use the manager to get the object so that it has full annotations
                room = (
                    Room.objects.filter(pk=room_pk)
                    .select_related("cenotaph")
                    .prefetch_related(
                        Prefetch(
                            "items",
                            queryset=Item.objects.exclude(
                                abstract_item__item_type=ItemType.TRAP,
                            ),
                        )
                    )
                    .prefetch_related("occupants")
                    .prefetch_related("npcs")
                    .prefetch_related(
                        Prefetch(
                            "exits_from",
                        )
                    )[0]
                )
            except (IndexError, AttributeError):
                pass
        warnings = []
        if room is not None:
            warnings = room.warnings_for(character)
        return serializers.TableauSerializer(
            {
                "character": character,
                "room": room,
                "warnings": warnings,
                "messages": stored_messages
                + [{"message": str(m)} for m in messages.get_messages(self.request)],
            }
        )


class TableauAPIView(generics.GenericAPIView, TableauSerializerMixin):
    serializer_class = serializers.TableauSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.get_tableau_serializer()
        # serializer.is_valid()
        return Response(serializer.data)


class GameActionMixin(TableauSerializerMixin):
    """
    Always use this as the first parent class for any game action view or viewset.
    It handles:
      * breaking the sour news to a player that their character is dead,
      * standardizing the response json to a game action
      * resolving actions (e.g. rolling for npc movement)
    """

    model = None
    service_class = None

    def dispatch(self, request, *args, **kwargs):
        self.dice_count = 0
        if request.character is None:
            raise GrottoGameWarning("Character does not exist")
        self.headers = self.default_response_headers
        # is this character alive
        if request.character.dead:
            # message about death
            messages.add_message(request, messages.INFO, request.character.deathnote)
            # Shortcut whatever action was requested because the player is dead!
            return self.finalize_response(request, None, *args, **kwargs)
        try:
            response = super().dispatch(request, *args, **kwargs)
        except GrottoGameWarning as e:
            self.dice_count = 0
            messages.add_message(request, messages.INFO, f"{e}")
            response = self.finalize_response(request, None, *args, **kwargs)
        return response

    def finalize_response(self, request, response, *args, **kwargs):
        self.advance_game()
        response = Response(self.get_tableau_serializer().data)
        return super().finalize_response(request, response, *args, **kwargs)

    def advance_game(self):
        if self.dice_count == 0:
            return
        game = GameService()
        # roll for dirtying room
        game.roll_to_dirty_room(
            room=self.request.character.room, dice_count=self.dice_count
        )
        game.roll_to_character_poop(
            character=self.request.character, dice_count=self.dice_count
        )
        # roll for NPC movements
        if self.request.character.room is not None:
            celery_app.send_task(
                "move_npcs",
                kwargs={
                    "dice_count": self.dice_count,
                    "level": self.request.character.room.level,
                    "character_pk": self.request.character.pk,
                },
            )

    def generic_action(self, request, pk, action="test", holder="character"):
        other = get_object_or_404(self.model, id=pk)
        service = self.service_class()
        service_return = getattr(service, action)(
            character=request.character, other=other
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        return Response("this should get overwritten!")


class EnterAPIView(GameActionMixin, generics.GenericAPIView):
    serializer_class = serializers.NullSerializer

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    def post(self, request, *args, **kwargs):
        PlayerCharacterService().enter(character=request.character)


class RoomViewSet(GameActionMixin, viewsets.GenericViewSet):
    serializer_class = RoomSerializer
    queryset = Room.objects.all()

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def move(self, request, pk):
        service_return = PlayerCharacterService().move(
            character=request.character, id=pk
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 1

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def fire_arrow(self, request, pk):
        service_return = PlayerCharacterService().fire_arrow(
            character=request.character, id=pk
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 5

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def place_trap(self, request, pk):
        service_return = PlayerCharacterService().place_trap(
            character=request.character, id=pk
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 5

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def bash(self, request, pk):
        room = get_object_or_404(Room, id=pk)
        service_return = PlayerCharacterService().bash(
            character=request.character, other=room
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 5

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def toggle_lock(self, request, pk):
        service_return = PlayerCharacterService().toggle_lock(
            character=request.character, id=pk
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 5

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=False,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def kneel(self, request):
        pass
        service_return = PlayerCharacterService().kneel(character=request.character)
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        self.dice_count += 1


class ItemViewSet(GameActionMixin, viewsets.GenericViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

    def generic_action(self, request, pk, action="test", holder="character"):
        item_service = ItemService()
        item = item_service.get_item(character=request.character, pk=pk, holder=holder)
        service_return = getattr(item_service, action)(
            item=item, character=request.character
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        return Response("this should get overwritten!")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def use(self, request, pk):
        return self.generic_action(request, pk, action="use")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def place(self, request, pk):
        return self.generic_action(request, pk, action="place")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def take(self, request, pk):
        return self.generic_action(request, pk, action="take", holder="room")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def drop(self, request, pk):
        return self.generic_action(request, pk, action="drop")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def equip(self, request, pk):
        return self.generic_action(request, pk, action="equip")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.MessageSerializer,
    )
    def compose(self, request, pk):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        item_service = ItemService()
        item = item_service.get_item(
            character=request.character, pk=pk, holder="character"
        )
        service_return = item_service.compose(
            item=item,
            character=request.character,
            message=serializer.validated_data["message"],
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        return Response("this should get overwritten!")


class CharacterViewSet(GameActionMixin, viewsets.GenericViewSet):
    serializer_class = CharacterSerializer
    queryset = Character.objects.all()
    model = Character
    service_class = PlayerCharacterService

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def bash(self, request, pk):
        return self.generic_action(request, pk, action="bash")

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["patch"],
        serializer_class=serializers.MessageSerializer,
        url_path="set-greeting",
    )
    def set_greeting(self, request, pk):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        service = self.service_class()
        service_return = getattr(service, "set_greeting")(
            character=request.character,
            message=serializer.validated_data["message"],
        )
        for message in service_return.messages:
            messages.add_message(request, messages.INFO, message)
        return Response("this should get overwritten!")


class NonPlayerCharacterViewSet(GameActionMixin, viewsets.GenericViewSet):
    serializer_class = NonPlayerCharacterSerializer
    queryset = NonPlayerCharacter.objects.all()
    model = NonPlayerCharacter
    # this is not a bug... the player is the actor
    service_class = PlayerCharacterService

    @swagger_auto_schema(responses={200: serializers.TableauSerializer})
    @action(
        detail=True,
        methods=["post"],
        serializer_class=serializers.NullSerializer,
    )
    def bash(self, request, pk):
        print("here!!!")
        return self.generic_action(request, pk, action="bash")
