from rest_framework import serializers

from guild.api.serializers import CharacterSerializer
from maze.api.serializers import RoomSerializer, WarningSerializer


class NullSerializer(serializers.Serializer):
    pass


class MessageSerializer(serializers.Serializer):
    message = serializers.CharField()


class TableauSerializer(serializers.Serializer):
    character = CharacterSerializer()
    room = RoomSerializer()
    warnings = WarningSerializer(many=True, read_only=True)
    messages = MessageSerializer(many=True, read_only=True)
