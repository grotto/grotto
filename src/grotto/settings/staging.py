from .base import *  # noqa: F403, F401

ALLOWED_HOSTS = ["grotto.wileywiggins.com"]

ENV = "STAGING"

BASE_URL = "grotto.wileywiggins.com"
DEFAULT_API_URL = "https://grotto.wileywiggins.com/api/v1/"
