import os

from .staging import *  # noqa: F403, F401

ALLOWED_HOSTS = ["www.mudroom.rip", "www.samfac.net"]

ENV = "PROD"

DATABASES = {  # noqa: F405
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("DATABASE_NAME", "postgres"),
        "USER": os.environ.get("DATABASE_USER", "postgres"),
        "PASSWORD": os.environ.get("DATABASE_PASSWORD", "postgres"),
        "HOST": os.environ.get("DATABASE_HOST", "db"),
        "PORT": os.environ.get("DATABASE_PORT", "5432"),
    }
}

BASE_URL = "www.mudroom.rip"
DEFAULT_API_URL = "https://www.mudroom.rip/api/v1/"
