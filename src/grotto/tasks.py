from django.core.management import call_command
from django.utils.timezone import now

from grotto import celery_app
from grotto.models import InstanceConfig, InstanceGedcom
from foundry.services import ItemService
from maze.models import Cenotaph
from grotto.game.services import GameService
from guild.models import CharacterMessage

@celery_app.task(name="import_config_csvs")
def import_config_csvs(*, instance_config_pk):
    instance_config = InstanceConfig.objects.get(pk=instance_config_pk)
    call_command("import_npcs", input_csv=instance_config.npcs_csv.open("r"))
    call_command(
        "import_abstract_items", input_csv=instance_config.abstract_items_csv.open("r")
    )
    call_command(
        "import_and_place_items", input_csv=instance_config.collectables_csv.open("r")
    )
    call_command("import_swaps", input_csv=instance_config.swaps_csv.open("r"))
    call_command(
        "import_activation_criteria",
        input_csv=instance_config.activation_criteria_csv.open("r"),
    )
    call_command("place_npcs")

    instance_config.imported = now()
    instance_config.save()


@celery_app.task(name="lock_trap_seed")
def lock_trap_seed():
    call_command("seed_items")
    call_command("lock_and_trap")


@celery_app.task(name="import_gedcom")
def import_gedcom(*, instance_gedcom_pk):
    instance_gedcom = InstanceGedcom.objects.get(pk=instance_gedcom_pk)
    call_command("cleanup_rooms")
    call_command("create_cenotaphs", gedcom_file=instance_gedcom.gedcom_file.open("rb"))
    starting_room = Cenotaph.objects.get(name="Wiley Ramsey Wiggins").room
    call_command("sort_rooms_to_levels", origin_room=starting_room.pk)

    instance_gedcom.imported = now()
    instance_gedcom.save()


@celery_app.task(name="burnable_swap")
def burnable_swap():
    ItemService().burnable_swap()


@celery_app.task(name="move_npcs")
def move_npcs(*, dice_count, level, character_pk):
    service_return = GameService().roll_to_resolve_npc_action(
        dice_count=dice_count, level=level
    )
    for message in service_return.messages:
        CharacterMessage.objects.create(
            character_id=character_pk, text=message
        )
