from rest_framework import mixins, viewsets

from foundry.api.serializers import ItemDetailSerializer
from foundry.models import Item


class ItemDetailViewSet(
    viewsets.GenericViewSet,
    mixins.RetrieveModelMixin,
):
    serializer_class = ItemDetailSerializer
    queryset = Item.objects.all()
