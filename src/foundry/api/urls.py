from rest_framework.routers import DefaultRouter

from foundry.api import views

app_name = "foundry-api"

router = DefaultRouter()
router.register("items", views.ItemDetailViewSet, basename="item")

urlpatterns = router.urls

urlpatterns += []
