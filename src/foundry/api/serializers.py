from django_enumfield.contrib.drf import NamedEnumField
from rest_framework import serializers

from foundry import models
from foundry.enum import ItemType


class PlacedTrapSerializer(serializers.ModelSerializer):
    item_pk = serializers.PrimaryKeyRelatedField(source="item", read_only=True)
    item_name = serializers.StringRelatedField(source="item", read_only=True)

    class Meta:
        model = models.PlacedTrap
        fields = ("pk", "item_pk", "item_name", "created", "removed")


class AbstractItemSerializer(serializers.ModelSerializer):
    item_type = NamedEnumField(ItemType)

    class Meta:
        model = models.AbstractItem
        fields = (
            "pk",
            "name",
            "description",
            "item_type",
            "active_adjective",
            "icon_data",
            "active_icon_data",
        )


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Item
        fields = (
            "pk",
            "name",
            # "abstract_item",
            "color_name",
            "color_hex",
            "is_active",
            "is_usable",
            "is_takeable",
            "cursed",
        )


class ItemStackSerializer(serializers.Serializer):
    abstract_item = AbstractItemSerializer(read_only=True)
    is_active = serializers.BooleanField(read_only=True)
    items = serializers.ListField(
        child=ItemSerializer(read_only=True),
        allow_empty=False,
        read_only=True,
    )


class FilenameField(serializers.CharField):
    def to_representation(self, value):
        _value = super().to_representation(value)
        return _value.rsplit("/", maxsplit=1)[-1]


class FileDetailSerializer(serializers.Serializer):
    url = serializers.CharField()
    name = FilenameField()

    def to_representation(self, value):
        print(value)
        if not value:
            return None
        return super().to_representation(value)


class HoldingSerializer(serializers.ModelSerializer):
    character = serializers.StringRelatedField()
    kind = serializers.CharField(source="character.kind")

    class Meta:
        model = models.Holding
        fields = (
            "character",
            "kind",
            "stamp_date",
        )


class ItemDetailSerializer(serializers.ModelSerializer):
    description = serializers.CharField(source="rendered_description")
    abstract_item = AbstractItemSerializer(read_only=True)
    model = FileDetailSerializer(required=False)
    pdf = FileDetailSerializer(required=False)
    mp3 = FileDetailSerializer(required=False)
    holdings = HoldingSerializer(many=True)

    class Meta:
        model = models.Item
        fields = (
            "pk",
            "name",
            "description",
            "abstract_item",
            "color_name",
            "color_hex",
            "is_active",
            "is_usable",
            "is_takeable",
            "model",
            "pdf",
            "mp3",
            "holdings",
        )
