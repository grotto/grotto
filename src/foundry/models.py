import re

from colorfield.fields import ColorField
from django.db import models
from django.utils.timezone import now
from django_enumfield import enum

from foundry.enum import ItemType
from grotto import validators
from grotto.models import InlineFileModel, inline_file_uploads
from guild.models import AbstractNonPlayerCharacter, Character, NonPlayerCharacter
from maze.models import Cenotaph, Room, RoomExit


class AbstractItem(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    item_type = enum.EnumField(ItemType)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256)

    active_time = models.DurationField(null=True, blank=True)
    viewable = models.BooleanField(default=False)
    usable = models.BooleanField(default=False)  # this is an analog for "activateable"
    untakable = models.BooleanField(default=False)
    untakable_if_active = models.BooleanField(default=False)
    stackable = models.BooleanField(default=False)
    item_name_template = models.CharField(
        max_length=250,
        default="{abstract_item_name}",
        help_text=(
            "You can use patterns like '{adjective}', '{animal}', "
            "'{substance}', and '{elaborate_color}' to pull random words from "
            "the words lists in the 'src/words_lists' directory; and "
            "'{abstract_item_name}'"
        ),
    )
    active_adjective = models.CharField(
        max_length=30, null=True, blank=True, help_text="e.g. 'lit' for a candle."
    )
    holders = models.ManyToManyField(
        AbstractNonPlayerCharacter, related_name="abstract_loot", blank=True
    )
    untakable_message = models.CharField(max_length=256, null=True, blank=True)
    icon_data = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        default="121",
    )
    active_icon_data = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        default="121",
    )
    warning_subscriptions = models.ManyToManyField(
        AbstractNonPlayerCharacter,
        related_name="warning_subscriptions",
        blank=True,
    )

    def __str__(self):
        return self.name


class ItemManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        burn_out_time = models.ExpressionWrapper(
            models.F("active") + models.F("abstract_item__active_time"),
            output_field=models.DateTimeField(),
        )
        return qs.annotate(
            burn_out_time=burn_out_time,
        )


class ItemQuerySet(models.QuerySet):
    def _do_filter_or_exclude(self, *, exclude, kwargs):
        if exclude:
            return self.exclude(**kwargs)
        return self.filter(**kwargs)

    def expired(self, exclude=False):
        return self._do_filter_or_exclude(
            exclude=exclude,
            kwargs={
                "burn_out_time__lte": now(),
            },
        )


class Item(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    name = models.CharField(max_length=300, default="blank")
    description = models.TextField(null=True, blank=True)
    abstract_item = models.ForeignKey(AbstractItem, on_delete=models.CASCADE)
    character = models.ForeignKey(
        Character,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="inventory",
    )
    room = models.ForeignKey(
        Room, on_delete=models.CASCADE, null=True, blank=True, related_name="items"
    )
    cenotaph = models.ForeignKey(
        Cenotaph, on_delete=models.CASCADE, null=True, blank=True, related_name="items"
    )
    npc = models.ForeignKey(
        NonPlayerCharacter,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="loot",
    )
    active = models.DateTimeField(null=True, blank=True)
    equipped = models.BooleanField(default=False)
    cursed = models.BooleanField(default=False)
    color_name = models.CharField(max_length=200, default="black")
    color_hex = ColorField(default="#222222")

    related_cenotaph = models.ForeignKey(
        Cenotaph,
        null=True,
        blank=True,
        related_name="relics",
        on_delete=models.PROTECT,
    )
    model = models.FileField(
        upload_to=inline_file_uploads,
        validators=[validators.validate_glb_upload],
        null=True,
        blank=True,
    )
    pdf = models.FileField(
        upload_to=inline_file_uploads,
        validators=[validators.validate_pdf_upload],
        null=True,
        blank=True,
    )
    mp3 = models.FileField(
        upload_to=inline_file_uploads,
        validators=[validators.validate_mp3_upload],
        null=True,
        blank=True,
    )
    rom = models.FileField(
        upload_to=inline_file_uploads,
        validators=[validators.validate_rom_upload],
        null=True,
        blank=True,
    )
    objects = ItemManager.from_queryset(ItemQuerySet)()

    def __str__(self):
        return self.name

    @property
    def is_takeable(self):
        if self.abstract_item.untakable:
            return False
        if self.active and self.abstract_item.untakable_if_active:
            return False
        return True

    @property
    def is_viewable(self):
        return self.abstract_item.viewable

    @property
    def is_active(self):
        # if held by NPC and activatable return true
        if self.active is None:
            return False
        if self.npc is not None:
            return True
        if self.abstract_item.active_time is None:
            return True
        if self.active > now() - self.abstract_item.active_time:
            return True
        return False

    @property
    def is_usable(self):
        return self.abstract_item.usable

    @property
    def rendered_description(self):
        # This is a strong candidate for caching, but with a relatively short TTL (2 hour)
        _description = self.description
        # index the cenotaph files
        for file in self.files.all():
            # scan the `description` for image markup
            # replace markup with image tag
            _description = re.sub(file.placeholder(), file.markup(), _description)
        return _description


class PlacedTrap(models.Model):
    exit = models.ForeignKey(
        RoomExit,
        on_delete=models.CASCADE,
        related_name="traps",
    )
    item = models.OneToOneField(
        Item,
        related_name="placement",
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True)
    removed = models.DateTimeField(blank=True, null=True)


class Swap(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    # picks_up_item
    picks_type = enum.EnumField(ItemType)
    picks_abstract_item = models.ForeignKey(
        AbstractItem,
        on_delete=models.PROTECT,
        related_name="swappables",
        null=True,
        blank=True,
    )
    # drops_item
    puts = models.ForeignKey(AbstractItem, on_delete=models.PROTECT)
    # npc
    abstract_npc = models.ForeignKey(
        AbstractNonPlayerCharacter,
        on_delete=models.PROTECT,
        null=True,
        related_name="swaps",
    )
    message = models.CharField(max_length=200)


class ItemFile(InlineFileModel):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="files")


class Holding(models.Model):
    item = models.ForeignKey(
        Item, on_delete=models.CASCADE, related_name="holdings"
    )
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="holdings"
    )
    stamp_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        get_latest_by = ("stamp_date",)

