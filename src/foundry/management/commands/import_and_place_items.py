from os.path import splitext

from foundry.models import AbstractItem, Item, ItemFile
from grotto.management.base.csv import CsvImportCommand
from maze.models import Cenotaph


class Command(CsvImportCommand):
    help = (
        "Creates Item instances for collecables from a CSV, and places "
        "the items in appropriate rooms"
    )

    field_model_map = {
        "abstract_item": AbstractItem,
    }
    model_class = Item
    file_fields = ["rom", "model", "pdf", "mp3"]

    def _preprocess_row(self, row):
        ret = {}
        ret["level"] = row.pop("starting_level", None) or None  # clean the blanks
        ret["attachments"] = row.pop("attachments")
        ret["related_cenotaph_name"] = row.pop("related_cenotaph_name", None)
        ret["room_cenotaph_name"] = row.pop("room_cenotaph_name", None)
        return ret

    def _pre_save_row(self, row, *, preprocess_output):
        if preprocess_output["related_cenotaph_name"]:
            cenotaphs = Cenotaph.objects.filter(
                name=preprocess_output["related_cenotaph_name"]
            )
            if cenotaphs:
                row.update({"related_cenotaph": cenotaphs[0], "cenotaph": cenotaphs[0]})
        if preprocess_output["room_cenotaph_name"]:
            cenotaphs = Cenotaph.objects.filter(
                name=preprocess_output["room_cenotaph_name"]
            )
            if cenotaphs:
                row.pop("cenotaph", None)
                row.update({"room": cenotaphs[0].room})
        if (
            not preprocess_output["related_cenotaph_name"]
            and not preprocess_output["room_cenotaph_name"]
        ):
            row.update(
                {"room_id": self._pick_room_on_level(preprocess_output["level"])}
            )

    def _post_save_row(self, row, *, instance, preprocess_output):
        for attachment in preprocess_output["attachments"].split(";"):
            if not attachment.strip():
                continue
            self._handle_attachment(attachment, instance)

    def _handle_attachment(self, attachment, instance):
        name = attachment
        if "|" in attachment:
            name, slug = attachment.split("|", maxsplit=1)
            alt_text = slug
            if "|" in slug:
                slug, alt_text = slug.split("|", maxsplit=1)
        else:
            slug = splitext(attachment)[0]
            alt_text = slug

        if not name.startswith("inline/"):
            name = f"inline/{name}"
        ItemFile.objects.get_or_create(
            item=instance,
            file=name,
            slug=slug,
            alt_text=alt_text,
        )
