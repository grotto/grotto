from foundry.enum import ItemType
from foundry.models import AbstractItem
from grotto.management.base.csv import CsvImportCommand
from guild.models import AbstractNonPlayerCharacter


class Command(CsvImportCommand):
    duration_fields = ("active_time",)
    boolean_fields = (
        "viewable",
        "usable",
        "untakable",
        "untakable_if_active",
        "stackable",
    )
    enum_fields = {
        "item_type": ItemType,
    }
    m2m_fields = {
        "holders": AbstractNonPlayerCharacter,
        "warning_subscriptions": AbstractNonPlayerCharacter,

    }
    model_class = AbstractItem
