from foundry.enum import ItemType
from foundry.models import AbstractItem, Swap
from grotto.management.base.csv import CsvImportCommand
from guild.models import AbstractNonPlayerCharacter


class Command(CsvImportCommand):
    field_model_map = {
        "picks_abstract_item": AbstractItem,
        "puts": AbstractItem,
        "abstract_npc": AbstractNonPlayerCharacter,
    }
    model_class = Swap
    enum_fields = {
        "picks_type": ItemType,
    }
