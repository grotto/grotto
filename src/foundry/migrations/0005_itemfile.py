# Generated by Django 3.1.4 on 2022-05-29 11:28

import django.db.models.deletion
from django.db import migrations, models

import grotto.models
import grotto.validators


class Migration(migrations.Migration):

    dependencies = [
        ("foundry", "0004_item_cenotaph"),
    ]

    operations = [
        migrations.CreateModel(
            name="ItemFile",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "file",
                    models.FileField(
                        upload_to=grotto.models.inline_file_uploads,
                        validators=[grotto.validators.validate_inline_upload],
                    ),
                ),
                ("original_filename", models.CharField(blank=True, max_length=128)),
                ("extension", models.CharField(blank=True, max_length=8)),
                ("slug", models.SlugField()),
                ("alt_text", models.TextField()),
                (
                    "item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="files",
                        to="foundry.item",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
