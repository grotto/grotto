from django.contrib import admin

from foundry.models import AbstractItem, Item, ItemFile, PlacedTrap, Swap


@admin.register(AbstractItem)
class AbstractItemAdmin(admin.ModelAdmin):
    list_display = ("name", "ref", "item_type")


class ItemFileInline(admin.TabularInline):
    fields = ("file", "slug", "alt_text")
    model = ItemFile


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "ref",
        "abstract_item",
        "character",
        "room",
        "cenotaph",
        "active",
        "related_cenotaph",
    )
    inlines = (ItemFileInline,)
    list_filter = ("abstract_item",)


@admin.register(Swap)
class SwapAdmin(admin.ModelAdmin):
    list_display = ("id", "ref", "picks_type", "puts", "abstract_npc")


@admin.register(PlacedTrap)
class PlacedTrapAdmin(admin.ModelAdmin):
    list_display = (
        "exit",
        "exit_from_pk",
        "exit_to_pk",
        "item",
        "created",
        "removed",
    )

    @admin.display(ordering="exit__from_room__id")
    def exit_from_pk(self, obj):
        return obj.exit.from_room.id

    @admin.display(ordering="exit__to_room__id")
    def exit_to_pk(self, obj):
        return obj.exit.to_room.id
