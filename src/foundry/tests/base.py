from foundry import models
from foundry.enum import ItemType


class ItemBuilderModelMixin:
    def _abstract_item(self, item_type, **kwargs):
        _base = {
            "item_type": item_type,
            "name": "test junk",
            "description": "nothing relevant",
        }
        _base.update(kwargs)
        return models.AbstractItem.objects.get_or_create(**_base)[0]

    def _item(self, item_type=ItemType.JUNK, **kwargs):
        _base = {
            "name": "test item",
            "abstract_item": self._abstract_item(item_type=item_type),
            "color_name": "clear",
            "color_hex": "#444444",
        }
        _base.update(kwargs)
        return models.Item.objects.get_or_create(**_base)[0]

    def _lightable(self, item_type, **kwargs):
        # this should be in a fixture or something!
        _abstract = kwargs.pop("abstract_item", None)
        if _abstract is None:
            _abstract = self._abstract_item(
                item_type=item_type,
                usable=True,
                untakable_if_active=True,
                stackable=True,
                active_adjective="lit",
            )
        _base = {
            "name": f"test {str(item_type).lower()}",
            "abstract_item": _abstract,
        }
        _base.update(kwargs)
        return self._item(**_base)

    def _candle(self, **kwargs):
        return self._lightable(ItemType.CANDLE, **kwargs)

    def _incense(self, **kwargs):
        return self._lightable(ItemType.INCENSE, **kwargs)
