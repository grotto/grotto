class ItemStackingHelper:
    def stack_items(self, items):
        ante_stacks = {}
        active_ante_stacks = {}
        non_stacking = []
        stacks = []
        for item in items:
            if item.abstract_item.stackable:
                if item.is_active:
                    active_ante_stacks.setdefault(item.abstract_item.pk, []).append(
                        item
                    )
                else:
                    ante_stacks.setdefault(item.abstract_item.pk, []).append(item)
            else:
                non_stacking.append(item)
        for _items in [v for k, v in sorted(active_ante_stacks.items())]:
            stacks.append(
                {
                    "abstract_item": _items[0].abstract_item,
                    "is_active": True,
                    "items": _items,
                }
            )
        for _items in [v for k, v in sorted(ante_stacks.items())]:
            stacks.append(
                {
                    "abstract_item": _items[0].abstract_item,
                    "is_active": False,
                    "items": _items,
                }
            )
        for _item in non_stacking:
            stacks.append(
                {
                    "abstract_item": _item.abstract_item,
                    "is_active": _item.is_active,
                    "items": [_item],
                }
            )
        return stacks
