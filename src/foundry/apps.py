from django.apps import AppConfig


class FoundryConfig(AppConfig):
    name = "foundry"
