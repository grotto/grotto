from grotto.services import BaseGenerationService, RandomColorService


class ItemGenerationService(BaseGenerationService):
    def generate(self, abstract_item):
        color, color_hex = RandomColorService().get_color()
        return {
            "name": self._enrich_template(
                template=abstract_item.item_name_template,
                abstract_item_name=abstract_item.name,
                color=color,
            ),
            "description": f"A {color} {abstract_item.name}",
            "color_name": color,
            "color_hex": color_hex,
        }
