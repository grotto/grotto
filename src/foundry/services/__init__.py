import re
from random import choice, shuffle, randint

from django.utils.timezone import now

from account.services import ban
from foundry.enum import ItemType
from foundry.models import AbstractItem, Item, PlacedTrap, Swap, Holding
from foundry.services.generation import ItemGenerationService
from grotto.services import escape, GrottoGameWarning, ServiceReturn
from grotto.models import BannedWord
from guild.models import Event, Kneel, Skill
from maze.enum import RoomRelationEnum
from maze.models import RoomExit


class ItemService:
    def create(self, *, abstract_item, character=None, room=None, npc=None):
        item_kwargs = ItemGenerationService().generate(abstract_item=abstract_item)
        item = Item.objects.create(
            abstract_item=abstract_item,
            character=character,
            room=room,
            npc=npc,
            cursed=randint(0, 19) == 0,
            **item_kwargs,
        )
        if character is not None:
            Holding.objects.create(item=item, character=character)
        return item

    def create_feces(self, *, character):
        abstract_item = choice(
            list(AbstractItem.objects.filter(item_type=ItemType.FECES))
        )
        return self.create(abstract_item=abstract_item, character=character)

    def use(self, *, item, character):  # Item model instance
        # validate that the item type exists
        ret = ServiceReturn()
        if item.abstract_item.item_type == ItemType.CANDLE:
            self._activate(item=item, character=character)
        if item.abstract_item.item_type in (ItemType.INCENSE,):
            self._activate(item=item, character=character)
            self.place(item=item, character=character)
        if item.abstract_item.item_type == ItemType.SCRUBBRUSH:
            ret.messages.append(self._use_scrubbrush(item=item, character=character))
        if item.abstract_item.item_type == ItemType.FECES:
            self._activate(item=item, character=character)
            self.place(item=item, character=character)
            self.perform_swaps(room=character.room, return_obj=ret)
            ret.messages.append("You have shit in the room")
        if item.abstract_item.item_type == ItemType.TUFFET:
            ret = self._kneel(item=item, character=character)
        if item.abstract_item.item_type == ItemType.JUNK:
            ret.messages.append("You're not sure how to use this")
        if item.abstract_item.item_type in (ItemType.ARROW, ItemType.TRAP):
            ret.messages.append("That's not how you use this")
        if item.abstract_item.item_type in (ItemType.POTION,):
            self.reskill(character=character)
            self.destroy(item=item)
            message = ItemGenerationService()._enrich_template(template="You feel {adjective}")
            ret.messages.append(message)
        return ret

    def reskill(self, *, character):
        Skill.objects.filter(character=character).delete()
        for skill, level in ItemGenerationService().skills().items():
            Skill.objects.create(
                name=skill,
                level=level,
                character=character,
            )

    def place(self, *, item, character, room=None):
        ret = ServiceReturn()
        item.character = None
        if room is None:
            room = character.room
        item.room = room
        item.save()
        _name = "The builder"
        if character is not None:
            _name = character.name
        Event.objects.create(
            room=room,
            text=f"{_name} placed {item.name}",
            character=character,
        )
        if item.abstract_item.item_type == ItemType.INCENSE and item.is_active:
            room.is_cursed = False
            room.save()
        if item.abstract_item.item_type == ItemType.FECES and item.is_active:
            room.cleanliness = 0
            room.save()
        return ret

    def take(self, *, item, character):
        ret = ServiceReturn()
        if item.room != character.room:
            ret.messages.append("You are not in the room with that item")
        elif item.is_active and item.abstract_item.untakable_if_active:
            # cannot pick up an active candle
            ret.messages.append("Cannot be taken when active")
        elif item.abstract_item.untakable:
            # cannot pick up a cenotaph
            ret.messages.append("Cannot be taken")
        else:
            item_count = character.inventory.filter(abstract_item__stackable=False).count()
            stacks = set()
            stackable_items = character.inventory.filter(abstract_item__stackable=True)
            for _item in stackable_items:
                stacks.add(_item.abstract_item.pk)
            if item_count + len(stacks) >= 20:
                ret.messages.append("Inventory is full, cannot take this.")
                return ret
            item.character = character
            item.room = None
            item.save()
            Holding.objects.create(item=item, character=character)
            Event.objects.create(
                room=character.room,
                text=f"{character.name} took {item.name}",
                character=character,
            )
            ret.messages.append(f"you picked {item.name} up")
            if item.related_cenotaph is not None:
                deleted, _ = RoomExit.objects.filter(
                    from_room=character.room,
                    to_room=item.related_cenotaph.room,
                    direction=RoomRelationEnum.ITEM_PORTAL,
                ).delete()
                _deleted, _ = RoomExit.objects.filter(
                    to_room=character.room,
                    from_room=item.related_cenotaph.room,
                    direction=RoomRelationEnum.ITEM_PORTAL,
                ).delete()
                if deleted + _deleted:
                    ret.messages.append("*SHWAP* A portal has closed")
        return ret

    def equip(self, *, item, character):
        ret = ServiceReturn()
        if item.character != character:
            ret.messages.append("You cannot equip an item you aren't holding")
        elif item.equipped:
            ret.messages.append("Item already equipped.")
        else:
            ret.messages.append(f"{item.name} equipped")
            if item.cursed:
                ret.messages.append(
                    f"Bad news. The {item.name} you equipped are cursed.")
            item.equipped = True
            item.save()
        return ret

    def drop(self, *, item, character):
        ret = ServiceReturn()
        # actually drop item
        room = character.room
        # check room size
        max_items = room.exits.count() * 5
        item_count = room.items.filter(abstract_item__stackable=False).count()
        if item_count >= max_items:
            ret.messages.append("Room is full, cannot drop this.")
            return ret
        stacks = set()
        stackable_items = room.items.filter(abstract_item__stackable=True)
        for _item in stackable_items:
            stacks.add(_item.abstract_item.pk)
        if item_count + len(stacks) >= max_items:
            ret.messages.append("Room is full, cannot drop this.")
            return ret
        item.character = None
        item.room = room
        item.equipped = False
        item.save()
        ret.messages.append(f"You have dropped {item.name}")
        Event.objects.create(
            room=room,
            text=f"{character.name} dropped {item.name}",
            character=character,
        )
        self.perform_swaps(room=room, return_obj=ret)
        # create portal
        if item.related_cenotaph is not None:
            if item.related_cenotaph.room != room:
                RoomExit.objects.get_or_create(
                    from_room=room,
                    to_room=item.related_cenotaph.room,
                    direction=RoomRelationEnum.ITEM_PORTAL,
                )
                RoomExit.objects.get_or_create(
                    to_room=room,
                    from_room=item.related_cenotaph.room,
                    direction=RoomRelationEnum.ITEM_PORTAL,
                )
                ret.messages.append(
                    f"*VWOOP* A portal opens to the {item.related_cenotaph.room}"
                )
        return ret

    def perform_swaps(self, *, room, return_obj):
        local_npcs = list(room.npcs.all())
        shuffle(local_npcs)
        for npc in local_npcs:
            if npc.room is None:
                continue
            self._perform_swaps(npc=npc, return_obj=return_obj)
        return return_obj

    def _perform_swaps(self, *, npc, return_obj=None):
        # check for anyone who wants item in room and drop resulting item
        for swap in Swap.objects.filter(abstract_npc=npc.abstract_npc):
            items = npc.room.items.filter(abstract_item__item_type=swap.picks_type)
            if swap.picks_abstract_item is not None:
                items = items.filter(abstract_item=swap.picks_abstract_item)
            for item in items:
                self._perform_swap(swap=swap, item=item, return_obj=return_obj)

    def _perform_swap(self, *, swap, item, return_obj=None):
        if return_obj is not None:
            return_obj.messages.append(swap.message)
        room = item.room
        created = self.create(abstract_item=swap.puts, room=room)
        self.destroy(item)
        Event.objects.create(
            room=room,
            text=f"{swap.abstract_npc} swapped {item.name} for {created.name}",
        )

    def _kneel(self, *, item, character):
        service_return = ServiceReturn()
        # find room
        room = character.room
        if room is None:
            return service_return
        cenotaph = None
        message = f"{character} knelt"
        if room.cleanliness != 2:
            message += " and got their knees dirty"
        # add Kneel record
        if hasattr(room, "cenotaph"):
            cenotaph = room.cenotaph
            Kneel.objects.create(character=character, cenotaph=cenotaph)
            message += f" at {cenotaph.name}'s cenotaph"

        message += f" in the {room.name}"
        # add room message
        Event.objects.create(
            room=room,
            text=message,
            character=character,
        )
        service_return.messages.append(message)
        return service_return

    def place_trap(self, *, item, exit, character):
        PlacedTrap.objects.create(item=item, exit=exit)
        self._activate(item=item, character=character)
        self.place(item=item, character=character, room=exit.from_room)

    def toggle_lock(self, *, item, exits, character, locked=None):
        for exit in exits:
            if locked is None:
                locked = not exit.locked
            exit.locked = locked
            exit.save()
        return locked

    def burnable_swap(self):
        """Check the room for burned out candles, incense and replace them
        with an appropriate junk item"""
        for item_type in (ItemType.CANDLE, ItemType.INCENSE):
            self._burnable_swap(item_type)

    def _burnable_swap(self, item_type):
        _name = f"Spent {str(item_type).capitalize()}"
        abstract_spent_item, _ = AbstractItem.objects.get_or_create(
            item_type=ItemType.JUNK,
            name=_name,
            description="It's all burned up!",
            stackable=True,
            item_name_template=_name,
        )
        for old_item in Item.objects.filter(
            abstract_item__item_type=item_type,
        ).expired():
            character = old_item.character
            self.create(
                abstract_item=abstract_spent_item,
                room=old_item.room,
                character=character,
            )
            if old_item.room is not None:
                Event.objects.create(
                    room=old_item.room,
                    text=f"{old_item.name} burned out",
                )
            elif character is not None and character.room is not None:
                Event.objects.create(
                    room=character.room,
                    text=f"{character.name}'s {old_item.name} burned out",
                    character=character,
                )
            old_item.delete()

    def break_shield(self, shield):
        if "durable" not in shield.name.lower():
            _name = "Broken Shield"
            abstract_spent_item, _ = AbstractItem.objects.get_or_create(
                item_type=ItemType.JUNK,
                name=_name,
                description="It's been smashed!",
                stackable=True,
                item_name_template="{abstract_item_name}",
            )
            self.create(
                abstract_item=abstract_spent_item,
                room=shield.room,
                character=shield.character,
            )

            if shield.room is not None:
                Event.objects.create(
                    room=shield.room,
                    text="A shield was destroyed.",
                )
            shield.delete()
            return True
        return False

    def spring_trap(self, trap_placement):
        if "durable" not in trap_placement.item.name.lower():
            _name = "Sprung Trap"
            abstract_spent_item, _ = AbstractItem.objects.get_or_create(
                item_type=ItemType.JUNK,
                name=_name,
                description="Someone stepped on this and dropped their viscera",
                stackable=True,
                item_name_template="{abstract_item_name}",
            )
            self.create(
                abstract_item=abstract_spent_item,
                room=trap_placement.item.room,
                character=trap_placement.item.character,
            )
            self.destroy(trap_placement.item)
            trap_placement.removed = now()
            trap_placement.save()
            return True
        return False

    def get_item(self, *, character, pk, holder="character", raises=GrottoGameWarning):
        get_kwargs = {"pk": pk}
        if holder == "character":
            get_kwargs.update({"character": character})
        elif holder == "room":
            get_kwargs.update({"room": character.room})
        try:
            item = Item.objects.get(**get_kwargs)
        except Item.DoesNotExist:
            raise raises("Item doesn't exist")
        return item

    def destroy(self, item):
        item.room = None
        item.character = None
        item.save()

    def _activate(self, *, item, character):
        if item.active:
            return
        item.active = now()
        item.save()

    def _use_scrubbrush(self, *, item, character):
        room = character.room
        # clean up feces first
        poop_qs = room.items.filter(
            abstract_item__item_type=ItemType.FECES,
            active__isnull=False,
        )
        if poop_qs.count():
            poop_qs[0].delete()
            Event.objects.create(
                room=room,
                text=f"{character.name} cleaned feces out of the room",
                character=character,
            )
            return "You have cleaned feces"
        new_cleanliness = min(2, room.cleanliness + 1)
        if new_cleanliness == room.cleanliness:
            return "The room is already very clean!"
        room.cleanliness = new_cleanliness
        room.save()
        Event.objects.create(
            room=room,
            text=f"{character.name} cleaned the room",
            character=character,
        )
        return "You have cleaned the room"

    def _use_amulet(self, *, item, character):
        pass

    def compose(self, *, item, character, message):
        service_return = ServiceReturn()
        _tokens = re.split(r"\W+", message.lower())
        if BannedWord.objects.filter(word__in=_tokens).exists():
            # log out the user; ban the account.
            ban(character.user, reason=f"wrote note: '{message}'")
            service_return.messages.append("goodbye")
            return service_return
        buffer = escape(message)
        buffer += "<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&mdash;"
        buffer += f"{character.name} the {character.kind}"
        item.description = buffer
        item.save()
        self._activate(item=item, character=character)
        self.place(item=item, character=character)
        service_return.messages.append("You have placed a letter")
        return service_return
