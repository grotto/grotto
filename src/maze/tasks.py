from django.core.management import call_command

from grotto import celery_app
from maze.models import Room
from maze.services import CenotaphActivationService


@celery_app.task(name="set_origin_room")
def set_origin_room(*, room_pk):
    Room.objects.get(pk=room_pk)
    call_command("sort_rooms_to_levels", origin_room=room_pk)


@celery_app.task(name="check_activations")
def check_activations():
    CenotaphActivationService().check_activations()
