from django.urls import path
from django.views.generic.base import TemplateView

from maze import views

app_name = "maze"
urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path(
        "character-map/",
        TemplateView.as_view(template_name="magicmap.html"),
        name="map",
    ),
    path("<slug:color_slug>/", views.RoomDetailView.as_view(), name="room"),
    path("cenotaph/<slug:color_slug>/", views.CenotaphView.as_view(), name="cenotaph"),
    path("graphics/<int:pk>/", views.RoomGraphicsView.as_view(), name="graphics"),
    path("cenotaph/<int:pk>/relic/", views.CenotaphRelicView.as_view(), name="relic"),
]
