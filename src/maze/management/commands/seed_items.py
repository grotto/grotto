from django.core.management.base import BaseCommand

from maze.models import Room
from maze.services import RoomService


class Command(BaseCommand):
    help = "Puts some random items in the maze"

    def handle(self, **kwargs):
        for room in Room.objects.all():
            RoomService().populate(room)
