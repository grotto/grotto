from random import choice

from grotto.management.base.csv import CsvImportCommand
from maze.enum import RoomAttributeEnum
from maze.models import ActivationCriteria, Cenotaph


class Command(CsvImportCommand):
    model_class = ActivationCriteria
    enum_fields = {
        "attribute": RoomAttributeEnum,
    }
    duration_fields = ("activation_duration",)

    def initialize(self):
        _c = {}
        for cenotaph in Cenotaph.objects.all():
            _c.setdefault(cenotaph.name, []).append(cenotaph)
        self.cenotaphs_by_name = _c

    def _preprocess_row(self, row):
        return {"name": row.pop("name")}

    def _pre_save_row(self, row, *, preprocess_output):
        try:
            cenotaph = choice(self.cenotaphs_by_name[preprocess_output["name"]])
        except (KeyError, IndexError):
            raise self.PreSaveException(
                f"Cannot find cenotaph for {preprocess_output['name']}"
            )
        row["room_id"] = cenotaph.room_id
