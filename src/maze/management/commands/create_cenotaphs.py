from django.core.management.base import BaseCommand
from python_gedcom_2.element.individual import IndividualElement
from python_gedcom_2.parser import Parser

from grotto.services import RandomColorService
from maze.enum import RoomRelationEnum
from maze.models import Cenotaph, RoomExit
from maze.services import RoomService


class Command(BaseCommand):
    help = "Intakes GEDCOM files to create a maze."

    def add_arguments(self, parser):
        # arg for hostname
        parser.add_argument(
            "--gedcom_file",
            "-g",
        )

    def _normalize_unknowns(self, value):
        if value in ("", -1, None):
            value = None
        return value

    def handle_child_element(self, element):
        # look up cenotaph
        self._get_or_create_cenotaph(element)

    def _get_or_create_cenotaph(self, element):
        verbose = {
            "birth": " in an unknown year",
            "death": " in an unknown year",
            "birthplace": "",
            "deathplace": "",
            "occupation": "",
        }
        fields = {
            "birth": ("get_birth_year",),
            "death": ("get_death_year",),
            "birthplace": ("get_birth_data", 1),
            "deathplace": ("get_death_data", 1),
        }
        defaults = {}
        for field, address in fields.items():
            prelim = getattr(element, address[0])()
            if len(address) > 1:
                prelim = prelim[address[1]]
            defaults[field] = self._normalize_unknowns(prelim)
            if defaults[field] is not None:
                verbose[field] = f" in {defaults[field]}"

        name = " ".join(element.get_name())
        occupation = self._normalize_unknowns(element.get_occupation())
        gender = element.get_gender()

        if gender == "M":
            posPronoun = "his"
            pronoun = "he"
        elif gender == "F":
            posPronoun = "her"
            pronoun = "she"
        else:
            posPronoun = "their"
            pronoun = "they"

        f_occupation = ""
        if occupation is not None:
            f_occupation = (
                f"During {posPronoun} life, {pronoun} worked as a {occupation}.\n"
            )

        defaults["text"] = (
            "{name} was born{verbose[birth]}{verbose[birthplace]}.\n"
            "{occupation}"
            "{pronoun} died{verbose[death]}{verbose[deathplace]}.\n"
        ).format(
            name=name,
            verbose=verbose,
            pronoun=pronoun.capitalize(),
            occupation=f_occupation,
        )

        birth = defaults.pop("birth")
        death = defaults.pop("death")
        try:
            cenotaph = Cenotaph.objects.get(
                name=name,
                birth=birth,
                death=death,
            )
        except Cenotaph.DoesNotExist:
            # create a room for this person
            room = RoomService(color_service=self.color_service).generate()
            cenotaph = Cenotaph.objects.create(
                name=name,
                birth=birth,
                death=death,
                room=room,
                **defaults,
            )
        else:
            for field, value in defaults.items():
                setattr(cenotaph, field, value)
                cenotaph.save()
        return cenotaph

    def _create_child_room_relation(self, parent, child):
        RoomExit.objects.update_or_create(
            from_room_id=child.id,
            to_room_id=parent.id,
            defaults={
                "direction": RoomRelationEnum.DOWN,
            },
        )
        RoomExit.objects.update_or_create(
            from_room_id=parent.id,
            to_room_id=child.id,
            defaults={
                "direction": RoomRelationEnum.UP,
            },
        )

    def __create_room_relation(self, r1, r2, relation=RoomRelationEnum.SAME):
        if r1 == r2:
            return
        RoomExit.objects.update_or_create(
            from_room_id=r1.id,
            to_room_id=r2.id,
            defaults={
                "direction": relation,
            },
        )
        RoomExit.objects.update_or_create(
            from_room_id=r2.id,
            to_room_id=r1.id,
            defaults={
                "direction": relation,
            },
        )

    def _create_mutual_relations(self, collection, relation=RoomRelationEnum.SAME):
        rooms = []
        for peer in collection:
            rooms.append(self._get_or_create_cenotaph(peer).room)
        for peer in rooms:
            for _peer in rooms:
                self.__create_room_relation(peer, _peer, relation=relation)

    def _create_sibling_relations(self, siblings):
        self._create_mutual_relations(siblings, relation=RoomRelationEnum.SAME)

    def _create_spouse_relations(self, spouses):
        self._create_mutual_relations(spouses, relation=RoomRelationEnum.PORTAL)

    def handle(self, *args, gedcom_file, **options):
        self.color_service = RandomColorService()
        parser = Parser()

        # Parse your file
        if isinstance(gedcom_file, (str,)):
            parser.parse_file(gedcom_file, False)  # Disable strict parsing
        else:
            parser.parse(gedcom_file, False)

        root_child_elements = (
            parser.get_root_child_elements()
        )  # what is this? is it a list?

        # first data pass creates Cenotaphs
        for element in root_child_elements:
            if isinstance(element, IndividualElement):
                self.handle_child_element(element)
            else:
                print(">> Non IndividualElement")
                print(type(element))
                print(element)

        # second data pass creates familial relationships
        for element in root_child_elements:
            if isinstance(element, IndividualElement):
                cenotaph = self._get_or_create_cenotaph(element)
                print(cenotaph.name)
                room = cenotaph.room
                for idx, family in enumerate(parser.get_families(element)):
                    children = parser.get_family_members(family, "CHIL")
                    self._create_sibling_relations(children)
                    for child in children:
                        child_cenotaph = self._get_or_create_cenotaph(child)
                        print(f">> is parent to {child_cenotaph.name} (family {idx})")
                        # create a room relation for this
                        self._create_child_room_relation(room, child_cenotaph.room)

                    spouses = parser.get_family_members(family, "PARENTS")
                    self._create_spouse_relations(spouses)
                    for spouse in spouses:
                        spouse_cenotaph = self._get_or_create_cenotaph(spouse)
                        print(f">> is spouse to {spouse_cenotaph.name} (family {idx})")
            else:
                print("!! Non IndividualElement")
                print(type(element))
                print(element.to_gedcom_string())
