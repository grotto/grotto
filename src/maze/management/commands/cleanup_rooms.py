from django.core.management.base import BaseCommand

from foundry.models import Item
from guild.models import NonPlayerCharacter, NonPlayerCharacterDeath
from maze.models import Cenotaph, Room


class Command(BaseCommand):
    def handle(self, *args, **options):
        Item.objects.all().update(related_cenotaph=None)
        NonPlayerCharacter.objects.all().update(room=None)
        NonPlayerCharacterDeath.objects.all().delete()

        Cenotaph.objects.all().delete()
        Room.objects.all().delete()
