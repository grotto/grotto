from random import randint

from django.core.management.base import BaseCommand

from foundry.models import AbstractItem
from foundry.services import ItemService
from maze.enum import RoomRelationEnum
from maze.models import RoomExit


class Command(BaseCommand):
    help = "Locks all stairways and puts traps on doors"

    def handle(self, **kwargs):
        RoomExit.objects.filter(
            direction__in=(RoomRelationEnum.UP, RoomRelationEnum.DOWN)
        ).update(locked=True)

        trap_count = 0
        item_service = ItemService()
        _trap = AbstractItem.objects.get(name="Booby Trap")
        for exit in RoomExit.objects.all():
            # no traps on first floor or higher
            from_level = exit.from_room.level or 0
            to_level = exit.to_room.level or 0
            if from_level <= 0 or to_level <= 0:
                continue

            # lvl2 -> lvl10
            # 1/20 -> 1/10
            # 0.05 -> 0.1
            # 2P = 0.00625 * L - 0.0375
            # 1 / P = N
            # N = 1 / (0.003125 * L - 0.01875)
            N = int(1 / ((0.003125 * exit.from_room.level) + 0.01875))
            if randint(0, N) == 0:
                # trap!
                trap = item_service.create(abstract_item=_trap)
                item_service.place_trap(item=trap, exit=exit, character=None)
                trap_count += 1
                print(
                    f"trap placed (#{trap_count} on exit {exit.pk} from "
                    f"room {exit.from_room_id}"
                )
