from django.core.management.base import BaseCommand

from foundry.enum import ItemType
from foundry.models import AbstractItem
from foundry.services import ItemService
from maze.models import Room


class Command(BaseCommand):
    def handle(self, *args, **options):
        abstract_candle = AbstractItem.objects.filter(
            item_type=ItemType.CANDLE,
        )[0]

        service = ItemService()
        for room in Room.objects.all():
            candle = service.create(abstract_item=abstract_candle, room=room)
            service._activate(item=candle, character=None)
