from django.core.management.base import BaseCommand

from grotto.models import StartingRoom
from maze.enum import RoomRelationEnum
from maze.models import Room, RoomExit
from maze.services import RoomService


class Command(BaseCommand):
    help = "Assigns rooms to dungeon levels based on gedcom generations"
    czech_names = (
        "Friedel",
        "Fridel",
        "Fridl",
        "Regmund",
        "Kopecky",
        "Kopecký",
        "Sulak",
        "Kadanka",
        "Spacek",
        "Lero",
        "Holub",
    )

    def add_arguments(self, parser):
        parser.add_argument("--origin_room", "-o", type=int)

    def _get_room_exits(
        self, *, room_id, directions=(RoomRelationEnum.SAME, RoomRelationEnum.PORTAL)
    ):
        exits = RoomExit.objects.filter(from_room_id=room_id, direction__in=directions)
        return exits

    def _crawl_room(self, *, room_id, level=0):
        if room_id in self.room_levels:
            return

        self.room_levels[room_id] = level
        self.levels.setdefault(level, set()).add(room_id)

        for exit in self._get_room_exits(room_id=room_id):
            # same level rooms
            self._crawl_room(room_id=exit.to_room_id, level=level)

        down_rooms = self._get_room_exits(
            room_id=room_id, directions=(RoomRelationEnum.DOWN,)
        )
        for exit in down_rooms:
            self._crawl_room(room_id=exit.to_room_id, level=level + 1)

        up_rooms = self._get_room_exits(
            room_id=room_id, directions=(RoomRelationEnum.UP,)
        )
        for exit in up_rooms:
            self._crawl_room(room_id=exit.to_room_id, level=level - 1)

    def handle(self, *, origin_room, **kwargs):
        self.levels = {}
        self.room_levels = {}
        StartingRoom.objects.create(room_id=origin_room)
        self._crawl_room(room_id=origin_room)
        print(self.levels)
        captured_count = 0
        for level, peers in self.levels.items():
            captured_count += len(peers)
            print(f"Level {level}: {len(peers)} rooms")
        print(f"{captured_count} rooms tallied in {len(self.levels.keys())} levels")

        room_service = RoomService()

        for room_id, level in self.room_levels.items():
            # fix the desciptions to represent level and surname
            name = Room.objects.get(id=room_id).cenotaph.name

            kwargs = {}
            czech = False
            for family_name in self.czech_names:
                if family_name in name:
                    czech = True
                    break
            if czech:
                if 3 <= level <= 5:
                    kwargs[
                        "corpus"
                    ] = "word_lists/corpuses/czechReadingBook1892_corpus.txt"
            else:
                if 3 < level < 5:
                    kwargs["corpus"] = "word_lists/corpuses/galveston_corpus.txt"
                elif 5 <= level < 7:
                    kwargs["corpus"] = "word_lists/corpuses/cathedrals_corpus.txt"
                elif level >= 7:
                    kwargs["corpus"] = "word_lists/corpuses/caves_corpus.txt"

            description = room_service.description(**kwargs)
            print(room_id)
            print(level)
            print(kwargs.get("corpus"))
            print(description)

            Room.objects.filter(id=room_id).update(
                level=level,
                description=description,
            )
