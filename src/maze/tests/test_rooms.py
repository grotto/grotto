from django.test import TestCase
from django.utils.timezone import now

from foundry.tests.base import ItemBuilderModelMixin
from guild.tests.base import CharacterBuilderModelMixin
from maze import models
from maze.services import RoomAdjacencyService, RoomService
from maze.tests.base import MapBuilderModelMixin


class RoomAdjacencyServiceTestCase(TestCase):
    def test_initial_room_generation(self):
        self.assertEqual(models.Room.objects.count(), 0)
        room_service = RoomService()
        room1 = room_service.generate()
        self.assertEqual(models.Room.objects.count(), 1)
        room2 = room_service.generate()
        self.assertEqual(models.Room.objects.count(), 2)
        self.assertEqual(room1.exits.all()[0], room2)

        rooms = [room1, room2]
        for idx in range(20):
            new_room = room_service.generate()
            self.assertNotEqual(new_room.exits.count(), 0)

        for room in models.Room.objects.all():
            self.assertLessEqual(room.exits.count(), 4)

    def test_adjacency_reroll(self):
        room_service = RoomService()
        [room_service.generate() for i in range(20)]
        RoomAdjacencyService().reorganize_rooms()


class RoomManagerTestCase(
    TestCase, MapBuilderModelMixin, ItemBuilderModelMixin, CharacterBuilderModelMixin
):
    def test_lit_candle_count(self):
        _rooms = []
        for i in range(5):
            _rooms.append((i, self._room(name=f"test{i}")))

        # create candles and light several of them
        for idx, room in _rooms:
            for jdx in range(10):
                _active = None
                if jdx < idx:
                    _active = now()
                self._candle(active=_active, room=room)
            # create another active item
            self._incense(active=now(), room=room)

        # query
        rooms = {r.pk: r for r in models.Room.objects.all()}
        for idx, _room in _rooms:
            room = rooms[_room.pk]
            self.assertEqual(room.lit_candle_count, idx)
            self.assertEqual(room.lit_incense_count, 1)

        dark_rooms = models.Room.objects.all().dark()
        self.assertEqual(dark_rooms.count(), 1)
        lit_rooms = models.Room.objects.all().dark(exclude=True)
        self.assertEqual(lit_rooms.count(), 4)
        self.assertNotIn(_rooms[0][1].pk, lit_rooms.values_list("pk", flat=True))

    def test_lit_incense_count(self):
        _rooms = []
        for i in range(5):
            _rooms.append((i, self._room(name=f"test{i}")))

        # create candles and light several of them
        for idx, room in _rooms:
            for jdx in range(10):
                _active = None
                if jdx < idx:
                    _active = now()
                self._incense(active=_active, room=room)
            # create another active item
            self._candle(active=now(), room=room)

        # query
        rooms = {r.pk: r for r in models.Room.objects.all()}
        for idx, _room in _rooms:
            room = rooms[_room.pk]
            self.assertEqual(room.lit_incense_count, idx)
            self.assertEqual(room.lit_candle_count, 1)

        sanctified_rooms = models.Room.objects.all().sanctified()
        self.assertEqual(sanctified_rooms.count(), 4)
        unsanctified_rooms = models.Room.objects.all().sanctified(exclude=True)
        self.assertEqual(unsanctified_rooms.count(), 1)
        self.assertIn(_rooms[0][1].pk, unsanctified_rooms.values_list("pk", flat=True))

    def test_lit_character_candle_count(self):
        _room = self._room(name=f"test0")
        _other_room = self._room(name=f"test1")
        _third_room = self._room(name=f"test2")
        _character = self._character(room=_room)
        self._candle(active=now(), character=_character)
        self._candle(active=now(), character=_character)
        _other_character = self._character(room=_room)

        dark_rooms = models.Room.objects.all().dark()
        self.assertEqual(dark_rooms.count(), 2)
        self.assertNotIn(_room.pk, dark_rooms.values_list("pk", flat=True))
        lit_rooms = models.Room.objects.all().dark(exclude=True)
        self.assertEqual(lit_rooms.count(), 1)
        self.assertEqual(lit_rooms[0].lit_candle_count, 0)
        self.assertEqual(lit_rooms[0].lit_character_candle_count, 2)
        self.assertIn(_room.pk, lit_rooms.values_list("pk", flat=True))

        self._candle(active=now(), character=_other_character)
        lit_rooms = models.Room.objects.all().dark(exclude=True)
        self.assertEqual(lit_rooms[0].lit_character_candle_count, 3)
