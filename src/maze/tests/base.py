from maze import models


class MapBuilderModelMixin:
    def _room(self, **kwargs):
        _base = {
            "name": "test0",
            "description": "just for testing",
            "status": "ass",
            "color_name": "clear",
            "color_hex": "#444444",
        }
        _base.update(kwargs)
        return models.Room.objects.get_or_create(**_base)[0]
