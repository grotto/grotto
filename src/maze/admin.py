import csv
from io import StringIO
from tempfile import TemporaryFile

from django.contrib import admin
from django.http import FileResponse
from django.utils import timezone

from grotto import celery_app
from maze import models


@admin.action(description="Make selected the root room (reorganize maze)")
def make_room_origin(modeladmin, request, queryset):
    # ensure only one item in the queryset
    assert queryset.count() == 1, "too many!"
    record = queryset[0]

    celery_app.send_task(
        "set_origin_room",
        kwargs={"room_pk": record.pk},
    )


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ("name", "color_hex", "level")
    actions = [make_room_origin]


class CenotaphFileInline(admin.TabularInline):
    fields = ("file", "slug", "alt_text")
    model = models.CenotaphFile


def _export_cenotaph_row(row):
    columns = {
        "id": ("id",),
        "name": ("name",),
        "birth": ("birth",),
        "room_level": ("room", "level"),
    }
    _row = {}
    for attr, addr in columns.items():
        _ptr = row
        for token in addr:
            _ptr = getattr(_ptr, token)
        _row[attr] = str(_ptr)
    return _row


@admin.action(description="Export Cenotaphs with their primary keys")
def export_cenotaphs(modeladmin, request, queryset):
    _rows = []
    for row in queryset:
        _rows.append(_export_cenotaph_row(row))

    with StringIO() as f:
        writer = csv.DictWriter(f, fieldnames=["id", "name", "birth", "room_level"])
        writer.writeheader()
        for _row in _rows:
            writer.writerow(_row)
        f.seek(0)
        contents = f.read(None)
        tf = TemporaryFile()
        tf.write(contents.encode())
        # print(tf)
        tf.seek(0)
        response = FileResponse(
            tf, filename=f"cenotaphs_{timezone.now()}.csv", as_attachment=True
        )
        return response


@admin.register(models.Cenotaph)
class CenotaphAdmin(admin.ModelAdmin):
    list_display = ("name", "id", "room", "room_level", "room_id")
    inlines = (CenotaphFileInline,)
    actions = [export_cenotaphs]

    @admin.display(ordering="room__level")
    def room_level(self, obj):
        return obj.room.level

    @admin.display(ordering="room__id")
    def room_id(self, obj):
        return obj.room.id


@admin.register(models.ActivationCriteria)
class ActivationCriteriaAdmin(admin.ModelAdmin):
    list_display = (
        "room",
        "ref",
        "attribute",
        "required_level",
        "satisfied",
        "first_satisfied",
    )
