from random import choice

from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import redirect
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.conf import settings

from account.views import LoginRequiredMixin
from foundry.enum import ItemType
from maze.models import Cenotaph, Room
from maze.services import RoomService
from grotto.api.views import TableauSerializerMixin


class Index(ListView):
    template_name = "index.html"
    model = Room
    paginate_by = 25

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["now"] = timezone.now()
        return context

    def post(self, request):
        if request.user.is_staff:
            RoomService().generate()
        return redirect(".")  # points the user right back where they came from


class RoomDetailView(DetailView):
    model = Room
    template_name = "room.html"
    query_pk_and_slug = True
    slug_url_kwarg = "color_slug"
    slug_field = "color_slug"
    sanctity_adjectives = ("Cursed", "Mundane", "Sacred")
    cleanliness_adjectives = ("Profane", "Dirty", "Clean")
    actions = []

    def _room_level(self, item_type):
        room = self.object
        # check items in room
        _item = room.items.filter(
            abstract_item__item_type=item_type, active__isnull=False
        )

        active_item_count = sum([1 for candle in _item if candle.is_active])
        # active_item_count = 1
        if active_item_count > 0:
            return 2
        # check characters in room
        for character in room.occupants.all():
            # see if character has a candle
            _item = character.inventory.filter(
                abstract_item__item_type=item_type, active__isnull=False
            )
            active_item_count = sum([1 for candle in _item if candle.is_active])
            if active_item_count > 0:
                return 1
        return 0

    def get_room_level(self, descriptor="illumination"):
        room = self.object
        if descriptor == "cleanliness":
            return room.cleanliness
        if descriptor == "illumination":
            return self._room_level(ItemType.CANDLE)
        if descriptor == "sanctity":
            if room.is_cursed:
                return 0
            if self._room_level(ItemType.INCENSE) == 2:
                return 2
            return 1

    def get_room_adjective(self, descriptor):
        _level = self.get_room_level(descriptor)
        adjectives = getattr(self, f"{descriptor}_adjectives")
        try:
            return adjectives[_level]
        except IndexError:
            return "what?"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "visits": self.object.visits,
                "illumination_level": self.get_room_level("illumination"),
                "sanctity_adjective": self.get_room_adjective("sanctity"),
                "cleanliness_adjective": self.get_room_adjective("cleanliness"),
            }
        )
        return context

    def get_object(self, queryset=None):
        """
        Return the object the view is displaying.
        Require `self.queryset` and a `pk` or `slug` argument in the URLconf.
        Subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()
        # Next, try looking up by primary key.
        pk = self.kwargs.get(self.pk_url_kwarg)
        slug = self.kwargs.get(self.slug_url_kwarg)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        # Next, try looking up by slug.
        if slug is not None and (pk is None or self.query_pk_and_slug):
            slug_field = self.get_slug_field()
            queryset = queryset.filter(**{slug_field: slug})
        # If none of those are defined, it's an error.
        if pk is None and slug is None:
            raise AttributeError(
                "Generic detail view %s must be called with either an object "
                "pk or a slug in the URLconf." % self.__class__.__name__
            )
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(
                "No %(verbose_name)s found matching the query"
                % {"verbose_name": queryset.model._meta.verbose_name}
            )
        except self.model.MultipleObjectsReturned:
            for idx, _obj in enumerate(queryset):
                if idx == 0:
                    obj = _obj
                else:
                    _obj.color_slug += f"-{idx}"
                    _obj.save()
        return obj

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CenotaphView(DetailView):
    model = Room
    query_pk_and_slug = True
    slug_url_kwarg = "color_slug"
    slug_field = "color_slug"
    template_name = "cenotaph.html"

    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        try:
            return obj.cenotaph
        except ObjectDoesNotExist:
            raise Http404("No cenotaph here")


class RoomGraphicsView(LoginRequiredMixin, TableauSerializerMixin, DetailView):
    template_name = "graphics.html"
    queryset = Room.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = kwargs.pop("object")
        user = self.request.user
        context.update(
            {
                "graphics_context": {
                    "user": {
                        "pk": user.pk,
                        "is_anonymous": user.is_anonymous,
                        "is_staff": user.is_staff,
                    },
                    "base_url": settings.BASE_URL,
                    "raw_tableau": self.get_tableau_serializer(character=None, room_pk=instance.pk).data,
                }
            }
        )
        return context


class CenotaphRelicView(LoginRequiredMixin, DetailView):
    template_name = "item.html"
    queryset = Cenotaph.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.get_object()
        relics = list(instance.relics.all())
        relic = None
        if relics:
            relic = choice(relics)
        context.update({
            "relic": relic
        })
        return context
