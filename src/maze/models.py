import re
from datetime import timedelta
from random import shuffle

from colorfield.fields import ColorField
from django.db import models
from django.utils.timezone import now
from django_enumfield.enum import EnumField

from foundry.enum import ItemType
from foundry.helpers import ItemStackingHelper
from grotto.models import InlineFileModel
from grotto.validators import validate_inline_upload
from maze.enum import RoomAttributeEnum, RoomRelationEnum


class RoomExit(models.Model):
    from_room = models.ForeignKey(
        "Room", on_delete=models.CASCADE, related_name="exits_from"
    )
    to_room = models.ForeignKey(
        "Room", on_delete=models.CASCADE, related_name="exits_to"
    )
    direction = EnumField(RoomRelationEnum)
    locked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.from_room} -> {self.to_room}"

    @property
    def visible_traps(self):
        return self.traps.filter(removed=None)


class RoomManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        # add annotation for active candles in room
        lit_candle_count = models.Count(
            "items",
            filter=(
                models.Q(
                    items__abstract_item__item_type=ItemType.CANDLE,
                    items__active__isnull=False,
                )
            ),
            distinct=True,
        )
        # add annotation for active candles in character inv in room
        lit_character_candle_count = models.Count(
            "occupants__inventory",
            filter=(
                models.Q(
                    occupants__inventory__abstract_item__item_type=ItemType.CANDLE,
                    occupants__inventory__active__isnull=False,
                )
                | models.Q(
                    npcs__loot__abstract_item__item_type=ItemType.CANDLE,
                )
            ),
            distinct=True,
        )
        # add annotation for active incense in room
        lit_incense_count = models.Count(
            "items",
            filter=(
                models.Q(
                    items__abstract_item__item_type=ItemType.INCENSE,
                    items__active__isnull=False,
                )
            ),
            distinct=True,
        )
        active_feces_count = models.Count(
            "items",
            filter=(
                models.Q(
                    items__abstract_item__item_type=ItemType.FECES,
                    items__active__isnull=False,
                )
            ),
            distinct=True,
        )
        return qs.annotate(
            lit_candle_count=lit_candle_count,
            lit_character_candle_count=lit_character_candle_count,
            lit_incense_count=lit_incense_count,
            active_feces_count=active_feces_count,
            kneel_count=models.Count("cenotaph__kneels", distinct=True),
        )


class RoomQuerySet(models.QuerySet):
    def _do_filter_or_exclude(self, *, exclude, kwargs):
        if exclude:
            return self.exclude(**kwargs)
        return self.filter(**kwargs)

    def dark(self, exclude=False):
        # filter for lit_candle_count and player_lit_candle_count
        kwargs = {
            "lit_candle_count": 0,
            "lit_character_candle_count": 0,
        }
        return self._do_filter_or_exclude(exclude=exclude, kwargs=kwargs)

    def sanctified(self, exclude=False):
        # filter for lit_incense_count
        kwargs = {
            "lit_incense_count__gt": 0,
        }
        return self._do_filter_or_exclude(exclude=exclude, kwargs=kwargs)

    def dirty(self, exclude=False):
        # filter for cleanliness
        kwargs = {
            "cleanliness": 0,
        }
        return self._do_filter_or_exclude(exclude=exclude, kwargs=kwargs)

    def visited(self, user=None):
        if user is not None:
            return self.filter(
                models.Q(_visits__character__user=user) | models.Q(occupants__user=user)
            )
        return self.filter(_visits__isnull=False)


class Room(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    name = models.CharField(max_length=200)
    url = models.URLField(max_length=200, blank=True)
    exits = models.ManyToManyField("self", symmetrical=True, through=RoomExit)
    pub_date = models.DateTimeField(default=now, blank=True)
    description = models.TextField()
    color_name = models.CharField(max_length=200)

    # wtf is status?
    status = models.CharField(max_length=200, blank=True)
    color_hex = ColorField(default="#222222")
    color_slug = models.SlugField(null=True)
    # video embeds
    hosted_video_link = models.URLField(max_length=200, blank=True)
    vimeo_id = models.CharField(max_length=200, blank=True)
    youtube_id = models.CharField(max_length=200, blank=True)
    cleanliness = models.IntegerField(default=1)
    is_cursed = models.BooleanField(default=False)
    level = models.IntegerField(null=True, blank=True)

    objects = RoomManager.from_queryset(RoomQuerySet)()

    class Meta:
        unique_together = ["id", "url"]

    def __str__(self):
        return self.name

    def _room_level(self, *, item_type):
        # check items in room
        _item = self.items.filter(
            abstract_item__item_type=item_type, active__isnull=False
        )

        active_item_count = sum([1 for candle in _item if candle.is_active])
        # active_item_count = 1
        if active_item_count > 0:
            return 2
        # check characters in self
        for character in self.occupants.all():
            # see if character has a candle
            _item = character.inventory.filter(
                abstract_item__item_type=item_type, active__isnull=False
            )
            active_item_count = sum([1 for candle in _item if candle.is_active])
            if active_item_count > 0:
                return 1
        for npc in self.npcs.all():
            # see if npc has a candle
            _items = npc.loot.filter(
                abstract_item__item_type=item_type,
            )

            if _items:
                return 1
        return 0

    def get_attributes(self):
        sanctity = 0
        if not self.is_cursed:
            _sanctity = self._room_level(item_type=ItemType.INCENSE)
            if _sanctity == 0:
                sanctity = 1
            else:
                sanctity = 2
        return {
            "cleanliness": self.cleanliness,
            "brightness": self._room_level(item_type=ItemType.CANDLE),
            "sanctity": sanctity,
        }

    def warnings_for(self, character):
        if character is None:
            return []
        organs = character.inventory.filter(equipped=True).prefetch_related(
            "abstract_item__warning_subscriptions"
        )
        sensible_anpcs = set()
        for organ in organs:
            sensible_anpcs.update(organ.abstract_item.warning_subscriptions.all())
        decorators = {
            100: "∞",
            50: "⁂",
            20: "‡",
            10: "•",
            5: "≡",
            3: "=",
            2: "-",
        }
        _warnings = {}
        for exit in self.exits.exclude(exits_to__locked=True).prefetch_related("npcs"):
            for npc in exit.npcs.all():
                if npc.abstract_npc in sensible_anpcs:
                    _warnings.setdefault(npc.warning_text, 0)
                    _warnings[npc.warning_text] += 1

        # ensure that the warnings are in a random order so that the
        #  player can't deduce
        def decorate(value, count):
            # decorators
            buffer = ""
            for qty, sym in decorators.items():
                buffer += sym * (count // qty)
                count %= qty
            return f"{buffer[::-1]}{value}{buffer}"

        return [{"warning": decorate(w, c)} for w, c in _warnings.items()]

    @property
    def visible_items(self):
        qs = self.items.all()
        # if self._room_level(item_type=ItemType.CANDLE) == 0:
        #     qs = qs.none()
        return ItemStackingHelper().stack_items(qs)

    @property
    def visits(self):
        deduped_visits = []
        visitors = []
        visits = (
            self._visits.all()
            .filter(room=self)
            .exclude(stamp_date__lt=now() - timedelta(days=7))
            .order_by("-stamp_date")
        )
        for visit in visits:
            if visit.character not in visitors:
                deduped_visits.append(visit)
                visitors.append(visit.character)
        return deduped_visits

    @property
    def _cenotaph(self):
        if self.cenotaph and self.cenotaph.death:
            return self.cenotaph


def centaph_portrait_uploads(instance, filename):
    name = f"cenotaph/portraits/{filename}"
    return name


class ActivationCriteria(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    room = models.ForeignKey(
        Room, on_delete=models.CASCADE, related_name="activation_criteria"
    )
    attribute = EnumField(RoomAttributeEnum)
    required_level = models.SmallIntegerField()
    activation_duration = models.DurationField()
    first_satisfied = models.DateTimeField(null=True, blank=True)
    # These conditions will be checked by a periodic task, not continuously.
    satisfied = models.BooleanField(default=False)

    @property
    def satisfied_datetime(self):
        if self.first_satisfied is None:
            return None
        return self.first_satisfied + self.activation_duration


class Cenotaph(models.Model):
    ref = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text="tracks with spreadsheet for maintainance",
    )
    room = models.OneToOneField(Room, on_delete=models.PROTECT, related_name="cenotaph")
    text = models.TextField(
        help_text=(
            "The inscription on the cenotaph. Use [!image-slug] to reference "
            "an uploaded image from below"
        )
    )
    name = models.CharField(max_length=64)
    birth = models.CharField(max_length=32, null=True, blank=True)
    death = models.CharField(max_length=32, null=True, blank=True)
    birthplace = models.CharField(max_length=256, null=True, blank=True)
    burialplace = models.CharField(max_length=256, null=True, blank=True)
    deathplace = models.CharField(max_length=256, null=True, blank=True)
    scene = models.TextField(
        help_text="This 'a-frame' will be inserted in the cenotaph as background",
        null=True,
        blank=True,
    )
    portrait_filename = models.CharField(max_length=128, null=True, blank=True)
    portrait = models.FileField(
        upload_to=centaph_portrait_uploads,
        validators=[validate_inline_upload],
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name

    @property
    def _portrait_url(self):
        if self.portrait:
            return self.portrait.url

    @property
    def rendered_text(self):
        # This is a strong candidate for caching, but with a relatively short TTL (2 hour)
        _text = self.text
        # index the cenotaph files
        for file in self.files.all():
            # scan the `text` for image markup
            # replace markup with image tag
            _text = re.sub(file.placeholder(), file.markup(), _text)
        if self.is_active and self.relics.count() > 0:
            for relic in self.relics.all():
                if relic.room is None and relic.character is None and relic.npc is None:
                    # relic is lost... recall it?
                    relic.room = self.room
                    relic.save()
                    lcation = relic.room
                else:
                    location = relic.room or relic.character.room or relic.npc.room
                _text += f"<br/ >Relic: {relic.name} -- {location}"
        return _text

    @property
    def is_active(self):
        if self.room.activation_criteria.filter(satisfied=False).count() == 0:
            return True
        return False


# DEPRECATED
class RoomEvent(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name="_events")
    created = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=256)

    class Meta:
        ordering = ["-created"]


# This must be kept (misspelling and all) because it appears in an old migration!
def centaph_file_uploads(instance, filename):
    pass


class CenotaphFile(InlineFileModel):
    cenotaph = models.ForeignKey(
        Cenotaph, on_delete=models.CASCADE, related_name="files"
    )
