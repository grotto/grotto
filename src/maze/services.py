from random import choice, randint, sample

from django.db.models import Count
from django.utils import timezone
from django.utils.text import slugify

from foundry.models import AbstractItem
from foundry.services import ItemService
from grotto.services import BaseGenerationService, RandomColorService
from guild.models import Event
from maze.enum import RoomRelationEnum
from maze.models import Room, RoomExit


class RoomAdjacencyService:
    """Simple random adjacency for connecting rooms

    Usage
    ------

    While generating rooms call ``RoomAdjacencyService().add_room(<room>)``
    passing the newly generated room.

    Alternately, if rooms exist and you want the connections between them
    to be shuffled use ``RoomAdjacencyService().reorgainize_rooms()``

    """

    max_adjacency = 4
    min_adjacency = 1

    def _make_adjacent(self, room, neighbor, direction=RoomRelationEnum.SAME):
        # check room and neighbor for compliance in max_adjacency
        for _room in (room, neighbor):
            if _room.exits.count() + 1 > self.max_adjacency:
                raise Exception(f"Cannot add more rooms to {_room}")
        reverse = direction
        if direction == RoomRelationEnum.UP:
            reverse = RoomRelationEnum.DOWN
        elif direction == RoomRelationEnum.DOWN:
            reverse = RoomRelationEnum.UP
        RoomExit.objects.get_or_create(
            from_room=room, to_room=neighbor, direction=direction
        )
        RoomExit.objects.get_or_create(
            to_room=room, from_room=neighbor, direction=reverse
        )

    def _adjacency_candidates(self, room):
        candidates = (
            Room.objects.exclude(id=room.id)
            .annotate(num_exits=Count("exits"))
            .filter(num_exits__lt=self.max_adjacency, num_exits__gt=0)
        )
        return list(candidates)

    def reorganize_rooms(self):
        """Will remove all exits from all rooms and generate a new adjacency"""
        rooms = list(Room.objects.all())
        if len(rooms) < 2:
            return
        for room in rooms:
            room.exits.clear()
        starters = sample(rooms, k=2)
        self._make_adjacent(starters[0], starters[1])
        for room in rooms:
            if room in starters:
                continue
            self.add_room(room)

    def add_room(self, room):
        """Will find neighbors for a derelict room"""
        # if room already has adjacency then return
        if room.exits.count() > 0:
            return
        # find rooms which can be neighbors
        candidates = self._adjacency_candidates(room=room)

        neighbors = []
        if candidates:
            desired_adjacency_count = randint(
                self.min_adjacency, min(self.max_adjacency, len(candidates))
            )
            # randomly choose some rooms
            neighbors = sample(candidates, k=desired_adjacency_count)
        else:
            # are there no candidates because I'm the only room, or
            #  because the dance card is full?
            room_count = Room.objects.count()
            if room_count == 1:
                # only room, no exit
                pass
            else:
                # dance card full
                # randomly choose a set of rooms to divorce and squeeze between
                to_split = choice(list(Room.objects.exclude(id=room.id)))
                neighbors = [to_split]
                try:
                    # is there a connection to split?
                    also_to_split = choice(list(to_split.exits.all()))
                    to_split.exits.remove(also_to_split)
                except IndexError:
                    pass
                else:
                    neighbors.append(also_to_split)
        # make room adjacent to them
        for neighbor in neighbors:
            self._make_adjacent(room, neighbor)


class RoomService(BaseGenerationService):
    description_corpus = "word_lists/corpuses/room_corpus.txt"
    description_sentences = (
        ("", ()),
        ("", ()),
    )
    item_creation_probabilites = (
        ((50,), "Brazier"),
        ((25,), "Arrow"),
        ((15,), "Shield"),
        ((1, 5, 10, 20, 30, 35, 40, 45), "Junk"),
    )

    def __init__(self, *, color_service=None):
        self.color_service = color_service
        if color_service is None:
            self.color_service = RandomColorService()

    def description(self, *, name=None, corpus="word_lists/corpuses/homes_corpus.txt"):
        description = self._description(name=name)
        description += "\n"
        description += self._description(
            name=name,
            corpus=corpus,
        )
        return description

    def generate(self):
        # get color
        color_name, color_hex = self.color_service.get_color()
        # create instance
        room = Room.objects.create(
            name=color_name.capitalize() + " Room",
            description=self.description(),
            color_hex=color_hex,
            color_name=color_name,
            color_slug=slugify(color_name),
        )
        Event.objects.create(room=room, text=f"{room.name} was created")
        return room

    def populate(self, room):
        roll = randint(1, 50)
        for rolls, abstract_item_name in self.item_creation_probabilites:
            if roll in rolls:
                abstract_item = AbstractItem.objects.get(name=abstract_item_name)
                # give this room a lantern
                item = ItemService().create(abstract_item=abstract_item, room=room)
                if abstract_item_name == "Brazier":
                    ItemService().use(item=item, character=None)
        # return room


class CenotaphActivationService:
    def check_activation(self, room):
        attributes = room.get_attributes()
        satisfied = True
        for criteria in room.activation_criteria.all():
            if criteria.required_level == attributes[criteria.attribute.label]:
                # currently satisfied
                if criteria.first_satisfied is not None:
                    if timezone.now() > criteria.satisfied_datetime:
                        criteria.satisfied = True
                        criteria.save()
                else:
                    criteria.first_satisfied = timezone.now()
                    criteria.save()
            else:
                satisfied = False
                criteria.first_satisfied = None
                criteria.satisfied = False
                criteria.save()

        relics = room.cenotaph.relics.all()
        if satisfied:
            for relic in relics:
                if relic is not None and relic.cenotaph == room.cenotaph:
                    # [treasure sound] release the relic into the room
                    relic.room = room
                    relic.cenotaph = None
                    relic.save()
        else:
            for relic in relics:
                if relic is not None and relic.cenotaph != room.cenotaph:
                    # recall the relic
                    RoomExit.objects.filter(
                        from_room=relic.room,
                        to_room=relic.related_cenotaph.room,
                        direction=RoomRelationEnum.ITEM_PORTAL,
                    ).delete()
                    relic.room = None
                    relic.character = None
                    relic.npc = None
                    relic.cenotaph = room.cenotaph
                    relic.save()

    def check_activations(self):
        for room in Room.objects.all().order_by("level"):
            self.check_activation(room)
