from django.db.models import Count
from rest_framework import mixins, viewsets
from rest_framework.pagination import PageNumberPagination

from maze import models
from maze.api import serializers


class CenotaphViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    lookup_url_kwarg = "pk"
    serializer_class = serializers.CenotaphSerializer
    queryset = models.Cenotaph.objects.all()


class VisitedRoomViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """Shows the rooms that the _user_ has seen"""

    serializer_class = serializers.VisitedRoomSerializer
    queryset = models.Room.objects.visited()

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return self.queryset
        return models.Room.objects.visited(user=self.request.user)


class RoomViewSet(
    viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin
):
    serializer_class = serializers.SimpleRoomSerializer
    queryset = models.Room.objects.filter(level__isnull=False).annotate(
        exit_count=Count("exits_from", distinct=True)
    )
    pagination_class = PageNumberPagination
