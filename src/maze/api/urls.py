from rest_framework.routers import DefaultRouter

from maze.api import views

app_name = "maze-api"

router = DefaultRouter()
router.register("cenotaphs", views.CenotaphViewSet, basename="cenotaph")
router.register("visited", views.VisitedRoomViewSet, basename="visited")
router.register("rooms", views.RoomViewSet, basename="rooms")

urlpatterns = router.urls
