from django_enumfield.contrib.drf import NamedEnumField
from rest_framework import serializers

from foundry.api.serializers import ItemStackSerializer, PlacedTrapSerializer
from guild.api.serializers import (
    EventSerializer,
    NonPlayerCharacterSerializer,
    OccupantSerializer,
    RoomVisitSerializer,
)
from guild.models import RoomWarning, Visit
from maze import models
from maze.enum import RoomRelationEnum, RoomAttributeEnum


class SimpleExitSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(source="to_room.pk", read_only=True)
    direction = NamedEnumField(RoomRelationEnum, read_only=True)

    class Meta:
        model = models.RoomExit
        fields = ("pk", "direction")


class ExitSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(source="to_room.pk", read_only=True)
    direction = NamedEnumField(RoomRelationEnum, read_only=True)
    name = serializers.CharField(source="to_room.name", read_only=True)
    color_slug = serializers.CharField(source="to_room.color_slug", read_only=True)
    color_hex = serializers.CharField(source="to_room.color_hex", read_only=True)
    visible_traps = PlacedTrapSerializer(many=True, read_only=True)
    visit_count = serializers.IntegerField(read_only=True)
    # warnings =

    class Meta:
        model = models.RoomExit
        fields = (
            "pk",
            "direction",
            "name",
            "color_slug",
            "color_hex",
            "visible_traps",
            "locked",
            "visit_count",
            # "warnings",
        )


class RoomAttributeSerializer(serializers.Serializer):
    brightness = serializers.IntegerField()
    cleanliness = serializers.IntegerField()
    sanctity = serializers.IntegerField()


class CenotaphSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source="rendered_text")
    events = EventSerializer(many=True, source="room.events")
    portrait = serializers.CharField(source="_portrait_url")
    is_active = serializers.BooleanField()

    class Meta:
        model = models.Cenotaph
        fields = (
            "pk",
            "name",
            "birth",
            "death",
            "text",
            "events",
            "portrait",
            "is_active",
        )


class SimpleCenotaphSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cenotaph
        fields = ("pk", "name", "birth", "death")


class VisitedRoomSerializer(serializers.ModelSerializer):
    exits = SimpleExitSerializer(many=True, read_only=True, source="exits_from")
    lit_candle_count = serializers.IntegerField(read_only=True)
    lit_incense_count = serializers.IntegerField(read_only=True)
    active_feces_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Room
        fields = (
            "pk",
            "name",
            "exits",
            "color_name",
            "color_hex",
            "lit_candle_count",
            "lit_incense_count",
            "active_feces_count",
        )


class WarningSerializer(serializers.Serializer):
    warning = serializers.CharField()


class ActivationCriteriaSerializer(serializers.ModelSerializer):
    attribute = NamedEnumField(RoomAttributeEnum)

    class Meta:
        model = models.ActivationCriteria
        fields = (
            "attribute",
            "required_level",
            "first_satisfied",
            "satisfied",
        )


class RoomSerializer(serializers.ModelSerializer):
    exits = ExitSerializer(many=True, read_only=True, source="exits_from")
    occupants = OccupantSerializer(many=True, read_only=True)
    npcs = NonPlayerCharacterSerializer(many=True, read_only=True)
    items = ItemStackSerializer(many=True, read_only=True, source="visible_items")
    attributes = RoomAttributeSerializer(source="get_attributes", read_only=True)
    visits = RoomVisitSerializer(many=True, read_only=True)
    cenotaph = SimpleCenotaphSerializer(
        read_only=True, required=False, source="_cenotaph"
    )
    lit_candle_count = serializers.IntegerField(read_only=True)
    lit_character_candle_count = serializers.IntegerField(read_only=True)
    lit_incense_count = serializers.IntegerField(read_only=True)
    active_feces_count = serializers.IntegerField(read_only=True)
    kneel_count = serializers.IntegerField(read_only=True)
    activation_criteria = ActivationCriteriaSerializer(read_only=True, many=True)

    # def get_attributes(self, obj):
    #     return RoomAttributeSerializer(RoomService().get_attributes(obj))

    class Meta:
        model = models.Room
        fields = (
            "pk",
            "name",
            "color_name",
            "color_slug",
            "color_hex",
            "description",
            "exits",
            "occupants",
            "npcs",
            "items",
            "visits",
            "cenotaph",
            "attributes",
            "lit_candle_count",
            "lit_character_candle_count",
            "lit_incense_count",
            "active_feces_count",
            "kneel_count",
            "level",
            "activation_criteria",
        )


class SimpleRoomSerializer(serializers.ModelSerializer):
    cenotaph = SimpleCenotaphSerializer(
        read_only=True, required=False, source="_cenotaph"
    )
    exit_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Room
        fields = (
            "pk",
            "name",
            "color_name",
            "color_slug",
            "color_hex",
            "description",
            "exit_count",
            "cenotaph",
            "level",
        )


class RoomWarningSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomWarning
        fields = ("text",)


class CharacterRoomVisitSerializer(serializers.ModelSerializer):
    room = VisitedRoomSerializer()
    warnings = RoomWarningSerializer(many=True)

    class Meta:
        model = Visit
        fields = ("room", "stamp_date", "warnings")
