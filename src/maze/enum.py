from django_enumfield.enum import Enum


class RoomRelationEnum(Enum):
    UP = 0  # child
    SAME = 1  # sibling
    DOWN = 2  # parent
    PORTAL = 3  # spouse
    ITEM_PORTAL = 4  # cenotaph collectable

    __default__ = SAME


class RoomAttributeEnum(Enum):
    CLEANLINESS = 0
    BRIGHTNESS = 1
    SANCTITY = 2

    __labels__ = {
        CLEANLINESS: "cleanliness",
        BRIGHTNESS: "brightness",
        SANCTITY: "sanctity",
    }
