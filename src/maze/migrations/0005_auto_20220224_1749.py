# Generated by Django 3.1.4 on 2022-02-25 01:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("maze", "0004_auto_20220224_1749"),
    ]

    operations = [
        migrations.RenameField(
            model_name="roomexit",
            old_name="to_room_id",
            new_name="to_room",
        ),
    ]
