# Generated by Django 3.1.4 on 2022-02-24 18:48

import django.db.models.deletion
import django_enumfield.db.fields
from django.db import migrations, models

import maze.enum


def forward_ref(apps, schema_editor):
    Room = apps.get_model("maze", "Room")
    RoomExit = apps.get_model("maze", "RoomExit")
    for room in Room.objects.all():
        for exit in room.exits.all():
            RoomExit.objects.get_or_create(from_room_id=room, to_room_id=exit)


class Migration(migrations.Migration):

    dependencies = [
        ("maze", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="RoomExit",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "relation",
                    django_enumfield.db.fields.EnumField(
                        default=1, enum=maze.enum.RoomRelationEnum
                    ),
                ),
                (
                    "from_room_id",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="exits_from",
                        to="maze.room",
                    ),
                ),
                (
                    "to_room_id",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="exits_to",
                        to="maze.room",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="room",
            name="_exits",
            field=models.ManyToManyField(
                related_name="_room__exits_+", through="maze.RoomExit", to="maze.Room"
            ),
        ),
        migrations.RunPython(forward_ref, migrations.RunPython.noop),
    ]
