# Generated by Django 3.2.15 on 2022-09-18 01:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("maze", "0013_auto_20220821_1325"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cenotaphfile",
            name="slug",
            field=models.SlugField(max_length=120),
        ),
    ]
