init:
	@docker compose -p grotto run node npm install
	@docker compose -p grotto up -d app node
	@docker compose -p grotto exec app python manage.py createsuperuser
build:
	@docker compose -p grotto build --pull
sass:
	@docker compose -p grotto exec app python manage.py compress --force
	@docker compose -p grotto exec app python manage.py collectstatic --no-input
run:
	@docker compose -p grotto up -d
stop:
	@docker compose -p grotto down
migrations:
	@docker compose -p grotto exec app python manage.py makemigrations
logs:
	@docker compose -p grotto logs -f app
beat-logs:
	@docker compose -p grotto logs -f beat
node-logs:
	@docker compose -p grotto logs -f node
superuser:
	@docker compose -p grotto exec app python manage.py createsuperuser
django-shell:
	@docker compose -p grotto exec app python manage.py shell
shell:
	@docker compose -p grotto exec app sh
node-shell:
	@docker compose -p grotto exec node sh
lint:
	@docker compose -p grotto exec app python -m black .
	@docker compose -p grotto exec app python -m isort . --profile black
	@docker compose -p grotto exec node npx prettier --write .
	@docker compose -p grotto exec app python -m flake8
	@docker compose -p grotto exec node npm run lint
worker-logs:
	@docker compose -p grotto logs -f worker
unittest:
	@docker compose -p grotto exec app pytest
