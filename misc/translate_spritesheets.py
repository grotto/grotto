coords_mapping = {
    (0,0): (9,9),
    (0,1): (5,12),
    (0,2): (7,16),
    (0,3): (7,15),
    (0,4): (5,14),
    (0,5): (5,15),
    (0,6): (5,16),
    (0,7): (7,14),
    (0,8): (5,17),
    (0,9): (5,18),
    (0,10): (5,19),
    (0,11): (6,22),
    (1,0): (5,10),
    (1,1): (5,22),
    (1,2): (6,0),
    (1,3): (6,1),
    (1,4): (6,2),
    (1,5): (6,3),
    (1,6): (6,4),
    (1,7): (6,5),
    (1,8): (6,6),
    (1,9): (6,7),
    (1,10): (6,8),
    (1,11): (6,9),
    (2,0): (6,10),
    (2,1): (6,11),
    (2,2): (6,12),
    (2,3): (6,13),
    (2,4): (6,14),
    (2,5): (6,15),
    (2,6): (6,16),
    (2,7): (6,17),
    (2,8): (6,18),
    (2,9): (6,19),
    (2,10): (6,20),
    (2,11): (6,21),
}


addresses = {}
for orig, new in coords_mapping.items():
    _orig = orig[0] * 12 + orig[1] + 1
    _new = new[0] * 23 + new[1] + 1
    addresses[_orig] = _new

def translate_cell(orig):
    rot = orig & 0xffff0000
    addr = orig & 0xffff
    try:
        _addr = addresses[addr]
    except KeyError:
        _addr = addr
    return rot | _addr


orig_file = "../src/static/grottopaint/maps/wumpus_bite.tmj"
new_file = "../src/static/grottopaint/maps/wumpus_bite_newspritesheet.tmj"
import json
from pathlib import Path

def main():
    with open(Path(orig_file)) as f:
        rooms = json.load(f)

    for layer in rooms["layers"]:
        try:
            layer["data"] = [translate_cell(d) for d in layer["data"]]
        except KeyError:
            continue

    with open(Path(new_file), "w") as f:
        json.dump(rooms, f)

main()


# print(translate_cell(1073741850))
# print(translate_cell(26))
# print(translate_cell(16))
# print(translate_cell(17))
# print(translate_cell(25))
# print(translate_cell(1073741849))