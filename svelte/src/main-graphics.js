import Graphics from "./Graphics.svelte";

const target = document.getElementById("graphics-target");

function replaceTarget(target) {
  const graphics = new Graphics({
    target: target.parentElement,
    anchor: target,
    props: JSON.parse(document.getElementById("graphics-props").textContent),
  });
  target.remove();
  return graphics;
}

const graphics = replaceTarget(target);

export default graphics;
