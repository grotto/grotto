function unpackColorHex(colorhex) {
  const figure = parseInt(colorhex.slice(1), 16);
  const ret = [];
  ret.push(figure >> 16);
  ret.push((figure >> 8) & 0x00ff);
  ret.push(figure & 0x0000ff);
  return ret;
}

function clampValue(value, min = 0, max = 255) {
  return Math.min(max, Math.max(min, value));
}

function packColorHex(color) {
  // clamps values between 0-255 and assembles color hex
  return (
    "#" +
    color.reduce((existing, value) => (existing << 8) | value).toString(16)
  );
}

export function lightenColor(colorhex, percent = 20) {
  const amount = percent * 2.55;
  let color = unpackColorHex(colorhex);
  color = color.map((value) => clampValue(parseInt(value + amount)));
  return packColorHex(color);
}

export function invertColor(colorhex) {
  const figure = parseInt(colorhex.slice(1), 16);
  return (
    "#" +
    ((figure & 0x000000) | (~figure & 0xffffff))
      .toString(16)
      .padStart(6, "0")
      .toUpperCase()
  );
}

export function eventConsolidator(raw) {
  const _events = [];
  let event = { cardinality: 0, text: null, room: null };
  raw.forEach((d) => {
    if (d.text == event.text && d.room == event.room) {
      event.cardinality += 1;
    } else {
      if (event.text !== null) {
        if (event.cardinality == 1) {
          _events.push({
            text: event.text,
            created: d.created,
            room: d.room,
          });
        } else {
          _events.push({
            text: `${event.text} x${event.cardinality}`,
            created: d.created,
            room: d.room,
          });
        }
      }
      event = { cardinality: 1, text: d.text, room: d.room };
    }
  });
  return _events;
}
