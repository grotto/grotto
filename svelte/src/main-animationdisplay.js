import AnimationDisplay from "./AnimationDisplay.svelte";

const target = document.getElementById("animationdisplay-target");

function replaceTarget(target) {
  const animationdisplay = new AnimationDisplay({
    target: target.parentElement,
    anchor: target,
    props: JSON.parse(document.getElementById("animationdisplay-props").textContent),
  });
  target.remove();
  return animationdisplay;
}

const animationdisplay = replaceTarget(target);

export default animationdisplay;
