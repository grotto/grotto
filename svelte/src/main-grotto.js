import Grotto from "./Grotto.svelte";

const target = document.getElementById("grotto-target");

function replaceTarget(target) {
  const grotto = new Grotto({
    target: target.parentElement,
    anchor: target,
    props: JSON.parse(document.getElementById("grotto-props").textContent),
  });
  target.remove();
  return grotto;
}

const grotto = replaceTarget(target);

export default grotto;
