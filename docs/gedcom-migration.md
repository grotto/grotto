# Seeding a fresh instance

* In admin create an "Instance Gedcom" model instance an upload the gedcom file.
* On the admin index for "Instance Gedcom" select the checkbox beside the newly created record and choose from the "Actions" select menu the option "make selected gedcom file active" and hit "Go". Wait about 10 minutes for the system to process the gedcom file; then refresh the page. The "imported" column should show that the file was successfully parsed.
* In admin create an "Instance config" model instance and upload the 4 csv files
* on the admin indes for "Instance config" select the checkbox beside the newly created record and choose from the "Actions" select menu "Make selected config active" and hit "Go". It will take a few minutes to complete the import. The "imported" column should confirm that the import did complete
* Finally choose the action called "Seed items, lock doors, place traps" and allow several minutes for the process to complete.




* Create new rooms
  * Run `create_cenotaphs`
  * Run `place_npcs`

* Identify the starting room (wiley's room) should be on the last page of rooms

* set the env var in docker-compose
GROTTO_STARTING_ROOM_ID

* Run `sort_rooms_to_level` and pass the starting room ID
