# Runloop

All actions in Grotto are initiated by an API request and will be resolved before the response returns.

Game actions will return a `Tableau` object as the response, which will show all data available to the character.

The API views that comprise game actions must inherit  `grotto.api.views.GameActionMixin` which regulates the advancement of the game by performing these steps:
  * Causing any candles and incense to "burn out" if they have been active beyond their threshold (all rooms),
  * Potentially make the character's current room dirtier,
    * Pops when any nD20 rolls 1.
  * Increase character entropy (action points),
    * Increase by value of nD20 roll, automatically advance to theshold on any roll of 20.
  * Increase NPC entropy (all rooms),
    * Increase by value of nD20 roll.

Some of these steps may require their own resolution.
For example, if the character entropy exceeds a threshold then an item may be spawned.
Likewise, when the NPC entropy exceeds the threshold the NPC may move which can have follow-on effects like character death or item creation/deletion/relocation.
In all cases the character action occurs and is resolved **before** the game is advanced.
The character action will specify the number of dice to roll (default 1).

Non game action API endpoints should not trigger any of the aforementioned steps and should not affect the game state apart from possibly adding a new character to the maze.

